{
    "id": "391cf328-de4d-4201-a4ce-c756710ceb84",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f34293a-bd7e-4ff6-85c4-80fc1c1a773e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "391cf328-de4d-4201-a4ce-c756710ceb84",
            "compositeImage": {
                "id": "93f68b06-b096-4780-87f1-326c177c0306",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f34293a-bd7e-4ff6-85c4-80fc1c1a773e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "963850ca-eec8-47da-adfc-9c824d884cd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f34293a-bd7e-4ff6-85c4-80fc1c1a773e",
                    "LayerId": "40a2ce4e-5796-47f6-a6d3-c4b24e0ce8cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "40a2ce4e-5796-47f6-a6d3-c4b24e0ce8cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "391cf328-de4d-4201-a4ce-c756710ceb84",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 400,
    "yorig": 25
}