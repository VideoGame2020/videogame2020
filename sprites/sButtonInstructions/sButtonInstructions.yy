{
    "id": "f3492ea5-b996-4e7e-9d28-5d4b1fa98f19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonInstructions",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 36,
    "bbox_right": 169,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30fd2480-b03a-401f-8aec-5284f86be0f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3492ea5-b996-4e7e-9d28-5d4b1fa98f19",
            "compositeImage": {
                "id": "99134420-e59a-4b40-8501-a749011d0803",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30fd2480-b03a-401f-8aec-5284f86be0f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e36ab867-2172-47a0-8489-84dd9cdf8f7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30fd2480-b03a-401f-8aec-5284f86be0f4",
                    "LayerId": "cd163b8f-9c7a-4d6e-92db-0804ccc2fc8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cd163b8f-9c7a-4d6e-92db-0804ccc2fc8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3492ea5-b996-4e7e-9d28-5d4b1fa98f19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 103,
    "yorig": 28
}