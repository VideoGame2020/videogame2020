{
    "id": "5c6288fe-5da4-42c7-a9a8-0d76f752e387",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGold",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 499,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97214bd9-3e43-4084-919c-3e0e527da6d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c6288fe-5da4-42c7-a9a8-0d76f752e387",
            "compositeImage": {
                "id": "0164dbe1-7a29-4539-bc9c-1b7b91df7d93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97214bd9-3e43-4084-919c-3e0e527da6d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "771b9bd5-fcfc-452d-a521-ccfb8afeba8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97214bd9-3e43-4084-919c-3e0e527da6d1",
                    "LayerId": "f1e0dd84-6288-4e86-9a57-b5c3c019e218"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "f1e0dd84-6288-4e86-9a57-b5c3c019e218",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c6288fe-5da4-42c7-a9a8-0d76f752e387",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 300,
    "yorig": 250
}