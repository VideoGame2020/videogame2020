{
    "id": "e175fcdf-4629-4cd9-a4b2-a0c6b46b87dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHomeScroll",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 766,
    "bbox_left": 0,
    "bbox_right": 1999,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b843303e-af62-4d77-a692-46896352f5a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e175fcdf-4629-4cd9-a4b2-a0c6b46b87dd",
            "compositeImage": {
                "id": "e7696f11-41b0-4869-9d0c-0c8082239441",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b843303e-af62-4d77-a692-46896352f5a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "498b0e99-7c9e-4999-958f-63feb0dbee17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b843303e-af62-4d77-a692-46896352f5a1",
                    "LayerId": "878799c7-1b51-4546-ae8c-1ebb5b151a1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 767,
    "layers": [
        {
            "id": "878799c7-1b51-4546-ae8c-1ebb5b151a1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e175fcdf-4629-4cd9-a4b2-a0c6b46b87dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2000,
    "xorig": 0,
    "yorig": 0
}