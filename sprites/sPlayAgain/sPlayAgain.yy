{
    "id": "45bf1ccc-3015-4ea5-bae7-1934035d5b5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayAgain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 36,
    "bbox_right": 170,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82f61054-74a3-45ba-a124-572795e6a247",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45bf1ccc-3015-4ea5-bae7-1934035d5b5c",
            "compositeImage": {
                "id": "331dbf1e-742f-4799-9d3f-8424d43b1724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82f61054-74a3-45ba-a124-572795e6a247",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "972cf9b0-573e-446b-846f-4ace9df2c8fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82f61054-74a3-45ba-a124-572795e6a247",
                    "LayerId": "a7108927-0e6a-443e-9020-fadd55f5a392"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a7108927-0e6a-443e-9020-fadd55f5a392",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45bf1ccc-3015-4ea5-bae7-1934035d5b5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 102,
    "yorig": 34
}