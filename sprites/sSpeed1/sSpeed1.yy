{
    "id": "cea494f2-2b94-4692-bc36-dcb494fce6b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpeed1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6398541-e74b-4047-9810-a51078280b9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cea494f2-2b94-4692-bc36-dcb494fce6b7",
            "compositeImage": {
                "id": "5558dc5e-75e0-454f-b634-4b12535baedc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6398541-e74b-4047-9810-a51078280b9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df4b5b8d-4d26-4d06-9ab1-265065bff785",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6398541-e74b-4047-9810-a51078280b9c",
                    "LayerId": "1abd3ed2-b011-4793-9cd6-d32bfeed34f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "1abd3ed2-b011-4793-9cd6-d32bfeed34f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cea494f2-2b94-4692-bc36-dcb494fce6b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 50
}