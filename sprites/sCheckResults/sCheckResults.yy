{
    "id": "2c008e0c-4a62-4348-928e-1697f53dcdfb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCheckResults",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8bb3ec3b-064f-49c1-9f95-9e864b14009b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c008e0c-4a62-4348-928e-1697f53dcdfb",
            "compositeImage": {
                "id": "15a3c46a-16df-4a5c-bb69-65232f575c46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bb3ec3b-064f-49c1-9f95-9e864b14009b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f5f69ea-e7da-44b7-9e4f-d48c2a3b55a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bb3ec3b-064f-49c1-9f95-9e864b14009b",
                    "LayerId": "4f9e02d4-3ca7-4772-9789-5d7ba00d9f7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4f9e02d4-3ca7-4772-9789-5d7ba00d9f7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c008e0c-4a62-4348-928e-1697f53dcdfb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}