{
    "id": "767e90df-0e8a-4ac5-a17e-015a9fa668ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGoHome",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 36,
    "bbox_right": 170,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "549ba605-df7c-4af8-81ae-3139872ed37a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "767e90df-0e8a-4ac5-a17e-015a9fa668ee",
            "compositeImage": {
                "id": "92dad460-46b4-4a30-8dd0-2f44982d8595",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "549ba605-df7c-4af8-81ae-3139872ed37a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf94b001-dcc8-441e-a199-9764a53a2111",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "549ba605-df7c-4af8-81ae-3139872ed37a",
                    "LayerId": "c28948c3-e318-476e-99a7-e2f4569e3808"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c28948c3-e318-476e-99a7-e2f4569e3808",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "767e90df-0e8a-4ac5-a17e-015a9fa668ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 102,
    "yorig": 34
}