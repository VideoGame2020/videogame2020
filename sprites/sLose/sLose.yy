{
    "id": "00394a96-e2bd-4192-82bb-a2b43c16af0b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLose",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 499,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3908df1-64e3-4654-b7f6-8fc898eeef40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00394a96-e2bd-4192-82bb-a2b43c16af0b",
            "compositeImage": {
                "id": "d7fa4168-1396-4308-97c9-4bae69881b2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3908df1-64e3-4654-b7f6-8fc898eeef40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba7960d9-477a-450c-aa6a-4050cb2f9024",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3908df1-64e3-4654-b7f6-8fc898eeef40",
                    "LayerId": "bba92843-59d4-4c13-abab-b006b4a00f6e"
                },
                {
                    "id": "08855f94-6fee-4020-89b3-0b10b8bc2bb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3908df1-64e3-4654-b7f6-8fc898eeef40",
                    "LayerId": "ce052a31-0bd0-4a77-b192-6182e5208bd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "ce052a31-0bd0-4a77-b192-6182e5208bd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00394a96-e2bd-4192-82bb-a2b43c16af0b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "bba92843-59d4-4c13-abab-b006b4a00f6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00394a96-e2bd-4192-82bb-a2b43c16af0b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 300,
    "yorig": 250
}