{
    "id": "28644f7d-fd18-4625-af16-e6f0e34b228a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLevel1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 169,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1425c71-8795-48cd-ad87-f54f716a2497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28644f7d-fd18-4625-af16-e6f0e34b228a",
            "compositeImage": {
                "id": "b58ac2c5-aad5-4e48-90fa-e136292bb6c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1425c71-8795-48cd-ad87-f54f716a2497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e475fbbe-1fa0-42e8-9178-a9392b4a1c6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1425c71-8795-48cd-ad87-f54f716a2497",
                    "LayerId": "3f18a443-d028-41b0-a6fb-3175cd3c0b77"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 170,
    "layers": [
        {
            "id": "3f18a443-d028-41b0-a6fb-3175cd3c0b77",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28644f7d-fd18-4625-af16-e6f0e34b228a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 85
}