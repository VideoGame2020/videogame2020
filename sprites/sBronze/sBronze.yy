{
    "id": "44045e11-5e5c-43d2-a2a6-8d54130cf1c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBronze",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 499,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0e3a08a-7af0-4790-8718-2a9f21520a4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44045e11-5e5c-43d2-a2a6-8d54130cf1c4",
            "compositeImage": {
                "id": "2e379d31-9564-40a5-a7ba-9a039bc5ca4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0e3a08a-7af0-4790-8718-2a9f21520a4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ad0b2f8-ae0d-45f9-9558-e61b1d2bc024",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0e3a08a-7af0-4790-8718-2a9f21520a4b",
                    "LayerId": "ba13e263-3730-44ca-b529-118464d29bfd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "ba13e263-3730-44ca-b529-118464d29bfd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44045e11-5e5c-43d2-a2a6-8d54130cf1c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 300,
    "yorig": 250
}