{
    "id": "b711154d-96d7-4223-9096-0c6be34aca5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDance1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 38,
    "bbox_right": 169,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "994282c1-5f3d-4d39-843a-fee4caae18d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b711154d-96d7-4223-9096-0c6be34aca5a",
            "compositeImage": {
                "id": "cf2e4932-a1c1-487c-a3fc-bdddaa69e0a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "994282c1-5f3d-4d39-843a-fee4caae18d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a84647a2-52a2-4e34-9f79-135f0ccd4cee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "994282c1-5f3d-4d39-843a-fee4caae18d6",
                    "LayerId": "fcbca910-ee89-41b6-9cc4-55359b041447"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "fcbca910-ee89-41b6-9cc4-55359b041447",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b711154d-96d7-4223-9096-0c6be34aca5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 205,
    "xorig": 102,
    "yorig": 57
}