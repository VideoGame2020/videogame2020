{
    "id": "c0c0b2eb-0ed9-46ce-8bd7-4e8e3052ac91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMenuBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1026,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60824d50-fad2-438d-a712-57903e91cb6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0c0b2eb-0ed9-46ce-8bd7-4e8e3052ac91",
            "compositeImage": {
                "id": "61e8b896-e528-4ce9-9c29-84f65653051f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60824d50-fad2-438d-a712-57903e91cb6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cd66069-8967-4539-90ab-d95a46d00bd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60824d50-fad2-438d-a712-57903e91cb6f",
                    "LayerId": "3aa0e76e-305d-4d58-a819-e02c479f44ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "3aa0e76e-305d-4d58-a819-e02c479f44ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0c0b2eb-0ed9-46ce-8bd7-4e8e3052ac91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1027,
    "xorig": 0,
    "yorig": 0
}