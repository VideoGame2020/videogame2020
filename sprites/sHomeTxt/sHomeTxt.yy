{
    "id": "1c7ebb5e-0046-446d-a760-a347dc0bde56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHomeTxt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03e120a8-c14b-4a30-918c-d970b8c1ab69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c7ebb5e-0046-446d-a760-a347dc0bde56",
            "compositeImage": {
                "id": "8c97d86a-1973-4ff9-b317-6c1049cedee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03e120a8-c14b-4a30-918c-d970b8c1ab69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "785622b4-1d0a-4a57-a398-a9844be70f44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03e120a8-c14b-4a30-918c-d970b8c1ab69",
                    "LayerId": "bad011d9-1e41-4baa-8705-308cbf5d9504"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bad011d9-1e41-4baa-8705-308cbf5d9504",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c7ebb5e-0046-446d-a760-a347dc0bde56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}