{
    "id": "37fb6a48-0580-4d1a-b150-12f887235597",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGameBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1026,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2f05617-2305-48f6-932d-5e0fc28fbc0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37fb6a48-0580-4d1a-b150-12f887235597",
            "compositeImage": {
                "id": "97500565-cdc4-4046-b31e-511581963822",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2f05617-2305-48f6-932d-5e0fc28fbc0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75ecc939-027e-4fec-89ec-b5d6215d5984",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2f05617-2305-48f6-932d-5e0fc28fbc0b",
                    "LayerId": "93e82105-a280-4887-8e47-d12cc030ae5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "93e82105-a280-4887-8e47-d12cc030ae5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37fb6a48-0580-4d1a-b150-12f887235597",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1027,
    "xorig": 0,
    "yorig": 0
}