{
    "id": "689f2267-dfae-4d82-92ab-388e122cc832",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSilver",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 499,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d243f8a-db2d-458a-b945-b1444681dc8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "689f2267-dfae-4d82-92ab-388e122cc832",
            "compositeImage": {
                "id": "0e8b8a83-30cf-4162-adaf-362c3b89530c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d243f8a-db2d-458a-b945-b1444681dc8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7752ea48-e619-47ae-b59d-b5b9dcb6924e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d243f8a-db2d-458a-b945-b1444681dc8b",
                    "LayerId": "7154474a-a942-4a63-a4a8-7e144959b986"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "7154474a-a942-4a63-a4a8-7e144959b986",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "689f2267-dfae-4d82-92ab-388e122cc832",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 300,
    "yorig": 250
}