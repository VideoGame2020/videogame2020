{
    "id": "095c34fa-fc12-4dbd-a77d-0531e75c3a69",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayAgain",
    "eventList": [
        {
            "id": "48780bfb-a994-444b-a21b-eb7b7ff7b364",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "095c34fa-fc12-4dbd-a77d-0531e75c3a69"
        },
        {
            "id": "2ebe4dc8-8248-4e25-b47a-75f8a235f9b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "095c34fa-fc12-4dbd-a77d-0531e75c3a69"
        },
        {
            "id": "d544497a-2a67-445e-8602-f08daa1188db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "095c34fa-fc12-4dbd-a77d-0531e75c3a69"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "45bf1ccc-3015-4ea5-bae7-1934035d5b5c",
    "visible": true
}