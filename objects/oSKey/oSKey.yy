{
    "id": "a3261ea3-c60b-4856-9f23-a8315f9071ce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSKey",
    "eventList": [
        {
            "id": "db722c41-c8d4-45b3-9384-fddc22f190b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a3261ea3-c60b-4856-9f23-a8315f9071ce"
        },
        {
            "id": "c1065a1e-aef8-4e94-837d-58e408678ea7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a3261ea3-c60b-4856-9f23-a8315f9071ce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a86951bf-5007-4c7f-bbdc-8f88795e972c",
    "visible": true
}