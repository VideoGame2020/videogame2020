{
    "id": "cecc909c-1161-4b9d-92a0-e792decab2b4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGoHome",
    "eventList": [
        {
            "id": "49d3aacb-ebf0-4632-9719-7e13777a6a56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cecc909c-1161-4b9d-92a0-e792decab2b4"
        },
        {
            "id": "bbf52ab3-12e9-466a-8496-e00aef1671dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cecc909c-1161-4b9d-92a0-e792decab2b4"
        },
        {
            "id": "9bd2e218-52f7-4a86-b116-9ae00e4b629b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "cecc909c-1161-4b9d-92a0-e792decab2b4"
        },
        {
            "id": "162c03bb-4887-410e-9460-e4ae2d6d650c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 5,
            "m_owner": "cecc909c-1161-4b9d-92a0-e792decab2b4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "767e90df-0e8a-4ac5-a17e-015a9fa668ee",
    "visible": true
}