{
    "id": "b5a7a9eb-a79a-4663-b6da-9f05a6c90a5d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGold",
    "eventList": [
        {
            "id": "5e50c31d-743a-483d-acc8-31f214f14435",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b5a7a9eb-a79a-4663-b6da-9f05a6c90a5d"
        },
        {
            "id": "36ca180d-b0f5-41cd-b62c-5063e8125c57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b5a7a9eb-a79a-4663-b6da-9f05a6c90a5d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5c6288fe-5da4-42c7-a9a8-0d76f752e387",
    "visible": true
}