{
    "id": "11eda8ef-0c17-44c1-afa5-c395c508582a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oControls_head",
    "eventList": [
        {
            "id": "1cf467ed-415e-4d87-ba29-c92903090a42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "11eda8ef-0c17-44c1-afa5-c395c508582a"
        },
        {
            "id": "f5732fcc-3036-4ed3-9f27-69de40bf74de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "11eda8ef-0c17-44c1-afa5-c395c508582a"
        },
        {
            "id": "b172e4ed-07ab-4232-b58e-d3d82f46ef34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 10,
            "m_owner": "11eda8ef-0c17-44c1-afa5-c395c508582a"
        },
        {
            "id": "df778258-dcf1-4ede-b7df-13c3beda0396",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 5,
            "m_owner": "11eda8ef-0c17-44c1-afa5-c395c508582a"
        },
        {
            "id": "1aaaa070-cdfe-4269-9730-ddf8f5caf45c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 10,
            "m_owner": "11eda8ef-0c17-44c1-afa5-c395c508582a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}