{
    "id": "1c037a9b-5df6-44a5-a308-bdf17d25a65f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDemGoalB",
    "eventList": [
        {
            "id": "73faecac-9674-41ef-a7d5-a4aa47ad1a35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1c037a9b-5df6-44a5-a308-bdf17d25a65f"
        },
        {
            "id": "3cb09a07-4164-4a16-8659-7d118d2848a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1c037a9b-5df6-44a5-a308-bdf17d25a65f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aa0a1a2b-2734-43cb-af91-bab6a2f48f7e",
    "visible": true
}