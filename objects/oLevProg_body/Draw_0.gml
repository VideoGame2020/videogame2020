draw_set_font(fBody);
draw_set_colour(c_white);


draw_text(x, y, string("Levels:"));
draw_text(x, y+spaceTxt*0.5, string("Each level adds more keys that fall"));
draw_text(x, y+spaceTxt, string("Level 1: 3 keys     Level 2: 4 keys     Level 3: 5 keys"));


draw_text(x, y+spaceTxt*2, string("Speeds:"));
draw_text(x, y+spaceTxt*2.5, string("The speeds are intended to allow the player to become more comfortable with "));
draw_text(x, y+spaceTxt*3, string("each level by practicing at a slow speed and ending with a fast speed"));