{
    "id": "9d5954b3-05e0-4298-bb48-6905c1a986ff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBasics_head",
    "eventList": [
        {
            "id": "bf3e8ee0-b38f-4362-a660-15ed55546dec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9d5954b3-05e0-4298-bb48-6905c1a986ff"
        },
        {
            "id": "0dfa50d9-a309-4cbc-8991-e15cfb67bad1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9d5954b3-05e0-4298-bb48-6905c1a986ff"
        },
        {
            "id": "ba04e267-fb84-4e31-9d98-7199434375af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 10,
            "m_owner": "9d5954b3-05e0-4298-bb48-6905c1a986ff"
        },
        {
            "id": "b54bf958-6907-4532-8d0b-90a5aa5860e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 5,
            "m_owner": "9d5954b3-05e0-4298-bb48-6905c1a986ff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}