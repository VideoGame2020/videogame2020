{
    "id": "a9057328-7b1e-477d-af5d-fcb7ef6f6cd1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBronze",
    "eventList": [
        {
            "id": "6773d5ae-22c1-4bf0-9188-8f2811212ffc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a9057328-7b1e-477d-af5d-fcb7ef6f6cd1"
        },
        {
            "id": "df5af5f3-dafc-46ad-91ad-94f0a30a72a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a9057328-7b1e-477d-af5d-fcb7ef6f6cd1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "44045e11-5e5c-43d2-a2a6-8d54130cf1c4",
    "visible": true
}