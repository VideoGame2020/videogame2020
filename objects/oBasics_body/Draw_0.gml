draw_set_font(fBody);
draw_set_colour(c_white);


draw_text(x, y, string("Letters will fall from the top of the screen"));

draw_text(x, y+spaceTxt, string("Each letter has a corresponding target at the"));
draw_text(x, y+spaceTxt*1.5, string("target at the bottom of the screen and"));
draw_text(x, y+spaceTxt*2, string("key on the keyboard"));


draw_text(x, y+spaceTxt*3, string("Your goal is to hold down the correct"));
draw_text(x, y+spaceTxt*3.5, string("key on the keyboard and press the spacebar"));
draw_text(x, y+spaceTxt*4, string("when the key is overlapping its target"));

draw_text(x, y+spaceTxt*5, string("Hit the keys to score points and"));
draw_text(x, y+spaceTxt*5.5, string("to keep the character dancing"));
