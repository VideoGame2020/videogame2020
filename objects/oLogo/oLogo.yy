{
    "id": "5e538634-16e1-4d99-80c4-5300ec5e3f0f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLogo",
    "eventList": [
        {
            "id": "8aab7b5f-ea98-41dd-90d6-fd12e84695d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5e538634-16e1-4d99-80c4-5300ec5e3f0f"
        },
        {
            "id": "99196c3c-6218-4c8e-a42a-7cec4f554ea8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5e538634-16e1-4d99-80c4-5300ec5e3f0f"
        },
        {
            "id": "310e36a6-95ca-49d4-9d96-197ac81bbe7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "5e538634-16e1-4d99-80c4-5300ec5e3f0f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6d4bf50e-6152-47cf-8173-8ae3ea06361b",
    "visible": true
}