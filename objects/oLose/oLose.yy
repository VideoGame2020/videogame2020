{
    "id": "d06740c6-eb53-4c9c-b2c5-01505ef4568f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLose",
    "eventList": [
        {
            "id": "7d5a7654-6990-4290-b477-302d35f91174",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d06740c6-eb53-4c9c-b2c5-01505ef4568f"
        },
        {
            "id": "9c5808a6-53e5-4561-8b5c-1d9b75f2db18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d06740c6-eb53-4c9c-b2c5-01505ef4568f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00394a96-e2bd-4192-82bb-a2b43c16af0b",
    "visible": true
}