{
    "id": "54bd92cb-1525-423b-833d-3f161a06e21f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRules_basicsBod",
    "eventList": [
        {
            "id": "005b8799-742a-4b36-a5df-0f4363d557bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "54bd92cb-1525-423b-833d-3f161a06e21f"
        },
        {
            "id": "6ee716c5-a637-48f8-b607-0aaccad99367",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "54bd92cb-1525-423b-833d-3f161a06e21f"
        },
        {
            "id": "55f83262-de2a-4777-a7dc-6d44fb9e78e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "54bd92cb-1525-423b-833d-3f161a06e21f"
        },
        {
            "id": "fbf7fd09-d67c-487f-9f31-bd798b908a02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 10,
            "m_owner": "54bd92cb-1525-423b-833d-3f161a06e21f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}