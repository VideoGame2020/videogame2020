draw_set_font(fRules_body);

draw_text(x, y, string("Letters will fall from the top of the screen"));

draw_text(x, y+spaceText, string("Each letter has a corresponding target at"));
draw_text(x, y+spaceText*1.25, string("the bottom of the screen and key on the"));
draw_text(x, y+spaceText*1.5, string("keyboard"));

draw_text(x, y+spaceText*2.5, string("Your goal is to hold down the correct key"));
draw_text(x, y+spaceText*2.75, string("on the keyboard and press the spacebar"));
draw_text(x, y+spaceText*3, string("when the key is overlapping its target"));

draw_text(x, y+spaceText*4, string("Hit the keys to score points and to keep"));
draw_text(x, y+spaceText*4.25, string("the character dancing"));