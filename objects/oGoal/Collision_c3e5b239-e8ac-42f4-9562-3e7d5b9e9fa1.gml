if (keyboard_check(vk_right))||(keyboard_check(vk_up))||(keyboard_check(vk_down))||(keyboard_check(vk_enter)){
	if (keyboard_check(vk_left)){
		if (mouse_check_button_pressed(mb_left) || keyboard_check_pressed(vk_space)){
			with (oScore) thescore = thescore - 10;
			with (oBaseDance) sprite_index = sBaseDance;
			oBaseDance.y+=pent;
		}
	}
}

if (keyboard_check(vk_down)){
	if (mouse_check_button_pressed(mb_left) || keyboard_check_pressed(vk_space)){
		with instance_nearest(x, y, oSKey) instance_destroy();
		with (oScore) thescore = thescore + 5;
		with (oBaseDance) sprite_index = sDance2;
		
		if(instance_exists(oDance1)){
			if(oDance1.y+pent > room_height/2){
				oDance1.y-=pent;
			}
		}else if(instance_exists(oDance2)){
			if(oDance2.y+pent > room_height/2){
				oDance2.y-=pent;
			}
		}else if(instance_exists(oDance3)){
			if(oDance3.y+pent > room_height/2){
				oDance3.y-=pent;
			}
		}else if(instance_exists(oDance4)){
			if(oDance4.y+pent > room_height/2){
				oDance4.y-=pent;
			}
		}else if(instance_exists(oDance5)){
			if(oDance5.y+pent > room_height/2){
				oDance5.y-=pent;
			}
		}else if(instance_exists(oBaseDance)){
			if(oBaseDance.y+pent > room_height/2){
				oBaseDance.y-=pent;
			}
		}
	}
}