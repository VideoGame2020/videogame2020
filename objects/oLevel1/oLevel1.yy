{
    "id": "6d48eb04-0445-4971-8d89-93599f95ace0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLevel1",
    "eventList": [
        {
            "id": "bebb861f-6c65-41d5-8792-2c78e23a2cfc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6d48eb04-0445-4971-8d89-93599f95ace0"
        },
        {
            "id": "f4fc36a7-6873-4b2a-a530-e752e0d30933",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6d48eb04-0445-4971-8d89-93599f95ace0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "28644f7d-fd18-4625-af16-e6f0e34b228a",
    "visible": true
}