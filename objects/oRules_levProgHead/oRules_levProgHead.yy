{
    "id": "26e136e8-958f-4139-b956-1ae3ba84f780",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRules_levProgHead",
    "eventList": [
        {
            "id": "2fb7789d-2371-40df-b84b-a021ca021e63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "26e136e8-958f-4139-b956-1ae3ba84f780"
        },
        {
            "id": "4170ccd5-1eed-4207-8eb4-abe86fcc39a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "26e136e8-958f-4139-b956-1ae3ba84f780"
        },
        {
            "id": "d02aa79b-058c-4897-b9a0-3bd86389fcd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "26e136e8-958f-4139-b956-1ae3ba84f780"
        },
        {
            "id": "0fedf19d-4ef3-4441-8911-c43d53ea549d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 10,
            "m_owner": "26e136e8-958f-4139-b956-1ae3ba84f780"
        },
        {
            "id": "044c11ab-124f-451d-ab8a-87848a61418f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 10,
            "m_owner": "26e136e8-958f-4139-b956-1ae3ba84f780"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}