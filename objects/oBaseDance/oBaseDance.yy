{
    "id": "32417331-c87e-4edb-b7ae-ba2ed81b0288",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBaseDance",
    "eventList": [
        {
            "id": "177deb1f-c51f-4d78-bfc9-aae7b13d1e57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "32417331-c87e-4edb-b7ae-ba2ed81b0288"
        },
        {
            "id": "5c323f3b-433c-4207-8f39-80099c4f6a64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "32417331-c87e-4edb-b7ae-ba2ed81b0288"
        },
        {
            "id": "60fc82bc-1766-4c8a-aa7a-2eceff0e18f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "32417331-c87e-4edb-b7ae-ba2ed81b0288"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b3a3557f-6ffe-40ff-bc9c-3a9303cbc53b",
    "visible": true
}