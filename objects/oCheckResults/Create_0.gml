with(oScore){
	if(thescore < 300){
		room_goto(rmLose);
	}else if(thescore >=300 && thescore<400){
		room_goto(rmBronze);
	}else if(thescore >=400 && thescore<500){
		room_goto(rmSilver);
	}else if(thescore >=500 && thescore<=600){
		room_goto(rmGold);
	}
}