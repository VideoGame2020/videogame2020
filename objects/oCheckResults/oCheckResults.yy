{
    "id": "08319935-f66a-47fd-82d0-40a451747247",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCheckResults",
    "eventList": [
        {
            "id": "59b40738-977a-4ac7-8b35-71cd431ddd15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "08319935-f66a-47fd-82d0-40a451747247"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2c008e0c-4a62-4348-928e-1697f53dcdfb",
    "visible": true
}