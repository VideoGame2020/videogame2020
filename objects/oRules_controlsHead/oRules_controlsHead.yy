{
    "id": "8802c733-0152-45ac-8c36-4213fa1b2adb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRules_controlsHead",
    "eventList": [
        {
            "id": "64b7dea6-8242-4aef-8701-8434291b6f83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8802c733-0152-45ac-8c36-4213fa1b2adb"
        },
        {
            "id": "df7ed02b-3385-43c7-a91d-80a03d120d8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8802c733-0152-45ac-8c36-4213fa1b2adb"
        },
        {
            "id": "cf41ae30-4e9e-4459-86a3-af08920a8832",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "8802c733-0152-45ac-8c36-4213fa1b2adb"
        },
        {
            "id": "3018e37a-1f34-4ab0-9d47-dc0cfc99c969",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 10,
            "m_owner": "8802c733-0152-45ac-8c36-4213fa1b2adb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}