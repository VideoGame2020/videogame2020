{
    "id": "c8f75e4e-7231-49b0-9a42-a360058ac604",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlack",
    "eventList": [
        {
            "id": "50b75868-fc60-41a2-90f6-6c829c951d42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c8f75e4e-7231-49b0-9a42-a360058ac604"
        },
        {
            "id": "bf02b62b-9135-4d84-b4d7-63e92443746f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c8f75e4e-7231-49b0-9a42-a360058ac604"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "391cf328-de4d-4201-a4ce-c756710ceb84",
    "visible": true
}