{
    "id": "9a6679f4-e562-43da-8ff4-2f5a28023240",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFKey",
    "eventList": [
        {
            "id": "397129de-c5b5-491e-8cd1-fed1261ca51c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9a6679f4-e562-43da-8ff4-2f5a28023240"
        },
        {
            "id": "db6dfbf9-a6bb-46c9-a11f-035b260c2160",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9a6679f4-e562-43da-8ff4-2f5a28023240"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ed609840-1733-41a4-aceb-686b8faf0d78",
    "visible": true
}