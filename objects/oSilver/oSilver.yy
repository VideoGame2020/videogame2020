{
    "id": "adaae5c3-ad83-480f-b60f-c6ad30412c55",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSilver",
    "eventList": [
        {
            "id": "5f2bd2b0-7501-4069-9ee3-8af7aed47ea4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "adaae5c3-ad83-480f-b60f-c6ad30412c55"
        },
        {
            "id": "4fc8ad9f-bd4e-41af-b2ff-e3371e5b44e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "adaae5c3-ad83-480f-b60f-c6ad30412c55"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "689f2267-dfae-4d82-92ab-388e122cc832",
    "visible": true
}