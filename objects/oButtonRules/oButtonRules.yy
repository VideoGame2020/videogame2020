{
    "id": "38aa8a24-d19b-4707-9194-257034d09443",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonRules",
    "eventList": [
        {
            "id": "e1f0ad7c-7538-4a76-8621-7526504d0437",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "38aa8a24-d19b-4707-9194-257034d09443"
        },
        {
            "id": "3f36149d-4b56-4b6d-949a-a9fb1fe07373",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "38aa8a24-d19b-4707-9194-257034d09443"
        },
        {
            "id": "544f56ea-72de-4c5e-a9c9-c19ee1bfd112",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "38aa8a24-d19b-4707-9194-257034d09443"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f3492ea5-b996-4e7e-9d28-5d4b1fa98f19",
    "visible": true
}