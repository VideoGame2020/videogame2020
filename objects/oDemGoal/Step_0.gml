//send targets
tarCool--;
if(tarCool <= 0){
	chosTar= irandom_range(1, global.maxTar);
	if(chosTar == 1){
		instance_create_layer(x, y, "Targets", oAKey);
	}else if(chosTar == 2){
		instance_create_layer(x, y, "Targets", oSKey);
	}else if(chosTar == 3){
		instance_create_layer(x, y, "Targets", oDKey);
	}else if(chosTar == 4){
		instance_create_layer(x, y, "Targets", oFKey);
	}else if(chosTar == 5){
		instance_create_layer(x, y, "Targets", oGKey);
	}
	tarCool = global.tarCool_Lock;
}

if (instance_exists(oAKey)){
	if (instance_nearest(x, room_height, oAKey).y > oDemGoal.y-45){
		instance_destroy(instance_nearest(x, room_height, oAKey));
		sprite_index = sGoalADown;
	}
}
if (instance_exists(oSKey)){
	if (instance_nearest(x, room_height, oSKey).y > oDemGoal.y-45){
		instance_destroy(instance_nearest(x, room_height, oSKey));
		sprite_index = sGoalSDown;
	}
}
if (instance_exists(oDKey)){
	if (instance_nearest(x, room_height, oDKey).y > oDemGoal.y-45){
		instance_destroy(instance_nearest(x, room_height, oDKey));
		sprite_index = sGoalDDown;
	}
}
if (instance_exists(oFKey)){
	if (instance_nearest(x, room_height, oFKey).y > oDemGoal.y-45){
		instance_destroy(instance_nearest(x, room_height, oFKey));
		sprite_index = sGoalFDown;
	}
}
if (instance_exists(oGKey)){
	if (instance_nearest(x, room_height, oGKey).y > oDemGoal.y-45){
		instance_destroy(instance_nearest(x, room_height, oGKey));
		sprite_index = sGoalGDown;
	}
}