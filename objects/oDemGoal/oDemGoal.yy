{
    "id": "87c135f7-43c5-4d98-9fa9-5d17fef0a83f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDemGoal",
    "eventList": [
        {
            "id": "575c8dc9-e41c-4b58-90ad-14f12000d1c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "87c135f7-43c5-4d98-9fa9-5d17fef0a83f"
        },
        {
            "id": "cc23b952-c803-466b-8e56-0552e7be1688",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "87c135f7-43c5-4d98-9fa9-5d17fef0a83f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aa0a1a2b-2734-43cb-af91-bab6a2f48f7e",
    "visible": true
}