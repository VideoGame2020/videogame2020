{
    "id": "09e109e7-3db1-474b-8f6b-67f345350862",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oReturnHomeTxt",
    "eventList": [
        {
            "id": "2e4b3de5-2161-423a-83eb-7c2f2b4962d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "09e109e7-3db1-474b-8f6b-67f345350862"
        },
        {
            "id": "d99467f4-020d-42ed-9fec-ee0b57ff5a3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "09e109e7-3db1-474b-8f6b-67f345350862"
        },
        {
            "id": "ea1483ef-3175-4943-9658-6c6f1f3d061b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "09e109e7-3db1-474b-8f6b-67f345350862"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}