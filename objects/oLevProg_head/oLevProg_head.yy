{
    "id": "fd880b8a-bfa4-4726-97b2-e466e542513c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLevProg_head",
    "eventList": [
        {
            "id": "ad3cfd86-b53c-4d24-99d8-355abf407359",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fd880b8a-bfa4-4726-97b2-e466e542513c"
        },
        {
            "id": "a74b67f1-79d5-4e72-8f9f-1f0d555cf36c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "fd880b8a-bfa4-4726-97b2-e466e542513c"
        },
        {
            "id": "02a5d5f6-5334-4ed6-8451-b9dfe5f1887b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 10,
            "m_owner": "fd880b8a-bfa4-4726-97b2-e466e542513c"
        },
        {
            "id": "973fca59-dc2d-400f-9f32-26238c119bd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 5,
            "m_owner": "fd880b8a-bfa4-4726-97b2-e466e542513c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}