{
    "id": "602e4c98-c694-4a26-b932-f861089d930f",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fLetters",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "210f6046-f538-4614-826c-2feb1a83507a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "c6018d11-7225-48c2-8a22-4e0bd07ffd64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 367,
                "y": 98
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "0527b64a-946e-4e13-b8b8-c615db3d46c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 46,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 353,
                "y": 98
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "73abb5b9-1b5d-4b22-b48e-c8e54ea606a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 329,
                "y": 98
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3d2d75e2-b56a-4ab1-8d30-c493c7e1b30f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 307,
                "y": 98
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "47044a9c-ac55-427e-b354-027109b63071",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 46,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 273,
                "y": 98
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "0c4fbf0b-4edf-4e2f-b789-bcdb1724ec8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 46,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 246,
                "y": 98
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2af2ef91-f93d-4b2a-9dc5-41fdabc640ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 46,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 239,
                "y": 98
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "a32a4a3e-2c2f-4787-98e5-65b846c204d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 46,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 227,
                "y": 98
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c1d9bf12-0ffc-4547-b9c5-fe4eb45317fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 46,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 215,
                "y": 98
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "469586f7-b1e2-4dd6-843e-c54304f1aa8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 46,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 374,
                "y": 98
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "bf11623c-33fe-488c-b776-d21d328decc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 193,
                "y": 98
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "570eba1e-4b65-4487-bc2f-96ff22f71cb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 164,
                "y": 98
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4544cb8e-94b3-4151-8fd2-2972af7087c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 46,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 150,
                "y": 98
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a4362e74-6c1c-43a7-b1cd-dc27e4c55004",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 143,
                "y": 98
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "7091163d-5580-4188-b17b-1292b447629d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 129,
                "y": 98
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "25b3b21c-25dd-486b-87cd-2db210325fcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 107,
                "y": 98
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "243c1603-8120-4f47-9f2c-3d25f1c13670",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 46,
                "offset": 4,
                "shift": 22,
                "w": 11,
                "x": 94,
                "y": 98
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "71ed2fa5-c0c3-4b9c-9e8f-9e5ad6f9e5cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 72,
                "y": 98
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8fbc3f1d-e603-4d6c-a800-afcaa9cdc652",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 50,
                "y": 98
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2c291728-b22d-4cb4-addc-9d8968390fc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 27,
                "y": 98
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8c9ab6b0-895c-4788-9e6c-838785303a53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 171,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "683ec749-0846-4aa3-992f-fd7ccea2cc26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 390,
                "y": 98
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9e00013b-746f-4830-aee7-b31c22d68a25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 412,
                "y": 98
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "e30ecfb8-2686-4c54-88c7-9b4ee1f186d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 434,
                "y": 98
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "29da13ce-645f-4eea-8930-18293aadf63e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 405,
                "y": 146
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f52a830f-b617-439a-95bc-a1cbb1c4c6c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 398,
                "y": 146
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b2e3f5bf-15a8-4218-a2bb-6ec19f03f137",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 391,
                "y": 146
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "863f4946-44bb-4fb7-a592-c0dc0f6652ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 369,
                "y": 146
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "13bb01e4-ef23-404d-972e-6af6695b7225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 347,
                "y": 146
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6435f5b8-30a9-450c-bc41-52da0a743d1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 325,
                "y": 146
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7f7d178c-9057-450c-ab8d-40b6c22ed531",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 303,
                "y": 146
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "db246916-ad73-419a-bbc8-be77495de4a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 46,
                "offset": 2,
                "shift": 41,
                "w": 38,
                "x": 263,
                "y": 146
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "88e206a5-7b68-4952-8af3-862ae72a8012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 46,
                "offset": -1,
                "shift": 27,
                "w": 28,
                "x": 233,
                "y": 146
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e2255496-286c-42f3-aa4a-8422e496f482",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 46,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 208,
                "y": 146
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f1857435-6444-4f0d-a0d5-cc676a5384cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 46,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 180,
                "y": 146
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fc98c347-2a17-47e8-9231-6ea02dac87bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 154,
                "y": 146
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "dbe4b457-5f11-4c79-aecf-8e8509a417da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 130,
                "y": 146
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "60e9e5f7-a8c8-402b-abe4-780c676d51e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 46,
                "offset": 3,
                "shift": 24,
                "w": 20,
                "x": 108,
                "y": 146
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "42f43362-e93a-4290-97aa-84085b178ff8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 46,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 79,
                "y": 146
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6dd3ec36-7766-421e-8047-0046e9519cef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 54,
                "y": 146
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "60839803-a964-44e4-9572-214d56662906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 47,
                "y": 146
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "cebcb6bf-a39a-4f55-ac16-32e89db57a93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 16,
                "x": 29,
                "y": 146
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "fe10b785-e2fe-4d86-b581-e8de0d580a81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 46,
                "offset": 2,
                "shift": 27,
                "w": 25,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "5ec43edd-710d-4d47-8d63-a0da360c4ee6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 487,
                "y": 98
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0d72cbdf-78e0-4a02-9df3-401d62728f0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 46,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 456,
                "y": 98
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3f2f2829-fbf0-48ad-93a5-5c3daebf81a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c5673ae5-553a-4337-89e2-4abeb39257a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 46,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 479,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2ce515d7-ed4b-4199-804f-71a4c5c4b869",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 455,
                "y": 50
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e2bf602c-0a77-428f-872e-bf44ef617345",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 46,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b54ddb3c-63a9-400b-80f5-b20c32db9600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 26,
                "x": 448,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d2d39130-0273-42cf-bee5-7e3acb91bd1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 46,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 422,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1a1c3f17-ca43-4d44-816e-b0987fa2972e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 46,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 396,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f8009a9d-178d-480a-89cd-709128d13db0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 371,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f2ccfea9-7919-41d8-bfcc-ce3fead04473",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 46,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "fb974f2f-8991-421e-bce5-e3a9f385bc14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 46,
                "offset": 0,
                "shift": 38,
                "w": 38,
                "x": 302,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7e8d8b84-b089-489f-966c-8e38894664bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 46,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 273,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ed344ce3-1b2b-48af-8124-f4a39513396b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 46,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 244,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ccfe4897-b346-4a49-a55f-935ca8b8f267",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 46,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "70de9229-35ca-407b-975e-393de25ebcdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 46,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 476,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1741b18d-9d51-42db-ac5e-c40c83d1503f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8bba93b0-21c8-454e-a824-20300a6a0066",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7d6e9b8f-059f-46f4-b01b-3d74564645af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ec05c0fc-ffd2-4cb7-8408-7a10fbc140b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 46,
                "offset": -1,
                "shift": 22,
                "w": 24,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "85a57e68-9b6f-47c2-ab6b-70f4b4d4a0d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 46,
                "offset": 1,
                "shift": 13,
                "w": 9,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "0e9de12f-df75-4a02-8624-57d3eb2c5228",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "236f7b02-b391-4ae9-9586-248a49a28c87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c68b14e6-7895-4130-b2e8-9b607cee5cba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "6ec31647-aea3-4d58-af31-6b5d26e9d8b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "6648cd7f-5271-44c7-94a9-2b6e58a597e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6b3b0ecc-64b0-432b-a836-543e079851e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 13,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7d6619f4-67bc-41f4-9b39-8a8738171981",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 33,
                "y": 50
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d4c3c179-d37a-4bc2-902f-8c77b357c6cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 235,
                "y": 50
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "2ff3e4be-16a8-4dbe-828c-03f049b4f4c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 46,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 54,
                "y": 50
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "51129066-edae-45c5-ba7b-eb3a4fac828c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 46,
                "offset": -2,
                "shift": 9,
                "w": 9,
                "x": 424,
                "y": 50
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "10a53f77-0b62-4cd5-9df1-449be58cf953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 46,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 404,
                "y": 50
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "dd34c168-0a0c-4327-8f7d-cabd5d8e839a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 46,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 397,
                "y": 50
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "022b6abb-33b4-4f70-83ee-fdfa57105821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 46,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 366,
                "y": 50
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "72b8ed2a-acc4-4efb-94b3-76499378f671",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 346,
                "y": 50
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "cef3bc18-426b-427c-a32a-fce24c5221ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 324,
                "y": 50
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b73bafff-d066-4ab3-b5dd-51462765f90d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 303,
                "y": 50
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "30278b0f-ed22-4052-8c42-92f568d06085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 282,
                "y": 50
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "1fd88b8f-c7e4-4db9-a2a6-f4be5f1dd608",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 46,
                "offset": 2,
                "shift": 13,
                "w": 12,
                "x": 268,
                "y": 50
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "f24f17a8-ff15-4bd9-9585-c8a6fe9d6fa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 435,
                "y": 50
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b31db756-41e5-4b75-afd3-b01d6fb079ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 255,
                "y": 50
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "48a18a86-e8ba-489c-8ec2-8aa9e6de918d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 215,
                "y": 50
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b5bb67c1-adb0-44f6-8580-44e5d9c06389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 193,
                "y": 50
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f51d4e76-1fed-40fd-85b6-26684b2a2301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 46,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 162,
                "y": 50
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1a2153df-e21c-48e2-9c6f-9cf3f9616e84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 140,
                "y": 50
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0847de98-7884-40fa-aec8-9359289ee99a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 118,
                "y": 50
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "892666ee-2ae3-4daf-a51d-5a1b8a46917b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 96,
                "y": 50
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "38ad3fdb-25e5-4665-a332-1712a135b92c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 46,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 82,
                "y": 50
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "8c69f325-4225-4b5d-ac94-f513a616aa22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 46,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 76,
                "y": 50
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "fe89dc98-b184-4883-b9d9-30f0ef7df1d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 46,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 61,
                "y": 50
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0bf7da1a-f308-49e9-91a0-be48d902f4bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 427,
                "y": 146
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "3b5299e0-8c35-4794-a881-046f94a3ef1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 46,
                "offset": 6,
                "shift": 24,
                "w": 12,
                "x": 450,
                "y": 146
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "a1066e49-20d6-4fcc-8fb5-6759606b7bf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 65
        },
        {
            "id": "26cf6515-9f7e-464a-ab93-ede151888136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "4a3c1a6f-7b9a-45b9-976b-5be3cab9e119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "b025b61c-ff93-49bc-a722-9c4768aa1db7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 902
        },
        {
            "id": "0d84e5e2-651b-4f13-a198-fe5036dbce6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 913
        },
        {
            "id": "4ee91727-ebbf-4839-9e38-708f8ee5b4a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 916
        },
        {
            "id": "ca69cf01-54ac-465b-816b-b4baab9de913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 923
        },
        {
            "id": "25151976-8625-4a1e-9d34-9844f59e0a7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "3c088097-d690-4da7-a32d-74cbdda585e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "bcf11c6b-4b77-4328-8d0c-98f85ad2bde4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "fc81a5c2-188d-47be-99c3-070686a8c625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 49,
            "second": 49
        },
        {
            "id": "373a61a0-9546-4819-a2c5-f499bc2f0d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 32
        },
        {
            "id": "07ee4b21-f9da-4fb2-a026-8a8237ce60ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "cb0297ad-5026-4585-b77d-5dd08f15e65d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 86
        },
        {
            "id": "2c46a1f3-1813-4b34-948e-9385e4048a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "53b504c4-70e6-4eb8-bd00-5b7d6b3bb490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 89
        },
        {
            "id": "2c77c8e1-c1ed-4406-b64a-e87cbb53d38f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "56b7ede6-79e0-49f2-984d-ae3b76be2697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "ae36ecec-0bbe-4041-a97d-c815888fc31a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "6e3ddcca-49eb-4263-a275-3f2b57ec0988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 160
        },
        {
            "id": "77fc5a59-290e-43a3-8d43-9e20188e403e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "9d92bade-8cb9-4901-9eb5-ac226a91c62b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "086132ca-c12e-4cbf-8293-4aa0f94d64c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "63c99f25-ab6e-4aa7-ad00-58e4b1e2ace7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "1bb1775f-4df7-4661-909b-1921eff84bb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "2c75b358-e698-4a74-949e-9c337d00d02b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "523a6ece-a676-4f28-bc66-efd647f4ce96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 86
        },
        {
            "id": "c03201fc-e89f-4ca0-8de5-d1b14088c85a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 87
        },
        {
            "id": "7b45d664-dbe5-46d2-b455-3618db7bf9a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "b87a7289-f66e-4883-8380-1c738ea7714a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "834b10bf-8088-4b54-96d7-c5d22539c26c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "bee28649-b0eb-4292-9db1-e1ad9133aa59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "d9486ae1-d9e8-49f7-a298-bc3e327adffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "a7d3ac85-20fc-4bcf-baf8-9c21092abea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 44
        },
        {
            "id": "72406b6c-0ea2-4014-8a14-ea018802fd90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 46
        },
        {
            "id": "0fece1a5-6c29-4ce6-958a-098800760d71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 65
        },
        {
            "id": "94673e09-43cf-440a-ba8e-65c91140d313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "11288aff-dbe8-469a-95d0-fc690c8d3a76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "e4de7dc8-9610-4a6c-8e52-3df7315e130d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "2523b398-82dc-4ab9-b819-247a7154b435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "2f2fa1ee-bfa4-402c-95c5-91341e0c85e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "3f05c3b6-e0e4-42d5-9e89-af6a76c4a512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "377f68a3-3c56-4923-b96f-d8e63af1f700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "382b1106-2d3b-4e55-a701-9e35d8c6d8ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "df733165-63b5-4c01-aaa3-7613d59d1b26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "54573abe-34b9-49c0-900f-6c5b74d2341f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 58
        },
        {
            "id": "067d6822-6b8c-49fa-86e8-6c0e034cb8f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 59
        },
        {
            "id": "ca7ffd98-528d-4fe2-bb97-f189012a918f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "b7ebdd4c-5a7e-44fe-8a3e-c54dfaae9a1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "2a883eb9-c165-4e01-8182-e5fddde6ddce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 97
        },
        {
            "id": "a175621b-58e2-4afa-a48e-7c2ca3dbfb63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 99
        },
        {
            "id": "acbaa357-fddd-499a-85ef-f7850e2d6795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 101
        },
        {
            "id": "0a1c2e35-ae58-4c42-b125-4aacaf65137d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "3beff8d7-38fc-4324-ac7f-539f12b1aa2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 111
        },
        {
            "id": "2df12962-585c-41e6-81b9-8320fdd8016d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "12dc972f-5737-44de-b89f-26a937e92620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 115
        },
        {
            "id": "f960fa40-28d9-4b8c-95c1-5a0b45325df2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "672d71ed-8175-475c-99b6-b39a07552e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "3c257cbc-ea08-434d-9214-32e2b98a3333",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "b7f3211c-a227-4355-9182-f72cb0d0bc57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "9efd4231-0d27-401d-a6ea-309c5be4092d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 173
        },
        {
            "id": "513cbd0d-e2ac-4608-adbf-6c4d52fdbce5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 894
        },
        {
            "id": "6b8fdedc-e800-42c8-8a67-a0deee355698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 44
        },
        {
            "id": "8b2cc336-9611-4c6d-9303-923b5f8c9349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "69e2ddd7-ac13-40cd-ae95-6983fc426c13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 46
        },
        {
            "id": "496bf5ff-098b-43d5-ad8e-f3a404808bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "81131239-7b78-4a16-90a8-76c95401f08e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "4d5216ed-81f0-4671-9952-196ad6fe3630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "622a3f3d-a3e4-4454-80f0-571db6c49c7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 97
        },
        {
            "id": "673b9a6a-6702-47aa-bf6b-db85136d56c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "2595d166-c861-4ca1-9e6b-bd8b602f9ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "e01c9fdc-30f8-4b24-b29c-c6ec99164703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "b9e7a593-b790-4a51-99d9-95af94348767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "60c3eae5-ab3b-4c4f-a10f-83a2e01a3730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "09e12ef3-611a-4335-998e-8690d39d9ed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "88e4df7b-491e-491c-bc52-c1d8ca00dbe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 173
        },
        {
            "id": "2578297d-c107-4694-ad2e-da53c35989cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "0dc1eced-fcdb-4beb-a2fd-0f4fc037d72f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "097436ee-fa9f-472d-bb85-7ace67bdf772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "4d371176-a349-4783-8fc6-f5235315ff8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "7f664d7f-4d8d-4c5e-9c5d-4074269bd82f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "fe680bcc-d322-40bd-b36f-417440dc03e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "e52c76b9-d457-4003-88da-882b844cc2f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "43bd96ad-248c-4b35-a1f3-aab6ae72dc76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "a5b4eb6b-5dd6-444b-af5f-af1ea56578ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "ead18c89-9143-40e0-a193-c8b0be57256e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "2ad62db8-55ba-419c-b86c-1ba626bfa798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "c7aa4cad-8335-4111-875e-9190129c90a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "dccb8df6-f931-4a65-8610-ca38ab61d2ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "332c38b7-243f-48bf-8fa5-6a8953a81a14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "b3a18f32-e8a9-4bcd-85d9-ae6a57385f0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "ea33cfd6-df57-4938-80cf-dba77e4d475d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 44
        },
        {
            "id": "092942ac-ef0a-44c3-acd6-3c2862a0931b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 45
        },
        {
            "id": "04052e65-330a-4a44-b54f-ce346f416cfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 46
        },
        {
            "id": "9634c070-0289-45bb-92a3-45616d8ad3c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 58
        },
        {
            "id": "64c80d62-f9a5-4700-ae9c-6ec65325a40a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 59
        },
        {
            "id": "ee4bf3bb-ccec-4833-be2f-32ae3f3f791d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 65
        },
        {
            "id": "ba7338c6-6dbe-4513-8974-021a68dbd5d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 97
        },
        {
            "id": "4c3523ea-c54f-4032-a8e7-01d0a709832b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 101
        },
        {
            "id": "493aeba2-d36e-4d64-8305-4098df01ce6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "6c23ba7e-14cc-4b9e-bbbd-e175f42e7342",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 111
        },
        {
            "id": "7023552a-6e0c-4bd3-9da6-b5dd1839dc86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 112
        },
        {
            "id": "d847a15e-08e5-41c1-aa28-150db0d9b283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 113
        },
        {
            "id": "692553e2-e49f-4cfb-93f6-0f97fb02ef54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "4a7228b0-e066-4169-b692-f0c9d8ae79ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "0b851135-05b1-4280-bdc1-63e195e1577f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "a585ee6c-3cda-4c7f-a4ae-ab44f46dd1db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 173
        },
        {
            "id": "9fd643c1-6ba7-4608-8630-da532459e683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 894
        },
        {
            "id": "761f76ba-d6aa-40da-a365-28e701dd635f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "6c1cad6c-126d-4d14-bd01-a463b23aa076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "1ca0dd2f-72c9-466c-a936-773655980aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "ac5b669c-f2c4-44ae-94f7-d0b6254ea398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "fb296ab0-148f-4f1c-81b4-22525f1e0a59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "a3cddcec-3c7a-4517-96b5-76ec7c7663eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 44
        },
        {
            "id": "b13e70ad-0f26-407a-9e1e-0f50bdf81846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 46
        },
        {
            "id": "3e0f6dff-9ea5-4ea7-a6fe-c910635d64ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "b4df6d63-2f5f-4f1e-b208-37aba1e46cc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "053b84dc-8d0e-459b-b276-3ed5cbdec2eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 44
        },
        {
            "id": "d4fe31c6-d7a3-45f9-9c4e-b231c3f4a8ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "Hold for A",
    "size": 30,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}