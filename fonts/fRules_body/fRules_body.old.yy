{
    "id": "e11f1b2d-040c-42ec-971d-2a78fd259ec8",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fRules_body",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2fe7e62e-5ff6-4c77-866c-443743cd04e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 61,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e57bee5e-2d94-4d70-b9b6-acf91ec8e83e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 61,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 222,
                "y": 191
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ad20013a-11ae-4264-8303-50dac71ab2d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 61,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 205,
                "y": 191
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "42839c65-f4a9-40cd-94f6-b5ca08ee0a57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 61,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 174,
                "y": 191
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "023b992f-c57e-403b-8d5f-8a7a05bfdbde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 26,
                "x": 146,
                "y": 191
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "993ab761-b59a-4296-9361-25793474e55f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 61,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 103,
                "y": 191
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "63eeb6ff-e5cf-4a8c-ae8c-cb0718096086",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 61,
                "offset": 2,
                "shift": 35,
                "w": 33,
                "x": 68,
                "y": 191
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d99300c9-6f48-45f7-b6fb-1a26afceb6d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 61,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 60,
                "y": 191
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "cc98447c-5b63-48e8-8d52-797fc108cf61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 61,
                "offset": 3,
                "shift": 18,
                "w": 13,
                "x": 45,
                "y": 191
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "44235537-754d-4274-8d44-4e2af0b71534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 61,
                "offset": 3,
                "shift": 18,
                "w": 13,
                "x": 30,
                "y": 191
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "887cb51c-b435-4262-a503-d5cbcb4e4bef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 61,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 231,
                "y": 191
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "092a065c-90f2-4556-9ab8-48df15bbcb85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 26,
                "x": 2,
                "y": 191
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6b4fc6ed-88e8-4de8-8ec7-55cd5e4a46a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 61,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 472,
                "y": 128
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "220a46ce-baaa-46bd-8321-e4941d60ae15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 61,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 455,
                "y": 128
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6f9676de-8aa7-4371-a87f-98e6a1881604",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 61,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 446,
                "y": 128
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "083f6321-2ab8-4d82-858f-fafe7bc3e9a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 61,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 429,
                "y": 128
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1f4bd5de-5ea2-4d05-99e2-d567bdf7dd1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 402,
                "y": 128
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ba578404-6c57-47a2-932a-20722103fcf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 61,
                "offset": 5,
                "shift": 29,
                "w": 15,
                "x": 385,
                "y": 128
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d53fa8ad-4a26-457b-94e1-5ea5845525fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 26,
                "x": 357,
                "y": 128
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "04cc5285-5790-4734-9448-f2eb023da009",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 329,
                "y": 128
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "c9543719-2bdf-4bef-8fbf-3206d1db29ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 61,
                "offset": 0,
                "shift": 29,
                "w": 27,
                "x": 300,
                "y": 128
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7ecf37a4-d2a7-432e-860f-df3233d9b2e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 481,
                "y": 128
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "68796f93-8777-4c0f-99ad-f4c7de286626",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 251,
                "y": 191
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "07ecbe27-2744-4efe-b7bb-669524ac4118",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 279,
                "y": 191
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "cf03cd8b-dbcc-4531-81f2-4dde02780ad8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 307,
                "y": 191
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8c328cac-377b-4c97-a5e3-1296e83c090c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 424,
                "y": 254
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a3b9b7aa-086f-4944-80d3-83b304ccb4e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 61,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 415,
                "y": 254
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ff81bfca-acc8-4fc6-8ea1-d0f62ff2fab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 61,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 406,
                "y": 254
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "950a4120-ff00-4fa0-a8d3-d4d6a04204e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 377,
                "y": 254
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "860defb4-0a00-4a0c-8bd1-dc7c49c22de4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 26,
                "x": 349,
                "y": 254
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b48eb609-33f2-44cf-b7ef-3db73440570e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 320,
                "y": 254
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "fe4b4abb-dcf2-46c9-9c3b-500de55c6793",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 293,
                "y": 254
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4ba06fb9-572e-4531-b7c5-bfbfab6bf6e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 61,
                "offset": 2,
                "shift": 54,
                "w": 50,
                "x": 241,
                "y": 254
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "637309bd-39e1-404a-a3eb-fcff3c422383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 61,
                "offset": -1,
                "shift": 35,
                "w": 37,
                "x": 202,
                "y": 254
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "66086270-fc34-40b7-a891-dd5b0c6ea142",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 61,
                "offset": 3,
                "shift": 35,
                "w": 30,
                "x": 170,
                "y": 254
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f504dea3-e0f8-4246-aaa3-7854a4a78ce1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 61,
                "offset": 2,
                "shift": 38,
                "w": 35,
                "x": 133,
                "y": 254
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "86d136f2-4f92-481b-99b8-64840811e721",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 61,
                "offset": 4,
                "shift": 38,
                "w": 32,
                "x": 99,
                "y": 254
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "653aaaa3-79df-4bcc-a70f-e58c916ba675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 61,
                "offset": 4,
                "shift": 35,
                "w": 29,
                "x": 68,
                "y": 254
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "20114ea7-b2a2-4ecf-91d6-820fc046c07a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 61,
                "offset": 4,
                "shift": 32,
                "w": 26,
                "x": 40,
                "y": 254
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f8d25792-fafc-480b-8b71-8913106d59ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 61,
                "offset": 2,
                "shift": 41,
                "w": 36,
                "x": 2,
                "y": 254
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d33aa2a7-04ac-4b39-bf6c-199395e1aa0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 61,
                "offset": 4,
                "shift": 38,
                "w": 30,
                "x": 469,
                "y": 191
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8d746b5a-f4db-4016-8cfb-82e942326324",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 61,
                "offset": 4,
                "shift": 15,
                "w": 6,
                "x": 461,
                "y": 191
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "651fcc38-cd85-46f8-9077-544f7c23a2bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 61,
                "offset": 1,
                "shift": 27,
                "w": 22,
                "x": 437,
                "y": 191
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6ba9018b-9686-4bae-b549-5597997f6516",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 61,
                "offset": 3,
                "shift": 35,
                "w": 33,
                "x": 402,
                "y": 191
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "882404dd-e0ff-4dfe-ba64-5fbdaba9014a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 61,
                "offset": 3,
                "shift": 29,
                "w": 25,
                "x": 375,
                "y": 191
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8b845ecc-752d-490c-b931-b5839563d807",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 61,
                "offset": 3,
                "shift": 44,
                "w": 38,
                "x": 335,
                "y": 191
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "cb9aebef-9518-404c-a2d6-737e2c48a2b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 61,
                "offset": 4,
                "shift": 38,
                "w": 30,
                "x": 268,
                "y": 128
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f60ba8c6-b247-45de-bfbe-bdfb1b2ee84b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 61,
                "offset": 2,
                "shift": 41,
                "w": 37,
                "x": 229,
                "y": 128
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "df0ca820-790f-4276-9567-355ef96cf2f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 61,
                "offset": 4,
                "shift": 35,
                "w": 30,
                "x": 197,
                "y": 128
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "31523454-a46d-4b6f-b6d4-09a6dc0fb857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 61,
                "offset": 2,
                "shift": 41,
                "w": 38,
                "x": 117,
                "y": 65
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4f7a96e7-5628-4143-8076-9a4f70955425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 61,
                "offset": 4,
                "shift": 38,
                "w": 34,
                "x": 68,
                "y": 65
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "eb2ddcad-28f5-4e28-81db-6a5cc4a3429e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 61,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 35,
                "y": 65
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "02a9ae43-594a-443d-bad4-d10da1cfb438",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 61,
                "offset": 1,
                "shift": 32,
                "w": 31,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8855fd2c-bcb3-4ed9-8070-15e8c77bdc35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 61,
                "offset": 4,
                "shift": 38,
                "w": 30,
                "x": 476,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "56b9bf11-f7e0-44ec-9759-438f1b25084a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 61,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "8af4c554-9493-43bc-8fb7-b0edf07ade34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 61,
                "offset": 0,
                "shift": 50,
                "w": 50,
                "x": 387,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3525520c-f1a5-4986-af3c-60d708c662ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 61,
                "offset": 0,
                "shift": 35,
                "w": 36,
                "x": 349,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f00212b0-8dae-41af-91fc-0a440004bf34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 61,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 312,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "35adc325-5a5a-446c-b580-66ea7845a588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 61,
                "offset": 1,
                "shift": 32,
                "w": 31,
                "x": 279,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d4ed7b8d-ec58-4970-a969-50820bf08290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 61,
                "offset": 3,
                "shift": 15,
                "w": 11,
                "x": 104,
                "y": 65
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "208f830c-ab68-4eed-94ad-f04d8d592b78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 61,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 262,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "792841e7-183f-48bc-8a40-2c3e89f63a08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 61,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "afbfcc53-0aab-4d1a-a56e-ed1d29052cdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 61,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0341b3b6-1459-47a4-b6fe-b18a44c071e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 61,
                "offset": -1,
                "shift": 29,
                "w": 32,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d8ce3f86-6332-45b0-ae92-0853485833ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 61,
                "offset": 2,
                "shift": 18,
                "w": 11,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "42f5fcbf-c5fb-490c-9908-366b9218fb6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1c039df8-a4f2-4a46-94e3-5d82e992e5cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 61,
                "offset": 3,
                "shift": 29,
                "w": 25,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d0a916f8-56bb-4858-bdac-b6b89b013090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 61,
                "offset": 2,
                "shift": 27,
                "w": 25,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "04606071-5ee9-4b3b-86b4-57c0d1f31ce5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 25,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4c330ee3-c283-4446-94dc-ed2adec8a661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bccaba77-7607-492c-92a4-17711b773a60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 61,
                "offset": 0,
                "shift": 15,
                "w": 17,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ec8a36c9-4acc-4b0b-a5c9-b3c79438f1cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 25,
                "x": 157,
                "y": 65
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "5de04f38-2604-458d-8625-148432dc4757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 61,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 413,
                "y": 65
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1d67530b-011b-4250-a908-6c684d0e0093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 61,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 184,
                "y": 65
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "734ec6f3-b6d7-44a7-8e43-9dc995c62bf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 61,
                "offset": -3,
                "shift": 12,
                "w": 12,
                "x": 157,
                "y": 128
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "50634773-9db9-4367-b38f-396bf4ce54bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 61,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 131,
                "y": 128
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "94d093d6-61aa-4d98-8455-ffd0bafd67b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 61,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 123,
                "y": 128
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6d2b89cf-b1e2-4fe6-a0e1-f021a870fe24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 61,
                "offset": 3,
                "shift": 44,
                "w": 38,
                "x": 83,
                "y": 128
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a3cc9b9b-41a1-40e0-8b68-5cc2b5d4dac0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 61,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 58,
                "y": 128
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "21b5e1c4-0a95-46fb-b052-83f6bd46d95b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 29,
                "y": 128
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b5fd7170-e372-4e73-8eda-dbe2a10372ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 61,
                "offset": 3,
                "shift": 29,
                "w": 25,
                "x": 2,
                "y": 128
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c55e83d9-462d-433b-b607-baf3555ac132",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 25,
                "x": 473,
                "y": 65
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "99899188-dc74-4e1e-b5a3-d2dcbc31ccdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 61,
                "offset": 3,
                "shift": 18,
                "w": 16,
                "x": 455,
                "y": 65
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a9d8f121-f7b7-49f9-9dd4-9efe7c89db0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 61,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 171,
                "y": 128
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c541cf2a-cd83-4f75-9d21-5238e631457e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 61,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 438,
                "y": 65
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e27de6c1-1da4-4ee4-82a1-112c8a481fbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 61,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 388,
                "y": 65
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "88c153d0-3bcd-42b0-bf69-2b9bc751c691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 61,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 360,
                "y": 65
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "036e14af-682f-4f64-8ae4-7faf1dd27322",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 61,
                "offset": 0,
                "shift": 38,
                "w": 38,
                "x": 320,
                "y": 65
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "acd1b353-a9cf-4d96-9867-90d861a975ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 61,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 291,
                "y": 65
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c8074607-c159-4c3c-9e92-e0e491e5c160",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 61,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 262,
                "y": 65
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d5e2dfc8-de73-4f9e-bb96-ffe104b97546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 61,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 235,
                "y": 65
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "299563f5-3135-4ea4-87ff-1dde7ebecb0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 61,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 217,
                "y": 65
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ef99cc4c-884e-4ca5-a8ab-40f9de6d0827",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 61,
                "offset": 4,
                "shift": 14,
                "w": 5,
                "x": 210,
                "y": 65
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9beabb4c-4894-4e55-8fb2-5a1d4ba9b194",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 61,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 192,
                "y": 65
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "622f9444-47bf-4e31-bedd-6b409d97d284",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 452,
                "y": 254
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "094f1049-25d4-47a6-b615-3c994d791289",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 61,
                "offset": 8,
                "shift": 32,
                "w": 16,
                "x": 481,
                "y": 254
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "d538cde5-a314-4ce9-a638-2b99b30435a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 65
        },
        {
            "id": "1d063835-729c-4f5d-8394-4c4eb0365acf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "d06d7e8d-6335-4a4e-af69-e6a99ef2dcf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "3fc5f830-5a04-4640-95b1-fe651dd5fe2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 902
        },
        {
            "id": "8360ade2-1da5-49f8-8a9f-e888b871d174",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 913
        },
        {
            "id": "8f8015fb-d08c-43be-b137-52e236730863",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 916
        },
        {
            "id": "e3d3f924-20d5-4f01-a2e1-91fb083fcac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 923
        },
        {
            "id": "90bd7daf-49d9-4cba-82ea-f7d0d1a7a80a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "bc3de1f0-5459-4ce8-9fa9-41964e8e841b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "c6ed9b86-adf7-4f08-80d8-5d081108054a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "fe1e246b-0634-44bd-b21f-dae8569f215b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 49
        },
        {
            "id": "785bc191-b1f8-4739-90a9-bbffc6ef2807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 32
        },
        {
            "id": "105e91f2-c1ba-4356-b5f6-7c47e436af13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 84
        },
        {
            "id": "58f8ad26-f353-46c9-9d5c-7a2473600724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 86
        },
        {
            "id": "9ae05703-a9ca-4f47-a72e-a1cb7f5c75b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "b0cc590a-8cea-4b76-99d6-ad1f08b03d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 89
        },
        {
            "id": "02a67960-cb71-4e14-ad67-bc942229dac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "236a0795-57d3-4d2f-8054-7ab805bb3f87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "d72d722d-0b64-4c72-8d64-9e796183bcb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "dced486c-f6cc-4bd5-9e98-949daba3bf08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 160
        },
        {
            "id": "4f4ed7f4-3040-4c53-b7b9-63f49b02ab14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 8217
        },
        {
            "id": "1167b987-ab8b-4cc7-96fc-615eb4778880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 44
        },
        {
            "id": "57d09cd4-40c8-4aaf-9628-af2e7f9da2fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 46
        },
        {
            "id": "f3a0b717-addb-4423-ba3c-d1e0e433b262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 65
        },
        {
            "id": "abede82a-3aeb-4954-a051-638a53034f93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 32
        },
        {
            "id": "911bd9f0-aa6e-44da-a21e-bc965c860812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 84
        },
        {
            "id": "e1719069-45aa-46d1-8634-efb1e63ab19c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 86
        },
        {
            "id": "75a4e902-a1ab-48d2-a36b-0c7b7941f648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 87
        },
        {
            "id": "e3f5f3d2-acce-4b15-a0a0-5088ec4a0578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 89
        },
        {
            "id": "4e779a2e-b885-46d0-b40d-909c26c2cc17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "8b6d52db-bbb8-4898-9c6e-451d77159304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 160
        },
        {
            "id": "80a0934b-33e1-47ce-b0fe-f07f8f1a0aed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "61622a2b-cd35-4bd3-b964-1f8645c032ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "dd6973ca-645a-457d-89d9-d09741bc14c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 44
        },
        {
            "id": "48f46e3c-3932-4909-9004-ab5a19ced048",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 46
        },
        {
            "id": "3cc6dbc4-db3d-46c2-a179-0d9931ac341e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 65
        },
        {
            "id": "0873ad01-df12-4255-98da-3340b643a007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "5ba0006e-02a8-4bfe-b199-8c22cd19d0bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "4b0ff1ad-2631-410d-ad74-90f856f3f98a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "ae006089-c439-4b86-845d-cc17c26385c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "36a7f0ab-1cd7-4804-bf7d-d027b300f49e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "dea634b1-c51d-4dc2-bce3-a9227ff6cf1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "30e77740-de0a-4647-ad2a-336b3ce97d7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 44
        },
        {
            "id": "aa0412c7-be20-4d5e-b557-d52acbca201d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "08e69ad8-20cf-4087-824c-98f9f4975a11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 46
        },
        {
            "id": "3ec5f874-a54a-4063-987b-de2ae9f5adef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 58
        },
        {
            "id": "412830c0-3533-44a0-af16-f5974fd3c83b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 59
        },
        {
            "id": "07debf70-bf7e-45a4-bc04-dcdcc8bf9d3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 65
        },
        {
            "id": "374eff3c-e24c-4d4e-bea2-71f97d041677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "a4a6d055-87dc-4abf-9eb3-edff38894c16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 97
        },
        {
            "id": "20492fe8-3f14-4fb7-acd2-5e689fac7f35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 99
        },
        {
            "id": "1dea525c-caa6-4d73-afd1-570cfc88b854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 101
        },
        {
            "id": "91aa3b00-7bb3-413c-a877-ed084470abf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 105
        },
        {
            "id": "9313978d-22dd-4603-bb11-7a5d0feab33f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 111
        },
        {
            "id": "e9d13ab8-6e66-4cd5-aa8e-fa55b4624269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "0e8f27ac-c125-4598-9bcd-21d37c2fb486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 115
        },
        {
            "id": "a09b8205-05f6-4155-b3e6-52ecafe4786c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "f17a0c98-e850-4103-9d48-65d87007ab6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 119
        },
        {
            "id": "fe832e78-4437-4c75-b4af-95276c60cf3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 121
        },
        {
            "id": "b0130436-7fa0-4f3f-ac13-b4690439cc0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "b884f0ac-23c4-488f-8522-731efe1a8cd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 173
        },
        {
            "id": "2eca1bf8-2562-4791-b649-4f5f7920e371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 894
        },
        {
            "id": "bb857934-5960-410c-a42d-0cff7b278338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 44
        },
        {
            "id": "95f4b56a-8943-41c4-b46b-831c3417e268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 45
        },
        {
            "id": "7b25f79c-acc5-43c7-b500-4ac042ca589d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 46
        },
        {
            "id": "5f3d8e98-a35b-4861-9b83-d7b0d723e731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "0fbc7105-ca41-436a-91f2-5c58859ba2c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "5e3ca6b0-0d5a-440e-8d5d-9d1fe3cc8cc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 65
        },
        {
            "id": "9aca5cff-658b-4a29-8b81-354a23fd89d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 97
        },
        {
            "id": "d4395546-62d7-491d-9810-e1e92873c741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 101
        },
        {
            "id": "5a5a100e-65d9-4c73-bec8-8ef19a92ade4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "02adc724-36e9-405f-b4f8-dd9f494ae6a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 111
        },
        {
            "id": "6f13734e-f12c-4162-a595-fc180f69c1ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 114
        },
        {
            "id": "156883ff-ebe4-4ca0-ad84-34c5bc0a237d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "568ea30f-b789-40e4-aa01-840ed0d7767b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 121
        },
        {
            "id": "333746bc-26bd-4fec-b94d-bd1e5eadd860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 173
        },
        {
            "id": "f07ef77e-a957-49df-a04e-99131b6056ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 894
        },
        {
            "id": "624d550a-0bef-4a2a-bb03-4a20d1cee0ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "20d3aacc-15de-487f-a452-18f8c63279e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "5b0e04bd-a956-439f-9b38-36cdede14dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 46
        },
        {
            "id": "dd0c4c69-e787-4212-aabf-6058d043c27f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "189eeea3-3bc6-4b2b-829a-1862564be7fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "8e0c31f5-29d3-41a2-bf34-5f43adda1429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "f7bdb4b1-51d1-4faf-a2c6-b6bd4623d96e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "23d4519c-b117-4664-8d5c-9bb75382ab6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "dd084510-d34a-4b03-99a1-5af0f5558f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "c6bdd263-50ea-4678-bf11-746624caf158",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "d2aa328f-e684-40e0-b150-075980d226ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "f74b1d06-48b9-4ec0-abf5-b3a6e2e6985b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "eff7963e-f6a2-4693-82d7-9f7bda7d2970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "74b271e6-7bb7-4587-876e-143c0870624c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "6b1abd84-e48d-43fc-9ea8-7a8d892fb40e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 44
        },
        {
            "id": "656bab3c-6d4c-412f-875b-96df6e5ddcc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 45
        },
        {
            "id": "8daeffb5-95ed-4862-83d4-f348061342a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 46
        },
        {
            "id": "2effea1d-69ab-4961-9bad-1577acc9a0a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 58
        },
        {
            "id": "8b34e34f-6c38-4c7e-a3c0-961975206d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 59
        },
        {
            "id": "dd7dd27c-e154-4b98-a7bf-2dc1e91905d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 65
        },
        {
            "id": "46ebf83f-7029-4a45-aed8-60e565d05225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 97
        },
        {
            "id": "962c08b7-b590-408a-9521-9ca626675fdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 101
        },
        {
            "id": "1c06f75c-bd66-4f81-a7ff-81ef306d374b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 105
        },
        {
            "id": "caa58dd7-e1fa-4ec1-ac41-00678f5b8f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 111
        },
        {
            "id": "c5a09c31-ca26-4f00-bb44-7c20096f9977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 112
        },
        {
            "id": "cfa29270-1b1a-42c8-80e7-99eae9362fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 113
        },
        {
            "id": "12cf6073-055a-41f6-a06c-e708142be447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 117
        },
        {
            "id": "c45808fa-8b50-4960-8444-8fdbd28cd359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 118
        },
        {
            "id": "0ed5fb5b-0ee6-415b-84e9-7520a4041228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "29210668-c8fb-4749-b24f-7b07653db041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 173
        },
        {
            "id": "7a0f182f-a0ca-4278-9af9-a2c8d5ec04a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 894
        },
        {
            "id": "df64d721-a28d-4382-a1c6-ddf917a2493f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "8e049104-fe9f-445a-93f5-6ae28e4281a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "a8f61663-78ca-42f4-b680-d0e19bdbbc40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "34f887ff-58ea-4529-962e-b56fdbf8164c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "dc000922-4d4c-4eea-848e-fad373fc8b7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "59379474-afdc-410d-9621-2d3b4c10cc6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 44
        },
        {
            "id": "90938a69-3ff7-4bb9-9964-66737ba4334c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 46
        },
        {
            "id": "e8766ece-55f5-4889-8e8a-04a5c5fd1e9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 44
        },
        {
            "id": "c7883da2-35bc-4349-8460-b8a6a8e14f76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 46
        },
        {
            "id": "18f489b8-a11e-48f3-8211-adca756aae97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 44
        },
        {
            "id": "646c0930-2e1b-4400-bf7f-80acb948fbdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "Basics",
    "size": 40,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}