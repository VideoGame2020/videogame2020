{
    "id": "e11f1b2d-040c-42ec-971d-2a78fd259ec8",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fRules_body",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "41d7eaff-3190-4365-83d1-8586f22e536f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ff2f3402-279b-4906-a707-bb3b4823e1f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 2,
                "y": 134
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6cae373e-98ba-42da-ba69-98442df4e80b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 31,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 240,
                "y": 101
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d20bd5ae-7c9a-4828-8d01-f64195e17962",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 223,
                "y": 101
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "fff6aff2-0f09-446b-8e5e-c9588ac306ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 207,
                "y": 101
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "66813739-fa49-4d4b-b549-7173242e3f77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 31,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 183,
                "y": 101
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "4ad77ef6-be18-4e3c-8b6f-b48e5d3fbd91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 164,
                "y": 101
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9a13eeea-cd97-4c1d-8cd6-e5340c67795c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 31,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 159,
                "y": 101
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f6047f38-ced5-406b-b15e-819fcca3beed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 149,
                "y": 101
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "cba6dacc-3e45-4072-85a3-c2f4ad618845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 139,
                "y": 101
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "bc6ca8ef-c58a-4de7-8ed2-bc96af9f59fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 31,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 8,
                "y": 134
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "77031e6b-eebc-4962-8d0e-d4b47b10ff01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 123,
                "y": 101
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "03f71532-3411-49d9-9f39-42a2a75aae47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 102,
                "y": 101
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "caa8e54f-5efc-4bde-bb29-0b5fc9f76ac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 91,
                "y": 101
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "01b45463-db3e-4b43-956b-7bb62ec56ebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 85,
                "y": 101
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "aa007158-92cc-44ff-a0b8-71369856d05f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 75,
                "y": 101
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1fce320e-de58-4136-98cd-411f9b107215",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 60,
                "y": 101
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8b070875-9df2-482e-89dc-27572f300050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 31,
                "offset": 2,
                "shift": 15,
                "w": 9,
                "x": 49,
                "y": 101
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "66e6aeeb-f3c8-48e2-bdb9-12aa26ea10d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 33,
                "y": 101
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "bb8c8021-04a1-4cc2-a856-2322974d502a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 18,
                "y": 101
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "cb000212-512f-4f6e-bf52-49d4782ece7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 101
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "0cf0ce5f-1eff-4034-bb5a-0000ab4167f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 108,
                "y": 101
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "8448774f-455c-4052-b510-aab2e01fda80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 20,
                "y": 134
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "295b8488-60cd-4266-81ad-041670829af1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 35,
                "y": 134
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4b032a23-9983-45ca-97f4-d37bddf4ed0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 50,
                "y": 134
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7ed4a2cf-75e5-4d36-999a-3ef41a8e4a69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 145,
                "y": 167
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d5dbf706-26cd-4639-9dc4-ccc034acee0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 139,
                "y": 167
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "21aef41b-c71c-4faf-a2ae-372dfbb8d05f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 133,
                "y": 167
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b2ed8ea6-e79a-4ba2-a5dd-d0b12c28bcfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 117,
                "y": 167
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "71163ab1-d646-4173-b256-583be7410972",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 101,
                "y": 167
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b2ae9069-e82d-456c-bb4b-a77f64348335",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 85,
                "y": 167
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1c8b74d4-15ff-413f-a0bc-af30f86e84cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 70,
                "y": 167
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b9a8eaf4-3e6f-466e-8880-83ddd5d6a565",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 31,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 42,
                "y": 167
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "91026d1e-0d5c-495b-b7e7-4e19dc7e4933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 31,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 20,
                "y": 167
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "43c9dfb2-0014-4b91-ac97-9068fabc9417",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 167
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "7801f22c-874c-4a77-9728-62021c0a5c73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 232,
                "y": 134
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c906f933-7949-478d-89e2-3e9de1733890",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 213,
                "y": 134
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8627aadf-0873-4db9-89ef-3b51075b9ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 31,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 196,
                "y": 134
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "459a756e-1b96-4165-afdc-ebb186227281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 31,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 180,
                "y": 134
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "df46ef81-ddb5-4076-9b0f-8e96a296732a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 31,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 159,
                "y": 134
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "58405e07-bb40-40ea-b28a-7d0649f61219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 141,
                "y": 134
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d3fac663-ca40-4dcc-851f-ce7fe1e32da1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 135,
                "y": 134
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d074f7ed-a26d-49fe-96a1-8be0bb360d08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 121,
                "y": 134
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "49e5549a-aeff-41bb-a81f-8c883ab9c94f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 102,
                "y": 134
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a46f1ae5-be6f-4fe1-bc84-362b61910898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 86,
                "y": 134
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "dcae23ed-88f5-4127-a1cb-8f9ce3527b73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 31,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 65,
                "y": 134
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "78e5e158-f7a4-43fd-b15b-7e8ed782fd20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 237,
                "y": 68
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d03215a1-9fb0-4946-8212-f9f5705ac519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 31,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 216,
                "y": 68
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "4ea9c369-ce66-4766-b905-dcfb98c47c28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 31,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 199,
                "y": 68
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "87433c2b-a72c-4448-836b-cd78d2a8b198",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 31,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 105,
                "y": 35
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e52a6548-3c59-4f77-ab1d-a1afd5d5dc13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 76,
                "y": 35
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e60df64a-c769-4a5e-a641-fb1e7fadaa1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 58,
                "y": 35
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "588c3ef1-1bc9-4354-bacf-6bba34a6c68e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 40,
                "y": 35
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "10ee89a2-330a-492e-b1fd-334a61f6ec5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 22,
                "y": 35
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "bd798321-8de2-49ce-8f2e-f20b89feca77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1d0f87b1-7b24-401d-b096-08eee1285649",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 31,
                "offset": 0,
                "shift": 25,
                "w": 26,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "113b6e11-82c6-497f-8287-9379445db1b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "3e78e9bd-5cac-42c6-ac29-5c117b5888ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "622a6e1b-87d9-4874-b549-5e3865b591c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5fb86721-ba63-434d-97df-7afbfe2abe1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 31,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 96,
                "y": 35
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a7f23f6a-90c4-4a28-af08-c0757777c5b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e064c36c-4638-4609-bd8d-f14e5d8ce23f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "8bd01289-7b68-4061-af45-3e4a532db31c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 31,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "64583dbc-8bd2-4854-b8a7-a530e8e5b82f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 31,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4dc07bc3-0b96-4699-a619-cc4c8948db3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2fbf5b7f-7b6a-4821-be90-9bfcc2adafd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "57fea751-0224-4f07-9f6d-37b46549aa93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "3858befe-270a-4c93-98e6-27409d294fee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 31,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a233de48-e1e3-4722-adbc-0b211396035f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "fbd81ae0-0610-4fb8-a9f2-e7bd0e728a55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "9f32d08c-9f71-4fd4-9a99-8579f4b38f61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9cf3f15f-7496-4980-95cc-b7ccdd950f86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 127,
                "y": 35
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "21d172c7-e15b-4e3a-9e5a-e04c93842e15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 33,
                "y": 68
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "733e3d98-6269-4574-b672-c8d49ccbf37e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 31,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 143,
                "y": 35
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "17d5695c-0441-4757-bec4-2927747c7f50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 31,
                "offset": -2,
                "shift": 6,
                "w": 7,
                "x": 175,
                "y": 68
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "88b20029-a178-48ac-81f6-2f8e83c50a1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 31,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 160,
                "y": 68
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c575220b-9109-4b69-a9b6-e32b1553a8bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 31,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 154,
                "y": 68
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a9698d71-53fd-4ce8-afff-62ce37e1fa8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 31,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 132,
                "y": 68
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "63d61c9b-d972-4686-92a7-782c86eecc99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 117,
                "y": 68
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ebbbf6e4-f56f-4fbf-87c9-8d862623cc6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 100,
                "y": 68
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6979a4ec-edbd-478e-90a1-83e5dd815210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 85,
                "y": 68
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "863c61d3-aa32-4432-824c-95e1b17634b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 69,
                "y": 68
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "1da8b6f0-0726-4c19-b1f6-d05941851f1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 58,
                "y": 68
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4327401b-d6ea-4288-95b5-6196968951f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 184,
                "y": 68
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "75368998-d605-4601-9dff-7950026e858a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 48,
                "y": 68
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6e649ca3-66cb-4f8f-9053-6338471ecbd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 18,
                "y": 68
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f23297d4-8e6a-4394-b808-71c536d7a84e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a21e34fd-262d-454c-a9bd-9131dce19fce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 31,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 223,
                "y": 35
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "68b49cdb-33a2-41a9-92ae-e9de4cbd5977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 207,
                "y": 35
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ad5a6320-5c1f-471d-b202-defcb6020750",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 191,
                "y": 35
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7d6105f1-41b6-437f-a244-1289dd4c8209",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 176,
                "y": 35
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "451f7ae6-c844-453c-9a06-f63825b4cc36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 165,
                "y": 35
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "66978999-5ecf-4ad9-9a9b-de9da193a733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 31,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 160,
                "y": 35
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "18de527b-41b2-41e7-a73b-47908b5558c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 149,
                "y": 35
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d3e983eb-8326-47b1-9ebb-681045af4873",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 160,
                "y": 167
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "8303f7e8-6046-448e-865c-7c5733c9759f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 31,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 176,
                "y": 167
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "f94693da-fc9d-4ec5-aa52-95aeb0eaee7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "67efb172-45e2-434e-9e05-664b906c380d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "0cb3646c-7fac-4387-bbbe-d51e5dcff4d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "3e1f5f66-cc04-4e3d-bf8d-1669c7429385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "c41de3ba-b06a-4cfb-b2de-138797a1593a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "a253b0fa-77a5-4db6-a253-eb4ee9ba9f20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 49
        },
        {
            "id": "2b5714e9-34ee-47bd-a1a4-e53195583fe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "cc3ef68d-48d5-4d42-bb3d-6e6acb79ce38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "8ce5e349-b5a4-4641-a412-642c6cacbc02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "83535194-91ce-4038-bd7e-01cb56538718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "73630383-f004-4adb-be78-d846148cf310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "ab5bf398-cf1c-49e3-be90-5569ea83b5e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "b7b7f8fc-88fc-4484-af8a-cb89aa22579e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "ba00c39c-18f7-4ce2-a7bd-ece742b96785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "13172098-8659-478a-9df4-d18a653f6bc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "a2c7e0bc-759f-4bb8-9177-91b6d5fdeb2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "71ee2504-d726-4886-a9a1-7f0c2121ea0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "45c1619a-dacd-4ad3-8013-2f65fc80763e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "b1a78522-15fe-4596-9f49-d5ffd55a469d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "39ef7908-5319-42d9-ad2b-3776ea97c887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "f9df3333-79ac-46a5-b6ad-ea8ba7d12fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "4afef545-ae59-482c-80fc-fe1010bb6fa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "659ab027-83f6-4f46-9d70-68ffb2d90141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "85f767fe-e567-4c48-a867-a0b1bd743969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "bcac6b51-cfbf-4145-9c77-91492e10ed6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "40574525-d084-44b5-9e54-3402132d372f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "efa5d131-7347-4244-bc16-e1e054b101a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "6e239d8a-6e21-4c24-a649-3e56b9889dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "d602f716-4347-4573-90b0-9325baff368a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "33fef0f9-921f-4081-b7d4-769c562762e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "43ef4f0c-0159-4fdf-877a-62a79b89154a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 58
        },
        {
            "id": "21015f70-b983-4559-9edb-e7beb37e59c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 59
        },
        {
            "id": "d3e92961-18ff-4e0e-9457-c33195ab7201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "e47ce187-c9da-46e5-bf78-5840e96869ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "c1143285-f085-4768-a7c4-69b44cacc313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "8c23c38a-fe55-411b-be15-271c0c7c6384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "e3bb0c28-ba00-4c3a-964d-0ab88ac937e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "7e46b021-877d-4c62-a2ae-19b657903562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "d26f9594-ad8a-43d5-9112-2efd3e83bed1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "d8793a5f-1103-4709-bbaa-e18f76cc4912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 115
        },
        {
            "id": "a8d5bfe8-97ba-4275-9296-4e8f171adae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "edf4956e-6338-4643-8743-2a5a6d8c359d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "7770248e-ba30-4f2b-afc6-ce31a952dc15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "9e5e520a-03eb-4bef-a85a-88f3f56fd6e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "5e790f7e-5e28-48f7-858b-e5b076563bd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 894
        },
        {
            "id": "da36350a-4a25-4dee-af72-56cca1bac2a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "c477119a-9b8a-4b35-afff-68cf71093f62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "ace8a716-3b08-421d-8925-0dd32e12cbb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "e4acc602-5017-449d-b2c5-09d0e82f1998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "e5b0d64c-10ba-47e8-98e0-910c5346ca48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "e827fa54-bf6f-4e30-a867-6ef20c080c47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "ea93c4bc-a50a-44a1-adf7-2a51bb2c7163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "feb0d073-1258-4347-a6ba-0fd9b05413e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "29e34d82-251d-4020-bde1-7c1dd22462f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "46fd137a-788e-4f13-9357-b0b134be0f11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "302b0bb9-ecea-4040-b215-f96a82dd58a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "31867b52-0fca-4dca-9f5b-8dac7685a41f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "d3e46335-3111-4d18-aa31-07fea3ce631b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "7b421fd7-f7aa-4b7e-b563-a981d40598d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "b6ee7e69-d839-45cc-9e0e-7831d4c2f97f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "4acb5aad-b1d0-4070-9c54-89ccdbd7339f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "17ebc57a-6066-499b-88c5-86d59fc7bc18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "2c68cdbf-2cf2-4417-ac6d-374984129a33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "30726d68-60a7-4698-893e-b6994c202a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "b36e8db9-4c81-451a-a599-78a78e273958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "09735777-9490-4829-925e-04e888b0993f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "b9587cc5-868b-4afb-8f55-77b618562f44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "d32d590a-602c-446d-ba28-4fd6270edc04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 59
        },
        {
            "id": "979a3048-e6f8-419c-8f89-7b4c6179314e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "6719bdbb-2a32-4ef7-af27-7d7f2252275c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "d8eed1c1-e3ea-4843-a894-84bb52f10a6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "074b2807-ac38-475f-a133-4854771ea86a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "ec75622d-f26a-4520-85df-8df3a3403dce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "a6f2ede5-3945-4652-a77a-5a12e5d72fb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "1148ef12-e08c-418c-81af-69586134c405",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "c1f26d84-114a-4dfb-90dd-da4594f1496f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "9e9e43cc-af90-42ec-b3c8-190945149ec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "cf6a5744-a205-4c3f-b5fc-3de4e2b3d6c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "eaa26a55-e1fe-49b1-a7a9-e77794980b1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 894
        },
        {
            "id": "1b217f04-00b6-49cc-8f19-9b11b6d81c37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "a3923b37-dcd1-4682-a33a-dd52ce8374ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "18c94642-f045-447a-b498-619e2766fcc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "6cf91e9c-1228-47ce-8c8a-2a16da567c42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "539c2b32-111a-43c0-ba0c-19c0e59c26b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "4a9d5495-cd54-442d-93fa-08025ba93df2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "1c63300e-4389-4cbc-a850-2ccd6810e418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "9a0b4e54-57da-4ad8-bff0-1bd045a8caee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "6a5c75e3-870d-4170-9a87-8ef02b30ba03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "Basics",
    "size": 20,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}