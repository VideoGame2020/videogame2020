{
    "id": "6144b398-229d-4edb-8dab-b794f404aeb0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fSmall",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "8611817e-2f65-4286-ad67-d5f4a7d390fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "18639fe2-839f-4d1f-93d8-bdb12a7d009f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 162,
                "y": 77
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f0307c90-c923-4cc4-b84a-64dcf2dcf0f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 23,
                "offset": 3,
                "shift": 9,
                "w": 8,
                "x": 152,
                "y": 77
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "590dfc0c-30e8-4492-9070-7fc8ae940ea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 138,
                "y": 77
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "088044a1-a520-4c00-9828-5123e744bb64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 124,
                "y": 77
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "f6594ee8-d0b3-46e5-93da-a831affc146c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 23,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 105,
                "y": 77
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7932168a-b5a1-4a3d-9b0e-8c3b517dd472",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 89,
                "y": 77
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "eafbe9af-7d9e-45f6-94ec-4036cf3eadef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 23,
                "offset": 3,
                "shift": 5,
                "w": 4,
                "x": 83,
                "y": 77
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2d25d531-f2e0-4a4e-b97c-5cf1d79290ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 8,
                "x": 73,
                "y": 77
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4c8ccca3-e47c-4d18-9e36-079f1e4f1136",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 23,
                "offset": -2,
                "shift": 7,
                "w": 8,
                "x": 63,
                "y": 77
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "86e446cd-4b54-4692-b455-3937c79c4cdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 23,
                "offset": 1,
                "shift": 8,
                "w": 9,
                "x": 171,
                "y": 77
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f4c460c6-ca08-4ce4-9ae4-352c35feb446",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 50,
                "y": 77
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "fa14b84c-9b5e-4610-950b-8c6582eb2625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 77
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ffc9d59e-d3de-4ef6-b97f-079ad9f8a4d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 21,
                "y": 77
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b48628ce-8d09-4c0f-8c63-8ce780a264d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 14,
                "y": 77
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0627af57-8b86-43db-83aa-d1026ed7b72b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 23,
                "offset": -1,
                "shift": 6,
                "w": 10,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "19291ffc-b16f-4bb3-865b-9b1ec45cdf4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 232,
                "y": 52
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6d3a1623-38d2-4b0c-8219-6e4ed0eb7c81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 221,
                "y": 52
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a562f597-fee7-445c-baf8-37a3548c51ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 208,
                "y": 52
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "177945c3-e69c-4a8c-a073-16806e62fcc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 195,
                "y": 52
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "677734d7-ff8e-4c2d-b70e-34da28a5327f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 181,
                "y": 52
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7d1e0e04-e6a1-4458-92ec-cdcc3d650264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 37,
                "y": 77
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "2b290092-a5a9-40ae-9783-cc1d3f770fa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 182,
                "y": 77
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5332f301-274f-4c31-a027-85780ac1c4f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 11,
                "x": 195,
                "y": 77
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "214eb949-a67d-4ada-8a37-def0883b8145",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 208,
                "y": 77
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d5af055c-a37a-4cae-b99e-593ccfcb8546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 19,
                "y": 127
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d3059605-9885-48c9-a687-e84e62824272",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 11,
                "y": 127
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "fc32e741-5245-4def-a4ef-b10b3c0f5eb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 127
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "bdbe8ea0-0d38-400b-97af-13ceb8b3a0f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 240,
                "y": 102
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "61e611e5-ded6-4a6d-965d-0215fef68f63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 227,
                "y": 102
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "081aea88-d1d0-47bc-a07f-825074950df5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 214,
                "y": 102
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "81fb4fbd-6411-4215-963b-d21fab2bf87e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 23,
                "offset": 2,
                "shift": 12,
                "w": 11,
                "x": 201,
                "y": 102
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "56c9f4f4-48d8-4cd9-97a1-17f0b26d1458",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 23,
                "offset": 1,
                "shift": 20,
                "w": 20,
                "x": 179,
                "y": 102
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f77c65ee-bb10-4b67-a728-a3aa66042056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 23,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 162,
                "y": 102
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "470d91d6-9449-482d-8608-d4a5ece8ede7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 23,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 145,
                "y": 102
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e20a0a1d-645c-49bb-b9c3-dff37aa236be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 129,
                "y": 102
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9b35fb56-1df8-4f66-9606-71439af7b360",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 23,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 112,
                "y": 102
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "dd6dc7c3-28a0-4da1-9a93-cd7b15e65726",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 15,
                "x": 95,
                "y": 102
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d7938173-a07b-4b41-9091-e39084b584d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 14,
                "x": 79,
                "y": 102
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "835b4c72-2a04-4392-8af6-a2b69d30f128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 23,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 62,
                "y": 102
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "7ee95633-14db-4cdb-a1ae-0afcf415c3e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 23,
                "offset": 0,
                "shift": 14,
                "w": 16,
                "x": 44,
                "y": 102
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d7e57445-ef19-4921-9d84-ffab237c16af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 35,
                "y": 102
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a131c2af-48a8-4a21-84ce-517f8b7cbbf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 21,
                "y": 102
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1c20a328-c4a6-4ffb-8d2d-3f0fff94814e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 23,
                "offset": 0,
                "shift": 14,
                "w": 17,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "42415b2f-a29d-48f6-a288-139065741e6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 241,
                "y": 77
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "86fe1d9d-0aa4-4816-adb2-dbeddd5273c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 23,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 221,
                "y": 77
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5403e9be-b5e0-4e12-a6de-e6f9953e6b71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 23,
                "offset": 0,
                "shift": 14,
                "w": 16,
                "x": 163,
                "y": 52
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "808d0232-1d74-46a0-a4d4-3a257953245e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 23,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 146,
                "y": 52
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "7ab15455-63d5-41eb-ac5d-051dbf1bb9ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 15,
                "x": 129,
                "y": 52
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8bad6733-0d8c-40cf-b575-80a77ce46d09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 23,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 60,
                "y": 27
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "53d3662a-27b3-42b4-9c6d-4e0c69851a70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 23,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 32,
                "y": 27
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "739555cc-6af9-4b3e-b069-a337efc5cbe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 17,
                "y": 27
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "eedb5248-0388-467c-98b1-881ad75367ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 23,
                "offset": 2,
                "shift": 12,
                "w": 13,
                "x": 2,
                "y": 27
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "873f973c-96f2-4d73-a86e-5e8eea87e846",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 15,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "dd1062f4-b451-4d32-97ea-6f3bd2e761a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 23,
                "offset": 2,
                "shift": 13,
                "w": 14,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b68747ef-a53f-42a8-8985-8f36647e6f54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 23,
                "offset": 2,
                "shift": 19,
                "w": 20,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "826889af-e8d0-4cab-952b-507a2746b8b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 23,
                "offset": -1,
                "shift": 13,
                "w": 17,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c01bf67a-78aa-4872-b1b7-d98387dcfb48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 23,
                "offset": 2,
                "shift": 13,
                "w": 14,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "929ad729-3c54-43a6-ac31-d5ad05cec297",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 14,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "14245245-eed7-4366-a119-679890c3d087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 9,
                "x": 49,
                "y": 27
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1d01c509-234d-493e-8bc9-a9474fd5b14e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "a7458370-51ff-4763-b65b-f57145bed49b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 23,
                "offset": -2,
                "shift": 7,
                "w": 10,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "31c86641-215f-47e0-8275-976b4b953c8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 23,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "deb5e72b-4746-4e17-b537-f25cc0457d1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 23,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7d9bf19a-4940-4a7f-a6a4-4cb92d6327e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 23,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a6e8aa4f-ecee-485d-a11d-e9421d27e4cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1b5aa001-bfb2-49e3-bda4-b8bdaf6efb66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a321da34-08e2-4097-be4a-7186e4c9637c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "17dd9289-5a28-4308-9d09-c02ca1792667",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 13,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "61678003-595b-4ddc-be08-aa6e0a0eaa98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e271b516-6de5-4d99-87ad-80e5ed44e56b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 9,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a3ccf736-c4d0-456d-b2f2-14dec38b0fb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 77,
                "y": 27
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f65a6c26-c367-417d-8278-567f2c969af6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 220,
                "y": 27
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9a21ec50-ff14-4007-9786-a7d3bcfc406c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 92,
                "y": 27
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "da106172-ae44-4cf2-86dd-55be0cfb2414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 23,
                "offset": -3,
                "shift": 6,
                "w": 10,
                "x": 103,
                "y": 52
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "72c7ca6d-bd6c-473f-a43c-95815285aae9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 13,
                "x": 88,
                "y": 52
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b532d47a-a523-4b34-8025-f6bd72d62144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 79,
                "y": 52
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f0fb4f61-f13a-43cc-9571-d1e8400d9ead",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 23,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 59,
                "y": 52
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1b1dbf1c-98bf-4149-8c52-97954da47453",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 45,
                "y": 52
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "123c0af8-1970-4d75-963b-173150435460",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 32,
                "y": 52
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f26ac921-4a0e-478f-be51-98082ac6d4a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 23,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 16,
                "y": 52
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d0f23050-08c1-41e7-b4bc-ddce8c20389e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 52
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "759c52a8-c7eb-4468-b0a2-487cffc7bfa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 10,
                "x": 243,
                "y": 27
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ae5046ab-c1c6-4034-8ff8-5fa0afc5662a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 115,
                "y": 52
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "5c2a53b5-f8d3-4713-91a8-cb7f3e7486b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 234,
                "y": 27
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "fb503c7d-cac9-4355-81fe-9a242082dc2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 206,
                "y": 27
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "15d98a7c-d8c1-4df5-b671-889924f0b6d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 12,
                "x": 192,
                "y": 27
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e551cc94-e92e-4f76-8ddc-b2ef40a5bee8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 23,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 174,
                "y": 27
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "5da2ff28-2b2a-4b1b-9cd8-db475070717d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 23,
                "offset": -1,
                "shift": 11,
                "w": 14,
                "x": 158,
                "y": 27
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "030c1333-ef42-43b2-9fc2-563c84d11e11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 13,
                "x": 143,
                "y": 27
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "50197db8-fe4a-4143-88ca-6c6de9abad77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 130,
                "y": 27
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f70b0852-425a-44db-a15f-ffa92f6bbdd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 10,
                "x": 118,
                "y": 27
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "195545f3-12e9-4286-9a20-7196d92e288d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 113,
                "y": 27
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1fc64c74-48c4-4a59-9099-e3240134f650",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 23,
                "offset": -2,
                "shift": 8,
                "w": 10,
                "x": 101,
                "y": 27
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7acf5e2b-dba8-462a-8097-b3bfd25eb631",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 32,
                "y": 127
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "a84212b6-1360-4ffc-98ca-2014d2c96b3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 23,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 45,
                "y": 127
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": true,
    "kerningPairs": [
        {
            "id": "c76f15d4-daa1-4f79-b292-0b50c872508d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "0d096412-6faa-4ada-8a99-4b3dcedd38ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "a62cb100-a0eb-41dd-9248-11560e81a944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "f7dbb3d7-c8f1-42cd-aa08-97cb259e406d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "3cd4b852-19b2-401d-89f9-caa70d64afb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "038e51ca-d8d6-4437-a873-47e24e7772cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "2a9125a9-c035-4c2b-8e99-bd23b8129705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "3033dd80-0d47-4366-b122-691925f46c32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "fff0cde4-073f-4cea-9534-b80ebaa64561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "2ec65636-d842-4a15-981f-ad12702d42ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "4e01fca5-f20d-4370-8e1b-8dc57047dd66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "928f81e4-6cb6-4c8f-9882-b557044efeb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "96b814e7-b2bb-45bc-b1ba-751a2b041476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "9e4e9ce6-03de-4de2-80b3-d03b216d304c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "4950f736-89bb-411d-80ba-70f9d25ba465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "e05c5361-3e83-42a4-a87d-eb3f1606f916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "dc195d8a-eae1-42fd-8fcb-af26ff98f57c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "b5e234d5-809c-41eb-8583-49c6b2447e86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "4bd781ac-f6b7-448d-8746-de2d616ec0ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "88b58537-dfb4-4fd1-a7cd-480240465241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "1e97976c-5edd-4958-a828-4d5ff89fb513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "22ae5ef5-768e-443e-97a2-50bf5fc3566d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "28318a8b-0c04-4be6-a80a-6f66bd23b92b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "b62b5fd7-51b7-40bc-a242-2a00bce50400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "4539a084-8f60-48b2-964a-e0e99a9104ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "953347b4-8258-441e-bd89-9a1b43dba28b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "e590b5e5-8ab8-4796-8b65-14fc6c7cba9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "3e62b227-4c12-41d9-b188-3b9aa07a01e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "78c45b3b-d1fa-4b58-ac65-e12be2d117b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "6a45a6d8-8eab-4d71-acd5-eaa3f47bc742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "f3c05387-3af9-4d89-908a-af7323bae6a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "37f48250-3330-4310-bed6-74e151784ddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "2f341f17-3c54-41d1-9cbe-00fe76035e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "10c8fef2-a920-42c3-87a5-bc89030fbb8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "96662adc-c016-4e2d-96ee-a53ac603924e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "e9f827d1-d712-4d8b-b1f0-140390f148b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "339ec6c6-f9f2-4f22-9232-e42b5bb8b53f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "a3acb231-4221-444b-84ef-a54066417c16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "3fb4c822-3340-49b7-99a3-4c05f9163de0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "c1ae957c-7e7d-42cf-ae0f-5ba02db5d823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "971ced14-2c90-408b-8e8b-35311e134006",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "76f02d15-d431-4d6a-8af4-005861d24c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "3d0fe8b6-34b3-4b6f-9fa3-c945e015eeec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "c0d8a25e-ccaa-4606-b930-e20fe1b7bf15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "406cdfc1-7c79-43be-b881-30f372571acd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "14ecfd81-79f5-4650-a974-1739acfec7c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "86872416-6020-4491-82b4-df07d95fdcdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "26d5baa3-0cfd-4a87-ae22-582770efa8a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "70913f59-611f-48cc-bfb7-ba78b193dd8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "62ae3ddb-b101-42bd-915b-c65fb1b31da0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "bf9005af-5610-4869-aeb3-4b099994e04c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "f7e3242e-7263-42a4-b287-3947e4e29f7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "d580f4cc-f270-499c-99bf-1d2df7382649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "bc571b35-f870-4fd8-97b5-926110485c94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "e8329a8d-b4e6-4d9a-a3d7-5ea5e5511233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "207644df-41fd-44e5-b9c2-6094a05da2a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "e46dd1b7-728c-4975-8ba5-343f92267b4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "8ed6ce6d-f679-416d-a51e-9986b61ce0b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "00504e78-b5dc-40e9-9364-3785b655be89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "eba1b47f-167f-472d-bb30-3dc65714de32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "12e45ca2-9b60-4857-8892-55b63ac79fbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "4582c1e1-ecb2-4f30-b49a-90021b928e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "407f53c6-fdff-4416-8406-2fafb8cf81cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "a3697178-d66c-44b0-929c-42a34f50633f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "0ef9d5a9-63a3-4087-9b4b-115e82048b04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "c1ef33cd-f6f9-4ebb-9723-7c23b15cff2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "b969ac74-d446-4084-91c6-b9ee53c1f34d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "15058001-bf4f-4ca9-8174-cac3016569f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "c1e9056b-9a70-4a54-9890-39c33a476eaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "e71ac6da-fc96-420e-92be-0eca22cb85e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "a5f5a452-e779-4b2a-b1a2-c44ae75972f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "03d4627d-a2de-4c1d-82ab-502bf5ff2bc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "4942b588-a1ae-464c-a6ba-47471877a84e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "772f0a1d-9f06-4673-affa-d56a899b991a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "8dc3054b-b2f2-4034-b0e2-9147ed252f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "b466859c-4d91-442a-a391-2ae639da7b4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "40ec28c2-0250-4f4d-8344-84a0e1ce089a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "73e39240-33e8-4290-9003-4a3b3862b030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "6545fbfa-519a-4ee1-a045-98dedc8ad2d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "dad8b431-e404-4cfb-a736-adc716002279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "1959cbe5-dc39-439a-b914-c787384f9877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "42ff80db-c718-44e8-af58-3030a45c7df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "df00c255-30d0-4d0c-84ee-e67914520503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "32fe0f0b-ff39-4853-ba78-feb224666a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "d142db0d-56c3-48db-81ad-06eb2140111d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "957f0934-6512-4b14-a604-fb3415fe20b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "Press escape anytime to return home",
    "size": 15,
    "styleName": "Bold Italic",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}