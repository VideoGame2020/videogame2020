{
    "id": "c5509359-2569-4d75-a458-4a7d06744e02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 76,
    "bbox_top": 68,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80527858-5911-47c3-a72d-2079669e050f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5509359-2569-4d75-a458-4a7d06744e02",
            "compositeImage": {
                "id": "1092179d-5a42-446f-9aef-b09c88098706",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80527858-5911-47c3-a72d-2079669e050f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7d7d3f8-94ba-4b07-8991-12ad5ac1e507",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80527858-5911-47c3-a72d-2079669e050f",
                    "LayerId": "c83811cd-a013-4562-8096-bfbda86f12c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "c83811cd-a013-4562-8096-bfbda86f12c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5509359-2569-4d75-a458-4a7d06744e02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 38,
    "yorig": 38
}