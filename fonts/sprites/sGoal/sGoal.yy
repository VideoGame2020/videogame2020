{
    "id": "aa0a1a2b-2734-43cb-af91-bab6a2f48f7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGoal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 1,
    "bbox_right": 445,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf480b0e-df69-48ad-beb0-e0d52a377fcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa0a1a2b-2734-43cb-af91-bab6a2f48f7e",
            "compositeImage": {
                "id": "80ef1f68-011a-4627-873e-a8b5c2cccfda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf480b0e-df69-48ad-beb0-e0d52a377fcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37168bad-ac6d-4e86-8c39-b91c70116dc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf480b0e-df69-48ad-beb0-e0d52a377fcf",
                    "LayerId": "8f5bc963-2246-4d9f-98ac-c57c19dc743d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 97,
    "layers": [
        {
            "id": "8f5bc963-2246-4d9f-98ac-c57c19dc743d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa0a1a2b-2734-43cb-af91-bab6a2f48f7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 445,
    "xorig": 222,
    "yorig": 48
}