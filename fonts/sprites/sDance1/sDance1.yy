{
    "id": "b711154d-96d7-4223-9096-0c6be34aca5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDance1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 204,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbb8f0c9-f40f-4244-9a05-a3629d462203",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b711154d-96d7-4223-9096-0c6be34aca5a",
            "compositeImage": {
                "id": "2b8b2bd1-2200-42f9-90a6-1a9786971b98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbb8f0c9-f40f-4244-9a05-a3629d462203",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33be1902-81bd-4bdd-b38d-33f741e9a590",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbb8f0c9-f40f-4244-9a05-a3629d462203",
                    "LayerId": "2249fb13-07df-4ee5-a1ad-7804a3874c69"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "2249fb13-07df-4ee5-a1ad-7804a3874c69",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b711154d-96d7-4223-9096-0c6be34aca5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 205,
    "xorig": 102,
    "yorig": 150
}