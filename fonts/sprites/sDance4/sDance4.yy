{
    "id": "c51e34f4-3a53-4fcb-8556-c68cbf31cd4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDance4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 204,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60d1d6d2-710d-4051-9c99-8e913dbce3ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c51e34f4-3a53-4fcb-8556-c68cbf31cd4c",
            "compositeImage": {
                "id": "a4a902fa-b303-45bb-ab51-299ce60ecd7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60d1d6d2-710d-4051-9c99-8e913dbce3ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aacc0c1b-fbb4-420b-b4fd-a98d484631a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60d1d6d2-710d-4051-9c99-8e913dbce3ed",
                    "LayerId": "930acc13-c0b7-4925-ac51-b20178f3e681"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "930acc13-c0b7-4925-ac51-b20178f3e681",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c51e34f4-3a53-4fcb-8556-c68cbf31cd4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 205,
    "xorig": 102,
    "yorig": 150
}