{
    "id": "ae22a2d7-7420-4e3d-b046-b78718bb840d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpeed3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9db0e57-5168-41ce-b358-98a2fde62893",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae22a2d7-7420-4e3d-b046-b78718bb840d",
            "compositeImage": {
                "id": "a73accf6-e798-4606-8aef-28477008f3aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9db0e57-5168-41ce-b358-98a2fde62893",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8295f37b-cecd-49c1-87e3-e3b26edc21ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9db0e57-5168-41ce-b358-98a2fde62893",
                    "LayerId": "70078d24-8f66-4604-881e-caee9cc69a4c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "70078d24-8f66-4604-881e-caee9cc69a4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae22a2d7-7420-4e3d-b046-b78718bb840d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 50
}