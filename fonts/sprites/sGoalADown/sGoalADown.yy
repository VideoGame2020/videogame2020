{
    "id": "4a91b8ea-4a40-4963-a8ea-0ca5fe2ca17d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGoalADown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 1,
    "bbox_right": 445,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "296215d8-c5ee-4f3a-a93b-2370f2bfba32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a91b8ea-4a40-4963-a8ea-0ca5fe2ca17d",
            "compositeImage": {
                "id": "91ea4731-34ae-4afa-8655-a858972a9ccb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "296215d8-c5ee-4f3a-a93b-2370f2bfba32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25ab94a5-5b0d-4b44-baf1-e5904bc06385",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "296215d8-c5ee-4f3a-a93b-2370f2bfba32",
                    "LayerId": "f0b44460-eaf4-4f4c-ba1d-852cce304a68"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 97,
    "layers": [
        {
            "id": "f0b44460-eaf4-4f4c-ba1d-852cce304a68",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a91b8ea-4a40-4963-a8ea-0ca5fe2ca17d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 445,
    "xorig": 222,
    "yorig": 48
}