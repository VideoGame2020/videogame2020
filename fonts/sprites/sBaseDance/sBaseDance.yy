{
    "id": "b3a3557f-6ffe-40ff-bc9c-3a9303cbc53b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBaseDance",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 204,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ddb6c967-b7fd-4218-8d6a-7415330316e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3a3557f-6ffe-40ff-bc9c-3a9303cbc53b",
            "compositeImage": {
                "id": "2f5b6108-ed58-4ee0-b5ab-80fe7b91718e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddb6c967-b7fd-4218-8d6a-7415330316e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc1bf977-d555-4b7e-bc81-4a6a0ea17248",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddb6c967-b7fd-4218-8d6a-7415330316e6",
                    "LayerId": "45ab032f-8912-4bbe-bf86-563ddb30f90a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "45ab032f-8912-4bbe-bf86-563ddb30f90a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3a3557f-6ffe-40ff-bc9c-3a9303cbc53b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 205,
    "xorig": 102,
    "yorig": 150
}