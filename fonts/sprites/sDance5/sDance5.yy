{
    "id": "f115824f-d35b-44d2-8c54-33f3242087dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDance5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 204,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14b4497e-de25-4d87-9e67-9d5be87a7b28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f115824f-d35b-44d2-8c54-33f3242087dd",
            "compositeImage": {
                "id": "b00ebe06-cdd4-433c-b39d-f5660915b13f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14b4497e-de25-4d87-9e67-9d5be87a7b28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35ef8ab8-b3cb-40a7-a0a8-21aeaad65753",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14b4497e-de25-4d87-9e67-9d5be87a7b28",
                    "LayerId": "d25493d9-a249-45dd-89b6-fdbb1064ffb2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "d25493d9-a249-45dd-89b6-fdbb1064ffb2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f115824f-d35b-44d2-8c54-33f3242087dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 205,
    "xorig": 102,
    "yorig": 150
}