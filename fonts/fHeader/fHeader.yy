{
    "id": "16885dd0-0b13-4e78-9b67-7529b5ac4bb1",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fHeader",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c4d72cfc-f776-46b0-83cd-99f12006279e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 69,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "62755c5e-f1b1-43fa-83e6-63e377df8c3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 69,
                "offset": 5,
                "shift": 17,
                "w": 7,
                "x": 484,
                "y": 215
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "fdcc82cb-55bd-44d3-8d5a-37f31384fb18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 69,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 465,
                "y": 215
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "aec40348-96bf-40d0-8eef-e73111b11309",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 69,
                "offset": 0,
                "shift": 33,
                "w": 33,
                "x": 430,
                "y": 215
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "987f77f2-4b76-474e-b5c1-2296c4621fe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 399,
                "y": 215
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "786b0095-4150-4628-82fe-e2f1daa6390c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 69,
                "offset": 3,
                "shift": 53,
                "w": 47,
                "x": 350,
                "y": 215
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3812b2a2-78e4-4f27-829e-0c26cb470423",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 69,
                "offset": 2,
                "shift": 40,
                "w": 37,
                "x": 311,
                "y": 215
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "1ef8860f-1b66-4727-a880-0f605913897e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 69,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 302,
                "y": 215
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "38830d9b-d36f-4fa9-b937-219f35231f59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 69,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 285,
                "y": 215
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4503535b-ba5c-4b83-bf70-d6cec6fe3ecd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 69,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 268,
                "y": 215
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f3568c32-1c0b-4d78-999b-5866c8a8eb37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 69,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 2,
                "y": 286
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0b344b35-d5b8-4787-be3d-de5079b62afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 69,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 237,
                "y": 215
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e163bb1b-69f2-46d6-88e9-f305ec1ec80b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 69,
                "offset": 4,
                "shift": 17,
                "w": 8,
                "x": 196,
                "y": 215
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "715cf765-4103-425e-91fb-fb424d6ce8eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 69,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 176,
                "y": 215
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "13f3bbae-9f79-45ae-bd11-3d76290d09ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 69,
                "offset": 5,
                "shift": 17,
                "w": 7,
                "x": 167,
                "y": 215
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "86484725-40b7-4884-a82f-227ab75a6280",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 69,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 148,
                "y": 215
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6347b8fe-00ce-4bf2-9476-67fab9afcb76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 117,
                "y": 215
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "724a2d3c-b3c7-4e34-92d9-55bd98a867c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 69,
                "offset": 6,
                "shift": 33,
                "w": 17,
                "x": 98,
                "y": 215
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "df10b557-197d-4647-8137-9cb27e45cb24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 69,
                "offset": 1,
                "shift": 33,
                "w": 30,
                "x": 66,
                "y": 215
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "6e03682c-70be-4a47-8404-18a078be2811",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 35,
                "y": 215
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a570cf63-0c5b-4da8-823d-c3b38e37ce41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 69,
                "offset": 0,
                "shift": 33,
                "w": 31,
                "x": 2,
                "y": 215
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "05235d70-37c0-4149-8315-6be65178afc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 206,
                "y": 215
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "2ed224cd-af98-44e7-86aa-71e87509c1e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 25,
                "y": 286
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "69f47109-7f03-4aec-a10a-4566bd671ebb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 56,
                "y": 286
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "0fd6f598-09ed-486f-bd00-6e2cb7c6f2cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 87,
                "y": 286
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ea973a5f-5089-46af-afaa-4b01b7829677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 282,
                "y": 357
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "aa55a1a8-d0aa-4774-b552-93a4fb1b300c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 69,
                "offset": 5,
                "shift": 17,
                "w": 7,
                "x": 273,
                "y": 357
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "3b593e2c-5daf-498e-8761-37d7c7e0235c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 69,
                "offset": 4,
                "shift": 17,
                "w": 8,
                "x": 263,
                "y": 357
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "99e3b911-d75d-439c-a8fe-91b69fb40f06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 69,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 232,
                "y": 357
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2a883a00-e1cb-48a4-a49f-c668e600e2f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 69,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 201,
                "y": 357
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d21bc1d8-66d3-4af1-94ea-7de9bf51e30d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 69,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 170,
                "y": 357
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6c075942-a34a-4418-b5a7-faf2b97da61b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 139,
                "y": 357
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "bbf5deb2-5b60-4568-b353-4a7361b4f460",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 69,
                "offset": 3,
                "shift": 61,
                "w": 56,
                "x": 81,
                "y": 357
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5bcc4a9f-9d59-4cc2-8b48-678298f15265",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 69,
                "offset": -1,
                "shift": 40,
                "w": 42,
                "x": 37,
                "y": 357
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "75c8e514-f198-4bf3-aa1a-50d85b03688b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 69,
                "offset": 4,
                "shift": 40,
                "w": 33,
                "x": 2,
                "y": 357
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "73f2b27c-fd7d-432f-a20d-809915a287ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 69,
                "offset": 2,
                "shift": 43,
                "w": 39,
                "x": 451,
                "y": 286
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "8c2e67a9-6a54-4bae-bf71-f06cd03dd6df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 69,
                "offset": 4,
                "shift": 43,
                "w": 37,
                "x": 412,
                "y": 286
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d424f533-a108-423a-b9c8-5f7ab530da82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 69,
                "offset": 4,
                "shift": 40,
                "w": 33,
                "x": 377,
                "y": 286
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d616d5f2-bd06-4271-9d41-0f413fd0cc95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 69,
                "offset": 4,
                "shift": 37,
                "w": 30,
                "x": 345,
                "y": 286
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "da1575fa-bac7-4166-8c5e-15abe81f6d52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 69,
                "offset": 3,
                "shift": 47,
                "w": 40,
                "x": 303,
                "y": 286
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "cc2e0f5e-fb0c-4881-ab1d-38f73294a155",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 69,
                "offset": 4,
                "shift": 43,
                "w": 35,
                "x": 266,
                "y": 286
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d9139270-d7ef-47d5-888e-d01cc7eec171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 69,
                "offset": 5,
                "shift": 17,
                "w": 7,
                "x": 257,
                "y": 286
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "4dd35389-191e-4dca-9dcc-b978944c2aff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 69,
                "offset": 1,
                "shift": 30,
                "w": 25,
                "x": 230,
                "y": 286
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "499a8b61-b146-4a51-a893-0469f35f70d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 69,
                "offset": 4,
                "shift": 40,
                "w": 36,
                "x": 192,
                "y": 286
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "fd55c6eb-f2aa-41d2-8e07-b87e1eb26e3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 69,
                "offset": 4,
                "shift": 33,
                "w": 28,
                "x": 162,
                "y": 286
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "af7ddad1-16d4-4abd-973d-fad09d1e1b63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 69,
                "offset": 4,
                "shift": 50,
                "w": 42,
                "x": 118,
                "y": 286
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "55e69f12-e9b9-43ce-8302-a5c59f55d0ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 69,
                "offset": 4,
                "shift": 43,
                "w": 35,
                "x": 459,
                "y": 144
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a509bf16-9702-4ba3-b20e-8ac3d6a3db68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 69,
                "offset": 2,
                "shift": 47,
                "w": 42,
                "x": 415,
                "y": 144
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "dcdb51a9-055c-42b8-8b4a-202dbf570a39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 69,
                "offset": 4,
                "shift": 40,
                "w": 34,
                "x": 379,
                "y": 144
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3faa2df2-6c71-4373-a997-0c72bedf4d9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 69,
                "offset": 2,
                "shift": 47,
                "w": 43,
                "x": 210,
                "y": 73
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c6be0254-aa42-4a7a-9491-69e3bf6a94f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 69,
                "offset": 4,
                "shift": 43,
                "w": 39,
                "x": 155,
                "y": 73
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "750caf0a-ed3c-4d75-bfaf-5895312f2209",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 69,
                "offset": 2,
                "shift": 40,
                "w": 35,
                "x": 118,
                "y": 73
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "56e48307-1ea7-4570-9496-86032832a225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 69,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 81,
                "y": 73
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f29a4f4d-fdd5-4114-a5e6-d16c2673e572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 69,
                "offset": 4,
                "shift": 43,
                "w": 35,
                "x": 44,
                "y": 73
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f403ad04-3e46-462f-ac83-a76079ad1122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 69,
                "offset": 0,
                "shift": 40,
                "w": 40,
                "x": 2,
                "y": 73
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d2e674b1-cba9-45d2-8623-fb4b6afcdf03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 69,
                "offset": 0,
                "shift": 57,
                "w": 56,
                "x": 428,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "dfab3f26-ed6c-4b9a-8770-fbc030aadd3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 69,
                "offset": 0,
                "shift": 40,
                "w": 40,
                "x": 386,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "05b79deb-42ee-4b57-a80a-6dbc8a457231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 69,
                "offset": 0,
                "shift": 40,
                "w": 40,
                "x": 344,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4f5da8cb-8e7a-4803-ae7f-a8aea0966963",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 69,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 307,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "73e8d02b-eccd-4aa4-bb08-ae0c781375ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 69,
                "offset": 4,
                "shift": 17,
                "w": 12,
                "x": 196,
                "y": 73
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "0a2da9bf-8f49-435d-b64f-eda0ad1810e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 69,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 288,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ef7e19e9-af3a-49d9-acc4-32290781bf01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 69,
                "offset": 1,
                "shift": 17,
                "w": 12,
                "x": 253,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "cf3649af-f9ef-46a0-8709-be5947147296",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 69,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "8765e925-ecb8-49ff-8ed5-8945461d141b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 69,
                "offset": -1,
                "shift": 33,
                "w": 36,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f714ff6b-4872-40b5-b8e5-3a7c2ec3b795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 69,
                "offset": 2,
                "shift": 20,
                "w": 12,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d1a100aa-fad8-4b55-957c-8de018778b12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d3ce4a72-c9e5-4829-a4bd-e90dde9be1c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 69,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "64d4a9b0-de88-4226-aa38-e1390003f9f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 69,
                "offset": 2,
                "shift": 30,
                "w": 28,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4dcab83c-393a-4cdd-a0f1-5342c82c8847",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 28,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0a0be9c4-e8b8-49cc-a787-2e336eb49558",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5c5468d7-b4e0-4c4b-b0aa-6d5380aba67d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 69,
                "offset": 0,
                "shift": 17,
                "w": 19,
                "x": 267,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b3d0974c-12dc-4af5-ae05-a1b69f9553dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 69,
                "offset": 1,
                "shift": 33,
                "w": 29,
                "x": 255,
                "y": 73
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "37b58d8d-63bd-4dc6-aab4-eb6c906446c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 69,
                "offset": 3,
                "shift": 33,
                "w": 27,
                "x": 63,
                "y": 144
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "afb63701-8275-468f-903c-90e49e09730f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 69,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 286,
                "y": 73
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "5e269c60-d1fe-473e-89f3-1a8731ad0cbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 69,
                "offset": -3,
                "shift": 13,
                "w": 13,
                "x": 335,
                "y": 144
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "777dd7e6-6b21-4e82-82ed-d3bc59cf0a74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 69,
                "offset": 3,
                "shift": 30,
                "w": 27,
                "x": 306,
                "y": 144
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "35b31d3a-7f44-4924-b12b-86eeac491a32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 69,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 297,
                "y": 144
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "70bf598a-df97-42a6-9a82-cab318f230aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 69,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 251,
                "y": 144
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5393a230-1216-4c5c-8b0b-ea16e113f48c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 69,
                "offset": 3,
                "shift": 33,
                "w": 27,
                "x": 222,
                "y": 144
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "2ffa8db9-615c-4b10-aef5-3ecd9ec69f33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 190,
                "y": 144
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a1dc26e3-69dc-4ea5-8481-25c98e1182d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 69,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 160,
                "y": 144
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6aef9dfb-7b3b-4690-831b-74ea814dbd37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 28,
                "x": 130,
                "y": 144
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f0d0624b-07ab-41e2-bf17-c1d6c72dfb19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 69,
                "offset": 3,
                "shift": 20,
                "w": 18,
                "x": 110,
                "y": 144
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "10f2feca-348d-4c1a-9dfa-7e1cb919c076",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 69,
                "offset": 1,
                "shift": 30,
                "w": 27,
                "x": 350,
                "y": 144
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "748a44ca-d62f-4a30-a2aa-e76bbbb952aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 69,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 92,
                "y": 144
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "58202154-688b-4997-aa53-6544ccb17264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 69,
                "offset": 3,
                "shift": 33,
                "w": 27,
                "x": 34,
                "y": 144
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "41e70024-6941-4d04-aa52-eb13c4f2464c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 69,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 2,
                "y": 144
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c153e3f5-c729-4729-ac73-a2a8a51693eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 69,
                "offset": 0,
                "shift": 43,
                "w": 43,
                "x": 437,
                "y": 73
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f701500b-dd94-493d-a190-0e680fb4a066",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 69,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 405,
                "y": 73
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0c10a5ea-97ea-4a7a-a087-c07813f6337e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 69,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 373,
                "y": 73
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9495e22a-122b-4f88-9ff9-0b5a194fc9d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 69,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 343,
                "y": 73
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4615a5cb-10f7-4931-b92e-a9f4aba792e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 69,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 323,
                "y": 73
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3ea6b648-9859-4462-99bb-5435ae1fe3f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 69,
                "offset": 5,
                "shift": 16,
                "w": 6,
                "x": 315,
                "y": 73
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d0267b09-2021-4df8-ab0a-fc2c30400ef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 69,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 295,
                "y": 73
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "3056c342-293f-42b5-83d6-3b5f89b845df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 69,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 313,
                "y": 357
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "dcc0b39c-74aa-454c-8a76-3ee213b00ee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 69,
                "offset": 9,
                "shift": 36,
                "w": 18,
                "x": 346,
                "y": 357
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "6b964691-84f6-4f2f-a93e-ed63fcd86844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 65
        },
        {
            "id": "a8dd002a-741b-4e6f-ae89-433d7f6a48fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "bbd132b3-60c7-4003-b4e9-1dd106a7fc3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "523fc88d-d11c-4dfb-ba95-11643d3cfac5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 902
        },
        {
            "id": "5baa67b7-5fe2-4917-ba84-673d5d05454f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 913
        },
        {
            "id": "20ff98a4-bdb8-416a-9c7c-3b24f3090d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 916
        },
        {
            "id": "64f9f31e-ea2f-4e34-8ffe-906780dc5dfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 923
        },
        {
            "id": "db8f0c74-3189-4bc6-82eb-cdad8d84e3ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "d53c9b86-af80-4081-bfe9-5952cdfc9232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "7ad42f08-20a6-4aa5-b54d-5edd51e60123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "1831ee99-67ac-40f2-899a-b84dbc117523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 49
        },
        {
            "id": "91242074-6333-4824-955c-7adf369872ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 32
        },
        {
            "id": "3819fbd1-05bc-491b-9017-6eaa9b49facd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 84
        },
        {
            "id": "db183105-33e7-4448-9f65-494db1d26cdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 86
        },
        {
            "id": "efd90e89-83c7-474a-962b-5546ab34c34e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "f797128c-378b-4a69-9770-87a0f532d099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 89
        },
        {
            "id": "f26f0f39-5891-4835-a05e-20e1b1b9e1fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "f3bf76ab-8cf7-4394-8037-a007ef775ff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "0fc1be15-f6d7-421b-a1e0-4faa726dcacb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "afe941f6-9225-41df-868c-837a9911acbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 160
        },
        {
            "id": "c532db3c-53f5-4bcb-8ec7-9d7eae4bee27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 8217
        },
        {
            "id": "d56e1509-325a-4b4f-b9e9-609822442bb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 44
        },
        {
            "id": "048e89df-59b7-4021-96c8-74da31fbe0ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 46
        },
        {
            "id": "024313a5-5df2-48df-995e-935a8985c7eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 65
        },
        {
            "id": "4da3241a-13f0-4d6f-a7b1-3a5d140f68d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 32
        },
        {
            "id": "fe408a74-6aba-4640-8b7a-8dd159f5d724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 84
        },
        {
            "id": "21b532f1-0fd1-47a6-8547-010f1e706814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 86
        },
        {
            "id": "b4faba6e-c877-4a92-aa49-1430e6008f15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 87
        },
        {
            "id": "3b30ef49-0f0d-48a0-a60c-0ae2d2c764bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 89
        },
        {
            "id": "c7474494-7ade-433a-b2a6-25da1c427209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "5ad685dd-117a-40a1-a87d-31d7b882da89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 160
        },
        {
            "id": "52202649-dbf3-4252-9d46-7431a02036a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "f02901c8-7bd5-4c6c-9fab-524d811510e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "a8ba90eb-8b42-4e87-9e92-29d46c7e802c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 44
        },
        {
            "id": "c0b6d6cd-c006-400d-b5c3-485390368370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 46
        },
        {
            "id": "7f42eeb8-6b71-4320-bd76-78ac54cf49eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 65
        },
        {
            "id": "3a4142b8-27bb-4c89-b72d-3b3e3c44bfdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "2b223a32-4e5d-4273-afa4-1914fe9807c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "e36707bb-b5f6-4043-8721-e375e1cfcc99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "653a745a-b16a-409a-b3ee-b8ef071317a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "84850eeb-4b4b-4f43-bd49-6714bbaf1a3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "48b90d48-9539-495f-a9a2-947cd5f964cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "8e7e8bf4-f0a5-423a-940e-2fd962db3f0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 44
        },
        {
            "id": "54362c3d-3f98-4630-8ac9-ecff0066705a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "76407543-4e98-41b0-8c06-4250ae37f96b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 46
        },
        {
            "id": "e853c4f7-cf93-4b27-920a-b5c5251b6156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 58
        },
        {
            "id": "1fa45ad2-589d-4fcd-bfda-be85c18f39f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 59
        },
        {
            "id": "99fa89c1-fb54-44d1-a506-c058e88d3d56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 65
        },
        {
            "id": "33853257-c37c-4d4b-a02a-50c1ba2e193b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "4a0c9254-67ad-4db3-a059-9aea40d117c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 97
        },
        {
            "id": "b1ca9568-9b9f-422f-9ada-f80b67c68831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 99
        },
        {
            "id": "9dcaf4b1-3b11-47d3-a371-205e867ffbb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 101
        },
        {
            "id": "49e89c04-5d37-4019-8348-92f47f120c1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 105
        },
        {
            "id": "c61348e1-0319-49b4-935a-5026f33cd519",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 111
        },
        {
            "id": "b50ac169-f031-41e9-8898-c589fb5d9f0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "7301b84d-2cbb-4610-a86a-b85136c4b1d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 115
        },
        {
            "id": "31b40442-fa18-4216-b109-11cfe67a964f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "ffef423c-a0a8-417e-874f-1636fb67daf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 119
        },
        {
            "id": "3211d062-2f50-449f-beaf-64308c3079d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 121
        },
        {
            "id": "f06c9515-aeb3-46b3-8d77-a71ef93aa6a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "1468b96e-9574-4c63-b54a-679afab2dae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 173
        },
        {
            "id": "c9356923-faad-46db-9980-c9980ed591fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 894
        },
        {
            "id": "48847148-62ce-483e-b68c-79c771fcdc54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 44
        },
        {
            "id": "67c194ec-6a93-42a4-af38-722047bde753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 45
        },
        {
            "id": "1ad685a0-135d-405e-a05b-b2c1492a5236",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 46
        },
        {
            "id": "179bdbfb-dd19-466e-88fe-720af2b7a8a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "8c62281f-4c9a-4ad1-8358-034f036ffab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "4c4d187b-164a-458c-b4ed-09d6c33d7e14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 65
        },
        {
            "id": "a8a0474c-6c38-4d8d-8ac5-9785aa0283c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 97
        },
        {
            "id": "bc29a441-8e80-42be-b84c-96d49e0dca09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 101
        },
        {
            "id": "9d964382-b868-4d83-abd7-7df99ed78849",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "f846fa02-e4fd-4ef4-bf2a-eb6d51de1269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 111
        },
        {
            "id": "4eda8a5c-b383-4eb9-be08-fb1850ce7a37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 114
        },
        {
            "id": "edb1851a-a1f3-4434-bc68-ebf3a4adbe2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "d98b5f98-16e1-4fa1-88ea-45c74135af3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 121
        },
        {
            "id": "a4f35990-189c-4b73-9941-0c55298f79e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 173
        },
        {
            "id": "c1392abb-28bd-4033-87a2-2afd95edc8a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 894
        },
        {
            "id": "6f322cb0-7181-4877-a09b-79caa770eb2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "f4e63b2d-60b5-430f-8406-78cb9dab781f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "c9754ad6-99a4-4226-888e-2b497560ec81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 46
        },
        {
            "id": "f4309964-100e-4970-a819-538b933f9ceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "5620e44d-c81a-432f-afd2-901f424b4bcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "53f1da72-d3b2-4454-8ffa-613a2b85ee8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "f24ecee1-cd77-4c00-b215-6f5c9f98aa67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "4277a3fc-0c2f-44cf-a6c9-f685db49d55a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "8156b39f-4663-47ef-862d-f658122a3f2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "f76063f9-1478-4e6a-bad2-17e854c61f12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "8843ecd6-c8eb-4226-9da9-cc7d60829382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "6724ac07-b9d3-4f3e-9391-73fb19ad4e24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "0d314265-80ab-4c6b-8d19-bc75e1ed882b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "b15aae5b-097a-4f2f-92e8-a9818672590a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "edb01556-b8a7-4bbf-ac2e-c926519a2d2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "92dd9903-af47-478b-924d-cb4bd42783a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 44
        },
        {
            "id": "fd988906-0878-4194-9521-334a202e3450",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 45
        },
        {
            "id": "746a20aa-7c8a-42bb-a0ba-50293627e5fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 46
        },
        {
            "id": "cbe67314-65e0-4c05-b112-0ba09d46af92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 58
        },
        {
            "id": "4e7a04c2-dbc7-4d1d-9eb0-5618779ec821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 59
        },
        {
            "id": "cd4d96d1-54bb-42b6-8814-69b43b001bdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 65
        },
        {
            "id": "1bf5095f-9758-43c8-bbb3-c36276166285",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 97
        },
        {
            "id": "2c6084f7-28ff-4b63-9ca1-0802829a240d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 101
        },
        {
            "id": "3ef9f550-d3db-4ce6-8da9-991bc05e1ba5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 105
        },
        {
            "id": "e212c548-9e8c-4bd4-a7c2-f004ac002f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 111
        },
        {
            "id": "187c2616-3522-4b70-aef2-fa42ba99d461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 112
        },
        {
            "id": "3153758c-1685-4fba-9038-8ed2eb144ef5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 113
        },
        {
            "id": "535395fe-ba0e-4be7-b21a-c774908df3f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 117
        },
        {
            "id": "433e8d06-e96f-44c8-889e-58a70438c7ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 118
        },
        {
            "id": "6e6b427b-08a0-445f-8835-dc842144461b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "bc9a3624-7d43-489c-84de-51d18dd5e9dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 173
        },
        {
            "id": "b1cacfd1-840a-4ed2-9b3d-9957a7056fdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 894
        },
        {
            "id": "a560add3-b45e-4c8d-9891-004b9e705018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "6a6e09c7-04b2-4960-8bda-e22a47368cee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "b1dd125d-df06-411b-b116-f72f56c85765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "5e78a3e5-675d-426a-b7bc-62f48502ba11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "024e6558-c791-4510-9fa3-ded1c8e9462f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "f3a6dc5f-2007-43b2-b293-683a46c8a034",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 44
        },
        {
            "id": "d049c93d-af4b-472d-afbd-0e373ca43c4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 46
        },
        {
            "id": "82d0968c-0d18-4926-968f-4e2eb73582e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 44
        },
        {
            "id": "d36c5deb-fa34-40cd-b91c-067465278dce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 46
        },
        {
            "id": "40c8ccf0-a6ab-4e03-83c3-563e2523a39c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 44
        },
        {
            "id": "123dbf0e-494a-4baa-b2af-950eec6366b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "Basics",
    "size": 45,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}