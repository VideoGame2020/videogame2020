{
    "id": "16885dd0-0b13-4e78-9b67-7529b5ac4bb1",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fHeader",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a5a7756b-9408-4569-a27b-fa26ec1dcaf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 77,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2edd1a5b-4a28-4912-b8d8-37e38129dd5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 77,
                "offset": 5,
                "shift": 19,
                "w": 9,
                "x": 151,
                "y": 160
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a4d7d850-a8fb-49de-88b9-6f1e71b33068",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 77,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 131,
                "y": 160
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "74afa360-6d82-4621-a088-07b63a0f3ed1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 77,
                "offset": 0,
                "shift": 37,
                "w": 37,
                "x": 92,
                "y": 160
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b7238b3b-c3c0-4e6a-a5f9-29ac1d409e23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 57,
                "y": 160
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "baaaab38-1ce0-45ca-bf6e-8112a93ffd31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 77,
                "offset": 3,
                "shift": 60,
                "w": 53,
                "x": 2,
                "y": 160
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a4abf838-d7f2-459e-9180-470006e17640",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 77,
                "offset": 2,
                "shift": 45,
                "w": 42,
                "x": 929,
                "y": 81
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7993b5a1-2355-497c-8a54-03c8d64b53e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 77,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 919,
                "y": 81
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5a028901-1b58-4cdb-81ac-7dd6f5011bb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 77,
                "offset": 4,
                "shift": 22,
                "w": 16,
                "x": 901,
                "y": 81
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f3e59d7a-3d10-4df2-8f99-6ab3d27dfafd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 77,
                "offset": 4,
                "shift": 22,
                "w": 16,
                "x": 883,
                "y": 81
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "bfecab4f-0efc-41e2-8d57-3e428194fbf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 77,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 162,
                "y": 160
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f035ee15-1e46-40e8-addd-79abf56c05d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 77,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 848,
                "y": 81
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0518210d-b820-4a7e-8e47-ce79cc846169",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 77,
                "offset": 5,
                "shift": 19,
                "w": 8,
                "x": 803,
                "y": 81
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "513cfb00-f620-4952-8dba-2c42d2728914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 77,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 782,
                "y": 81
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f14bf403-0943-4c79-b631-6655fbf0ab96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 77,
                "offset": 6,
                "shift": 19,
                "w": 7,
                "x": 773,
                "y": 81
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "926ba066-f8be-4eb0-90d0-e7fbd279d291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 77,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 752,
                "y": 81
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "eb83d2a3-0bf0-467f-9968-61863ebac8f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 717,
                "y": 81
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "de2845b6-7f00-4eae-875d-1093f11a7674",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 77,
                "offset": 7,
                "shift": 37,
                "w": 18,
                "x": 697,
                "y": 81
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f6043864-a039-488b-8164-7b0271d8de12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 77,
                "offset": 1,
                "shift": 37,
                "w": 33,
                "x": 662,
                "y": 81
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9fc3f04a-7af3-462b-981b-8b7ab8c787db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 627,
                "y": 81
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3cddc440-93f0-45a9-a040-37b2da118c46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 77,
                "offset": 0,
                "shift": 37,
                "w": 35,
                "x": 590,
                "y": 81
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e43e3820-c638-4a02-ad85-b35aaf47e3df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 813,
                "y": 81
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c8b9262f-86df-4aa5-ac62-ae779a95f276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 186,
                "y": 160
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5ba48031-eec5-481c-8651-8fa0b7b96b84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 77,
                "offset": 3,
                "shift": 37,
                "w": 32,
                "x": 221,
                "y": 160
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ef3db390-2789-4b7b-a3d4-a499fe9d9459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 255,
                "y": 160
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "819231ec-f2f1-4bae-a45b-5fc7525e8b04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 2,
                "y": 239
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "743811fc-ae01-4959-b856-e8f05586502b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 77,
                "offset": 6,
                "shift": 19,
                "w": 7,
                "x": 1004,
                "y": 160
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e43df94c-c947-49fc-8a52-91a504585783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 77,
                "offset": 5,
                "shift": 19,
                "w": 8,
                "x": 994,
                "y": 160
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f71907bb-577d-4dfb-8d1d-3c7cfb2cb855",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 77,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 959,
                "y": 160
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "93586d08-e1b9-4e4d-81f7-a2b9379e4756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 77,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 924,
                "y": 160
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "9eab06b0-c6c9-45a2-b277-aa3e679a449a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 77,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 889,
                "y": 160
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2af6df22-593d-458b-82b3-4d530cff3f01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 855,
                "y": 160
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d7a0ae38-78ad-4c8e-bb2d-b7d4c5979e68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 77,
                "offset": 3,
                "shift": 68,
                "w": 63,
                "x": 790,
                "y": 160
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0e7844e0-4d74-4588-a67a-ba7e04302b31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 77,
                "offset": -1,
                "shift": 45,
                "w": 46,
                "x": 742,
                "y": 160
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "72d037bd-6f8f-4e97-8389-956b3d0abfa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 77,
                "offset": 4,
                "shift": 45,
                "w": 38,
                "x": 702,
                "y": 160
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "6899ed83-7d4b-436e-801e-a282231711a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 77,
                "offset": 3,
                "shift": 48,
                "w": 43,
                "x": 657,
                "y": 160
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "8bae451f-44c7-4ac8-b549-0b6689bc3b69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 77,
                "offset": 5,
                "shift": 48,
                "w": 40,
                "x": 615,
                "y": 160
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "dcaeb3dc-0292-4a13-9cc0-a0d6b684220e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 77,
                "offset": 5,
                "shift": 45,
                "w": 37,
                "x": 576,
                "y": 160
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a6ded4c8-c8a0-485c-b83b-d87e493837a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 77,
                "offset": 5,
                "shift": 41,
                "w": 33,
                "x": 541,
                "y": 160
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "933bcce6-e051-47ff-85aa-494aa9b1adbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 77,
                "offset": 3,
                "shift": 52,
                "w": 45,
                "x": 494,
                "y": 160
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "7deb428b-b176-4bff-a078-8aa97a55d04f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 77,
                "offset": 5,
                "shift": 48,
                "w": 38,
                "x": 454,
                "y": 160
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "253390b3-cc6b-472a-8ab3-4de89e19cc35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 77,
                "offset": 6,
                "shift": 19,
                "w": 7,
                "x": 445,
                "y": 160
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "80844059-ad8c-47a9-a1ad-d5fbfa6d4eb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 77,
                "offset": 1,
                "shift": 34,
                "w": 28,
                "x": 415,
                "y": 160
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "259fdcec-6880-48c5-9dfa-96ca48d7cff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 77,
                "offset": 4,
                "shift": 45,
                "w": 41,
                "x": 372,
                "y": 160
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b0eec84a-b5a0-48af-bd05-416e264ecb79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 77,
                "offset": 4,
                "shift": 37,
                "w": 31,
                "x": 339,
                "y": 160
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "fdb07a44-8e23-49be-a58e-af5844b345e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 77,
                "offset": 4,
                "shift": 56,
                "w": 47,
                "x": 290,
                "y": 160
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f8f8c06d-fce4-470d-b5e1-f68a7e55e3c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 77,
                "offset": 5,
                "shift": 48,
                "w": 38,
                "x": 550,
                "y": 81
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2735b6f0-6403-4cd9-beb4-da4f0da38224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 77,
                "offset": 3,
                "shift": 52,
                "w": 47,
                "x": 501,
                "y": 81
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "3dc2cd1e-ca59-48b0-bd10-83040e129038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 77,
                "offset": 5,
                "shift": 45,
                "w": 37,
                "x": 462,
                "y": 81
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b205eede-f9d5-476b-bf34-8f3449b11117",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 77,
                "offset": 2,
                "shift": 52,
                "w": 48,
                "x": 772,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ffa8f6cf-394b-4ec7-a2ef-01e5e6fdd99b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 77,
                "offset": 5,
                "shift": 48,
                "w": 43,
                "x": 711,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d5f7068e-c799-4065-b5a1-6ca9b2267aa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 77,
                "offset": 3,
                "shift": 45,
                "w": 39,
                "x": 670,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "25ff05ba-67c8-40a2-863a-b8845b2cc23a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 77,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 629,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "62e99b7a-1348-406e-81e2-0b0546b5b1a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 77,
                "offset": 5,
                "shift": 48,
                "w": 38,
                "x": 589,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "65727043-946e-4783-b463-97ac42f35b68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 77,
                "offset": 0,
                "shift": 45,
                "w": 45,
                "x": 542,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "92d6457b-dfc7-4fc5-bdc2-de623cc95278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 77,
                "offset": 0,
                "shift": 63,
                "w": 63,
                "x": 477,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "760a58bd-6eb7-4e5c-8020-b0bfb234ed72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 77,
                "offset": 0,
                "shift": 45,
                "w": 45,
                "x": 430,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9876bf6c-b08c-45b1-b207-083c4b5a3cd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 77,
                "offset": 0,
                "shift": 45,
                "w": 45,
                "x": 383,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7e95c346-e386-4a2b-900e-76017a482b31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 77,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "075c3006-a045-43eb-8230-ebf4a230f0de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 77,
                "offset": 4,
                "shift": 19,
                "w": 14,
                "x": 756,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2c0ef273-e512-4850-b7c3-2c333f8cd623",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 77,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 321,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "187d87cf-bc94-4480-bbdc-aa32339e0cb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 77,
                "offset": 1,
                "shift": 19,
                "w": 14,
                "x": 282,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "43503f67-a9e9-4266-90df-fda40c0ec040",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 77,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 251,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c0ed206b-ed0f-4126-9332-ddc63eee13ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 77,
                "offset": -2,
                "shift": 37,
                "w": 41,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9793f3c2-b91d-4e63-b494-733ca07040a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 77,
                "offset": 2,
                "shift": 22,
                "w": 14,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "229c9531-57ef-4446-9a43-1bf4ee232d0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8baaf524-9834-4fe3-a920-644ee94d8868",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 77,
                "offset": 4,
                "shift": 37,
                "w": 31,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b6f89e17-63cd-480f-9e42-56787b54a54a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 77,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "683d24f7-2fb3-4632-976d-62b80126882d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 31,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "58e6a99c-576e-4051-a283-0b5408f779b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0b1a9ab1-7b25-4920-a435-904f61f2bc5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 77,
                "offset": 0,
                "shift": 19,
                "w": 21,
                "x": 298,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7d61147d-ae71-47f9-8265-fe391c59eb3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 31,
                "x": 822,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b342caf2-036a-4110-97db-42652a603868",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 77,
                "offset": 4,
                "shift": 37,
                "w": 29,
                "x": 118,
                "y": 81
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3b820c4e-e1bb-4994-8e52-d6ae67be09c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 77,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 855,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "fdd4a21e-bfca-48ac-ab3d-60ae8d118179",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 77,
                "offset": -4,
                "shift": 15,
                "w": 15,
                "x": 414,
                "y": 81
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3d73a4a1-9cc7-4e21-b32e-e66362baf7b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 77,
                "offset": 4,
                "shift": 34,
                "w": 30,
                "x": 382,
                "y": 81
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2bbceefa-a0d5-43b3-899c-e41432787384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 77,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 373,
                "y": 81
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f1835649-7e5c-41f6-8656-6a8a5539b66a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 77,
                "offset": 4,
                "shift": 56,
                "w": 48,
                "x": 323,
                "y": 81
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ab3f9f2d-d97b-42b5-8229-5dd9e4c2a13e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 77,
                "offset": 4,
                "shift": 37,
                "w": 29,
                "x": 292,
                "y": 81
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "7c38fc71-51a8-42e1-8831-c27f7dd970bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 257,
                "y": 81
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "706840ce-1074-4a37-bff6-7363ea40ca02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 77,
                "offset": 4,
                "shift": 37,
                "w": 31,
                "x": 224,
                "y": 81
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "84dea59a-ce2a-45c2-b27b-e125d73acbe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 31,
                "x": 191,
                "y": 81
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "868cfdfb-88b9-4f2a-9c1c-eed21b0832fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 77,
                "offset": 4,
                "shift": 22,
                "w": 20,
                "x": 169,
                "y": 81
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "014451e2-f4d9-444a-875d-05caf75d99a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 77,
                "offset": 2,
                "shift": 34,
                "w": 29,
                "x": 431,
                "y": 81
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "fe6ad22f-3727-4059-b7bf-8be9557f170b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 77,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 149,
                "y": 81
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "76e3dd6d-991b-492b-914f-c94a39a82705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 77,
                "offset": 4,
                "shift": 37,
                "w": 29,
                "x": 87,
                "y": 81
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1590da9f-2864-4251-a4a3-3fc54e4c2422",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 77,
                "offset": 0,
                "shift": 34,
                "w": 33,
                "x": 52,
                "y": 81
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a3849ba3-4fe0-4e2d-b484-6b45811768a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 77,
                "offset": 0,
                "shift": 48,
                "w": 48,
                "x": 2,
                "y": 81
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "27a1c197-93c9-4b32-aed9-6309e07f4293",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 77,
                "offset": 0,
                "shift": 34,
                "w": 34,
                "x": 984,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "04d72c6a-1c1d-4544-83ae-fdd0a1db84a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 77,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 950,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "3bf2733a-9b02-45f0-bbc6-b0282549484c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 77,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 916,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3cce89a9-ff60-47de-b8c3-c9a7c35bfa59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 77,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 894,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4aa5fb86-dd59-46ce-9487-b48c27254b4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 77,
                "offset": 6,
                "shift": 17,
                "w": 6,
                "x": 886,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2a76c33f-67cc-4414-93d7-5b5a4304b97c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 77,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 864,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "941d4f70-c19a-4d30-8222-bfa5763889ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 77,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 37,
                "y": 239
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "e4390b58-776b-4122-801c-0d2122b08bb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 77,
                "offset": 10,
                "shift": 40,
                "w": 20,
                "x": 74,
                "y": 239
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "234928fd-1530-4205-9a4c-ac5a9e8677b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 65
        },
        {
            "id": "be0cfe01-158c-4c11-be9b-de2e3258feff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "909ee0d7-8411-4cad-b690-5d616243dddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "6bc9f5c0-fe4b-4b62-beab-bf5d23a66608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 902
        },
        {
            "id": "a10dbdbb-9444-4156-ae86-2482e205b6de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 913
        },
        {
            "id": "0b10cd21-5439-4b3f-ad7c-ff6ae9608d8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 916
        },
        {
            "id": "0237875f-2511-4870-a786-f5cc0e342644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 923
        },
        {
            "id": "ea0cb879-391d-4bec-a3a8-e865bf911f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "4920af71-3f4d-46a4-8b22-ec7378159913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "75a61580-6d06-459f-8536-34c0c99105fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "cf01912c-f39d-4771-9dbd-f37b76d9d053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 49
        },
        {
            "id": "c567d6a4-1990-41c1-8009-791503a4ca0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 32
        },
        {
            "id": "c0445913-7dbc-4cba-b5c4-7c616a6cf409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 84
        },
        {
            "id": "cf423102-2604-47e7-9717-7edcbf8cafa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 86
        },
        {
            "id": "ad9fd702-e364-4a3d-9f60-966ccb7c840a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "3ee5de0d-a888-4864-b614-7a4cdf32456e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 89
        },
        {
            "id": "59526314-db8e-4d0f-b3b2-b0e215f0c0ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "69d697ee-b478-416c-8988-fd82fa9edc96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "8f28b234-812a-4c4f-b27d-028546ca59d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "3b286b0e-e04d-4b8c-8104-a6ae4d9496a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 160
        },
        {
            "id": "0dad3acf-0981-43b9-b8dc-21802e6754a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 8217
        },
        {
            "id": "08a80b51-d5a3-4621-9eef-d2ff67bb57a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 44
        },
        {
            "id": "44c1e068-0964-4148-8493-316c6dfa958e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 46
        },
        {
            "id": "7ccb843e-16bb-4292-bdce-c401ccf6a71d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 65
        },
        {
            "id": "0867b7d0-8a42-48c4-9778-652378d27050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 32
        },
        {
            "id": "d7361074-bc2b-401b-8760-e4a339b9ce02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 84
        },
        {
            "id": "94878013-b4f9-4423-a1bc-6b7517e6acb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 86
        },
        {
            "id": "2e58629d-0cbe-4088-b3f6-98d3d9c951eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 87
        },
        {
            "id": "fce4a3c2-791a-4636-a4af-b0f872087e5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 89
        },
        {
            "id": "8c1c8124-adb7-4734-9da6-10f17b0ea06d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "6e177b3c-456c-4a94-ad90-3b41ad4fadeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 160
        },
        {
            "id": "50313bb7-bdda-4c35-8527-95099509ac44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8217
        },
        {
            "id": "820110df-af9e-4b34-b062-f5a9fa526d22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "f7498a8c-1955-4172-9e76-4145b482bb38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 80,
            "second": 44
        },
        {
            "id": "f4d29cfb-e441-4726-a04a-377dffa227a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 80,
            "second": 46
        },
        {
            "id": "74c0c967-f676-465e-be49-eab30a9d7d4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 65
        },
        {
            "id": "b0e94bae-dfeb-4ad7-93db-9e255f0891e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "dd5599a8-ef55-4317-a57c-98c2e74124a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "91bddd61-2cf1-4109-985c-e671a3266148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "a1041a62-74d5-467c-b41c-b0b793901e9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "0b5c0277-f81c-48cb-a9e5-94637d4392dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "d00e7157-b38e-48c5-b526-4135b8bc16b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "c7203e35-e405-4a01-b104-be06898ddba7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 44
        },
        {
            "id": "24ead504-ef38-4e6c-8773-f8e1bd5b2e2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 45
        },
        {
            "id": "9abae125-ee66-476f-acad-f10b33ee5c9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 46
        },
        {
            "id": "746850d4-7c93-48b4-a311-e46bfa125fcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 58
        },
        {
            "id": "46757a30-5a33-4b74-bb1b-65ca238be062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 59
        },
        {
            "id": "5cfb141a-dad9-4635-99c5-230ef0ca095f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 65
        },
        {
            "id": "b329e77d-2af4-4d7a-a876-38f25dbd1114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "0a3d8a1c-a49b-4b36-853d-04b91f9a6f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 97
        },
        {
            "id": "e18bb3b6-1dc7-4ce5-b326-f3de63f6a5cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 99
        },
        {
            "id": "2aec8683-4fd8-4085-bb12-b6bc359f0cc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 101
        },
        {
            "id": "fdfa87c7-a6a2-4a97-8f17-829cbcce704f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 105
        },
        {
            "id": "69e6302a-a115-4e2e-af35-83c4653f0d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 111
        },
        {
            "id": "89496d10-90d5-4a24-8f46-2b9071138c65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "4363e425-44f5-430e-8099-a461bb8b1b39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 115
        },
        {
            "id": "adbac8f7-0e27-427e-9074-c76845776848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "8401d5d6-4a1d-436a-b687-a13079ee0700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 119
        },
        {
            "id": "b63c242c-fa82-48f0-865b-6a721770543b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 121
        },
        {
            "id": "8e6de6ec-bbec-46a5-a4b2-744e536bb6b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "a854e7d3-8cae-4790-8edd-7e3625c572fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 173
        },
        {
            "id": "2d1fcb6c-0f27-4753-b473-f770cade53a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 894
        },
        {
            "id": "1e5c13e7-004e-4669-a963-827855c9f37d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 44
        },
        {
            "id": "3eec8141-5567-43b2-bfb1-64e112b5b485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 45
        },
        {
            "id": "5eb5078b-5e6a-4929-99ef-5b6cafbfabfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 46
        },
        {
            "id": "8615390f-a934-4873-b498-1d3364229cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "b230f1b3-56d2-43c5-b686-36e09399df91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "c32dd603-7de9-43a6-b1b6-2e2d9b90eca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 65
        },
        {
            "id": "3bb63960-5d91-4de4-9131-f258ef26f723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 97
        },
        {
            "id": "9bc1b035-139c-49ce-a8dd-df8f2d56d3c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 101
        },
        {
            "id": "bd74b9e1-1658-47fc-8792-4161cfec4044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "3568c809-2693-412c-a021-2cd5af11ff2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 111
        },
        {
            "id": "1064aa32-5221-4959-9939-3517989b04c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 114
        },
        {
            "id": "82ec89ba-ff2d-471b-bf98-15e9fb9c60cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "c3a91f76-e097-4cc8-8910-a07f25ea7723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 121
        },
        {
            "id": "025ea69e-d6ec-4ff0-bd6c-b981a8cb621c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 173
        },
        {
            "id": "de88baaa-ecd0-4ff2-a9a7-1ad369a8424a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 894
        },
        {
            "id": "94705bb5-c119-4a8d-be5b-6780de0cf766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 44
        },
        {
            "id": "515405e0-7772-42de-af83-de7bd251c4bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "cbf23a8f-ba85-4900-9d34-49354fb2ee17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 46
        },
        {
            "id": "a32d673f-326d-4b20-a5b5-7e2b1295cf49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "e82bf8ac-2c5f-4142-bedd-ce30d9ba1de9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "dd0f254a-5440-45f7-9d2b-0d9eb1aed4a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "6cea9972-4263-4429-8bd8-65404f2eb57f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "8419644f-b5d6-4f16-87a1-1d6145744dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "02bc96f1-0b95-4ee6-bedb-3f74d12817f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "4743f619-3c1e-452d-8721-d207b68c5ff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "928657c3-49c2-49b3-a0fd-c206ad074a48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "93cd4697-466f-4328-999f-e54d23a6b743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "4c4c35af-0168-47e1-8fa0-291a83865dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "83b3e348-1432-4f5a-9272-c8271692635e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "b463d24d-89b3-4ada-8188-19c938a54346",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "e9ead8b2-2d0b-422f-b943-8ba47363d7ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 44
        },
        {
            "id": "1c736e11-a05a-4c90-8f2b-2c4a9dd4a578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 45
        },
        {
            "id": "deee5d8d-407e-4fbf-b237-d02ddc96d82c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 46
        },
        {
            "id": "4f6026a0-151a-42f9-8dba-48701bc12527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 58
        },
        {
            "id": "790f889d-de42-412d-8859-6fcf8df40cce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 59
        },
        {
            "id": "7a74bf10-ee66-4ef8-8ef6-f3f36106ef7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 65
        },
        {
            "id": "51783066-aff0-406c-8fa1-0df379d19c7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 97
        },
        {
            "id": "3dac9187-3820-49b9-9fc3-0879ca95e0cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 101
        },
        {
            "id": "65793516-3018-4528-aced-86a7278e67e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 105
        },
        {
            "id": "f9530ea0-c7f9-4b91-b458-cd587b94edaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 111
        },
        {
            "id": "08e38234-3a77-4dc8-976b-6d639707be76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 112
        },
        {
            "id": "e6ad38ea-10fd-44a2-85f1-8d2910396b01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 113
        },
        {
            "id": "3f861c93-a05a-4431-b0ef-ad56a0e4263d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 117
        },
        {
            "id": "80fd8bcb-61d4-4819-9f12-e698afdd4ab3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 118
        },
        {
            "id": "c1c2e279-9ac0-4e8a-a567-e196a7da180f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "b75d0fc9-b21d-4887-a435-2ff92287bbcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 173
        },
        {
            "id": "5ce5f71c-2c33-4d7d-8399-8051431bf873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 894
        },
        {
            "id": "1b057264-1390-4d8b-8df1-f10623baec94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "de8d618e-ebe7-4684-9c2a-fa0964796bcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "dbdddbe1-68e9-488a-a55c-4fb47a728475",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 44
        },
        {
            "id": "d246ef33-fbc5-4218-862c-133ef2abb4d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 46
        },
        {
            "id": "6aaea187-e102-41a5-bdfa-d5738d777ef5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "90316442-a14f-4a6c-9e0f-6133fcf0216b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 44
        },
        {
            "id": "325d3b11-752f-4ce4-bfd0-447897285977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 46
        },
        {
            "id": "7716a55d-5e05-4813-8eb6-af946c4a712d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 44
        },
        {
            "id": "c4848e29-a9e9-42aa-8bed-c9c53c7f3330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 46
        },
        {
            "id": "f56a34d8-801c-4d6e-ae2b-cdc14b1df6ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 44
        },
        {
            "id": "4e28ffdc-8899-49fe-a420-d64e433eb882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "Basics",
    "size": 50,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}