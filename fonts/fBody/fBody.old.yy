{
    "id": "07fb5052-ab0b-497a-9710-59a321c2a9b4",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fBody",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "75543dd7-6ac2-48ce-bf6d-389b56c6ebcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3270fc7d-2c91-441a-81e7-05dc7be8be65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 149,
                "y": 82
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3ef75af3-4d2d-4a6f-89f8-88ce607ec281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 137,
                "y": 82
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "89d3f5ea-33a3-4e9d-a426-2340a3b0dcc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 117,
                "y": 82
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "fd5b1f5d-dda2-4ac9-a85f-7061ed9e46fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 99,
                "y": 82
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "32df4e0d-1018-412a-8991-bb4002a39b15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 38,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 70,
                "y": 82
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2ae219b9-34a0-4b65-9a98-eb8784383484",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 38,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 47,
                "y": 82
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8484f341-4b44-4f14-bf8f-7eb3604c842e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 38,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 41,
                "y": 82
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6755feb1-2a9c-4171-b879-038a17a6b686",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 38,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 31,
                "y": 82
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f4776a55-e79e-40f6-9067-7b758f1a7e67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 38,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 21,
                "y": 82
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "73457900-5ab4-42ee-8733-adc2e9d7bdbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 156,
                "y": 82
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0daa2e67-8193-427e-bacd-7ee0cbaaa6f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3773308b-1ee9-4985-9fb8-a36edbea2ba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 472,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b3e3f7c0-59de-4a6b-b2fc-7029f8ec56e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 38,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 461,
                "y": 42
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "608d44cf-f1cf-49aa-8157-936f6efe999e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 38,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 455,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "17f111d0-9b8d-468d-bb97-5cc598f092ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 443,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b8434475-e62f-4f3c-8010-e873338f0ab4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 425,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4c322ee5-9545-4f1b-bd20-9d913119b95b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 38,
                "offset": 3,
                "shift": 18,
                "w": 10,
                "x": 413,
                "y": 42
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "aa03fa2a-3b4a-4cf4-a473-0b236c1236fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 394,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7fb124fc-1760-4b1a-aadd-43917bf18259",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 376,
                "y": 42
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "eeefe766-205f-4d74-b0e8-7ff436ca81f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 357,
                "y": 42
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5f6928d2-9402-4b9d-911c-32b63ed66015",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 479,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d16a36d5-c79d-4460-a145-de1566565a7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 169,
                "y": 82
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "15fa5dc2-113b-4368-8c64-ccf3064ea471",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 187,
                "y": 82
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "671ad48c-11be-4fc2-8935-cb95b4e0203c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 205,
                "y": 82
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "02a7e239-e3bb-43f1-80a0-7be7b91ef17a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 125,
                "y": 122
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e0c41255-0772-4dca-ad08-d7f3ecb54620",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 118,
                "y": 122
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "97249751-ff08-4ccd-aac0-ded6cf4b97e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 111,
                "y": 122
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a61ad6d1-5959-49e5-94e0-404f9399939e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 92,
                "y": 122
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "67f1d8f8-67ec-4edb-a602-1495e9ecab6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 73,
                "y": 122
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e991d386-db6c-4f16-9dca-c8a7b9f61eea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 54,
                "y": 122
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f288a2c0-81cb-457a-9a0a-f786294c9cca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 36,
                "y": 122
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "96e6b52a-9701-4b07-808e-18079fc47e03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 38,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b1ecf221-54b6-49a9-bb52-f210ae1eb853",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 38,
                "offset": -1,
                "shift": 22,
                "w": 24,
                "x": 465,
                "y": 82
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "fb0023de-ff87-465d-b696-46154d0b19e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 38,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 444,
                "y": 82
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a252cea8-a938-4cb1-91cb-9fb0243c7ad6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 38,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 420,
                "y": 82
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "99138ff6-7ac3-4783-9286-783ac94fefca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 38,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 397,
                "y": 82
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "65856568-8792-4d23-9a3f-07730eec0bd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 38,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 376,
                "y": 82
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ad70ebe0-fd8d-44d8-9d8c-3e4a52f52383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 38,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 357,
                "y": 82
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "7970f284-fb35-49c9-be51-bf04afb92e92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 38,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 332,
                "y": 82
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5438b088-9ce9-4246-b7f0-98010e78326f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 38,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 310,
                "y": 82
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "fa15a4b1-05d5-473f-99aa-99cb9164340f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 38,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 304,
                "y": 82
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "07dc0077-f4ac-4edc-86e8-9dd21a8912db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 14,
                "x": 288,
                "y": 82
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "2e807d3a-b392-49e9-a357-676cc784932a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 38,
                "offset": 2,
                "shift": 22,
                "w": 20,
                "x": 266,
                "y": 82
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2a4e4f39-b0fe-461e-9200-3133f773b3b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 248,
                "y": 82
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2fc5ca93-2ddb-4133-9728-986afc3af35e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 38,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 223,
                "y": 82
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a5116b72-d8d7-45b6-ac35-688f720530d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 38,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 335,
                "y": 42
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2177d0e6-1ab8-4c91-b40b-4baf2018bd56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 38,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 309,
                "y": 42
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c1840892-3f9f-45bb-85b1-c52d60587d44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 38,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 288,
                "y": 42
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f91142eb-4fc6-4938-8a24-0225b4af9ae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 38,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 410,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "3d6c9f33-9253-4214-bc11-10afc16e1472",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 38,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 377,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f97a765a-8ddd-40d6-a011-3f2d48bd5040",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 38,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 355,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "39b1afd4-3fa1-433f-aeb8-4469bf71e556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 38,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 333,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b57b3444-63e3-453b-b9da-8d9767e39b51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 38,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 311,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8b23137d-dc93-4131-a68e-00704e6ca298",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 287,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c7435721-8c77-4af4-b18c-6c713d80d7fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 38,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 254,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "641a043f-5fa7-4095-a63b-ba1c287d0e38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f9cf6055-b651-41de-a09b-a0dfd169e293",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "0220deaa-888a-4e81-b7bf-82846cf4b294",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 38,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "92e0cb97-03ba-4b8b-b0e5-df0d5375ba3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 401,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b525d2e8-6bd8-4a0c-9812-44d188588d79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "dc6a172f-bc98-4970-8deb-301fc401950d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f3f1ff5f-aa99-4520-ad0d-18ef3b6baee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 38,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ceaa4ac6-e9f4-47b8-86ea-2794cec9468c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 38,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7428b68e-183d-4338-8324-1d162cf0dae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 38,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a915459b-bb84-40f6-9903-ca2c49257dfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "923adc84-649d-431d-a4b7-ea3b6786b836",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "347fb4cf-1e75-409d-82cb-1aceb2b82f07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2da40315-8742-469f-9efe-8fe79f548ed8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "cd406881-21f8-4e89-b1ae-ab7d7377a04d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d375ae9e-672a-43e7-89f3-231988b35203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 11,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "73e03153-1f8f-41b1-8c84-7c3e74cf8e70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 436,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "95f3a8b9-d4e4-42c3-b266-b32d21a2ac83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "823832d0-94ef-4a4e-ba4d-0bb5763f8cd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 38,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "16984443-7d43-44d0-9678-bfed66345955",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 38,
                "offset": -2,
                "shift": 7,
                "w": 8,
                "x": 261,
                "y": 42
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "facf4b7c-ecb5-4439-a355-07eb0b48a8cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 244,
                "y": 42
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "51b28213-68d9-45a5-9a72-7fba0cceed4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 38,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 238,
                "y": 42
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "2561d781-b870-41ef-bbda-54c122d5427f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 38,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 212,
                "y": 42
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "aea54312-5992-465b-ad67-3fa792feac87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 195,
                "y": 42
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b1fe3a97-4d0b-419e-b9e9-8d936fab67be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 176,
                "y": 42
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "21aec223-7da4-4e2c-bdde-0050f2927f4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 158,
                "y": 42
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e6f7f22c-fca5-42aa-a32d-cae6d28505ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 141,
                "y": 42
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f1c2d4e2-bed0-42bb-a8f9-323a11a3011a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 38,
                "offset": 2,
                "shift": 11,
                "w": 10,
                "x": 129,
                "y": 42
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "708b46ab-2de7-4aa1-9b46-f679f112994d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 271,
                "y": 42
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "96a4280e-f806-491c-a942-86a0742b4065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 118,
                "y": 42
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4cf49d65-e33f-4051-8b54-d99bfb55e04a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 85,
                "y": 42
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0486bcf3-84de-4212-8230-9034c4d6b0c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 66,
                "y": 42
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "eef4f4b5-49de-41cd-af0b-06ccb1963063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 38,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 40,
                "y": 42
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e8807276-cb0b-4663-8e60-e795160e68a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 21,
                "y": 42
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2b2d3316-2b1c-4776-9207-3895dbbbc919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "5b36dc61-3d81-48c8-a942-0cd2dcc39420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 491,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "962ec109-2a2d-42fa-83b8-5ca9742cf0de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 478,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "116ee958-ed7a-4030-a3be-7d6d8b0bf399",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 38,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 473,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d2fd04f5-70f4-4289-a701-db0a2840410e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 460,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "edef0a8b-b376-4038-a292-598591020c42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 143,
                "y": 122
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "fc3af629-3db2-4781-8f7e-6dda53d352c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 38,
                "offset": 5,
                "shift": 20,
                "w": 10,
                "x": 162,
                "y": 122
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "0c38025a-b5ff-4f9f-90d7-948faec9ac45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 65
        },
        {
            "id": "c8a3ba3a-1dc0-4228-873a-049488a60e3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "39a9e244-da0b-4102-9d06-f08cfb085d52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "0da7971b-93dc-4582-a2ac-d153256e4d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 902
        },
        {
            "id": "c22fba10-497d-4358-96e3-05ed404c852d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 913
        },
        {
            "id": "439891df-e638-4a06-8db3-6c09bc556de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 916
        },
        {
            "id": "0b9db8e1-59fa-410b-b8b5-49e25462c60f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 923
        },
        {
            "id": "c62ed2c3-9a96-4269-9f05-759285bc02cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "6815b58c-66ef-4213-bc1a-e1554aff580c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "91e7fe54-8a8b-4451-b62e-578bc33c6f8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "cc432c66-109c-4acb-a5cb-d9a6fa40f2e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 49
        },
        {
            "id": "e9375670-477c-40a0-b4a8-86fa194a7838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 32
        },
        {
            "id": "979ff30a-fc04-4962-b0fb-a2c44d7ebe9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "9465cab1-089c-4e59-8255-5f9cb539e1f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "51063533-6118-4589-93e8-8747359c2ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "9dd47223-8340-4801-b1fa-9ec2dbd459ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "a7b80b6c-3c06-4f1f-8f02-7f6ef9dcd4ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "2468283b-7c3c-42bc-a677-b9a1c1fb67a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "df2e0219-5334-4038-a62b-c5c6f5b18f69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "7c16bde0-a15b-40f9-a852-6556773062c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 160
        },
        {
            "id": "6ba6d9b3-6682-44f7-8f90-319705ee991a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "a1beccb6-85f4-460f-ab4c-0dfa5ab91530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "12b5f035-6aa5-42ee-a476-c8751fc76d05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "b8f9d418-a0cc-47da-b6d7-71a42c50cf45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "167c31ac-8f92-4de4-8eed-a4807d305fda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "1459d768-aabf-465d-bccf-cfc8086301ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "f28c625f-77df-4eee-9dc7-5f6d336371db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "12dfcaf1-ae88-4a43-b141-f365b4d5e27a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "9371acd7-7295-49dd-a10a-58af45c0444d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "fbbcc175-1567-49cb-9ba9-4dd4eaa5d061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "d10d7f01-5e23-47bb-861b-aa4fc21b0a02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "14a7e798-e736-4055-8c52-c5fa1d3e1173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "00f6751c-1ea5-42c4-98c3-dd34a3fcbdd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "67c95950-9784-4cf4-a3b2-116a5446f93c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 44
        },
        {
            "id": "45cfbc2c-f274-4f64-a1f5-4c2cce381c2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 46
        },
        {
            "id": "ccb7901c-67f5-4978-9d30-81bb2172acdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "d18994cc-4a56-4868-954d-c39f25efe624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "0f628234-a22f-4d2e-9fdd-636cbc12963b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "bf6f48c8-58d9-406b-a361-079968e81073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "77d21008-643a-4954-b48a-0a73684923c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "16a0b3ee-9ef7-4734-8a0c-e8f14a5813d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "747c1fa2-6e0c-479a-a9fd-78b5f7d2c97b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "576c28d7-d1d1-4b1e-a2c0-c960786c8223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "30384db1-45b9-41b5-a14c-f8981ed66c54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "5a6c818b-7295-45c5-99ce-dbab11b5eff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "3371400f-4f3e-44b1-88a3-f269c209a005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 58
        },
        {
            "id": "1b7325ae-ffd6-4ac3-8527-bf3318cff674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 59
        },
        {
            "id": "71ca8ddf-10a6-46db-86ef-e50b8c7c4255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "6069ea16-0a06-498c-90b8-7b50a4459a6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "13159342-ce8e-4e17-8bbc-8c45081923bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 97
        },
        {
            "id": "dc89449e-5b33-4d63-9b40-f603eb87caa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 99
        },
        {
            "id": "20e82ff6-6ba1-47a5-b793-5b7e9b0ca671",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 101
        },
        {
            "id": "89e2d9e0-9071-4787-9daa-71fa8eef74c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "c5a73336-406a-4138-8a8c-4c9d9e93a08d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 111
        },
        {
            "id": "06bb508f-259b-4657-a592-bbc17f129958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "78f5f814-0004-40af-904e-a27c5914ef86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 115
        },
        {
            "id": "ea6ce684-fadb-4387-9970-3d46859a1343",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "d8f28744-91e7-4389-8e42-d24db91fa55b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "69415e3d-4ea2-40f3-9fd6-482c829ad2b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "592704f4-cfc8-4ab4-a3ff-0cdd03915dca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "7b5290ae-e2a8-4575-8b06-81aa58b6114b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 173
        },
        {
            "id": "ceaf0fb0-ec31-4f2f-9f57-d4ae7401b1a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 894
        },
        {
            "id": "0ca28a17-f808-456b-9558-87098018a123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "42c41067-a115-438b-b573-a65db047f7d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "02691e60-0549-451b-8a73-e81e522a8174",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "9560659c-e720-4686-9a5c-906079ef6428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "7eff4adc-7bb7-4470-821d-3e3706539292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "8b2c035e-a3f7-49f5-a52a-7f249055f626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "9296492f-f946-4dde-b104-63d6cabf7146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "a04fec51-b186-49b1-8b14-e7e2c317bf9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "4da2971a-1db2-4696-9644-a082d67af888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "617e23ca-df10-4cbf-9fa9-817bc0d0d7ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "e47fb58a-97c2-4f2f-b247-a1f5787bced9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "971a605b-1216-445b-8a88-b00b66c1b2bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "9da53881-c78e-4257-b7f0-938fb437ba46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "1b0fa016-5d53-4094-97ae-14c3a68e1b04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 173
        },
        {
            "id": "47df4d66-3b77-4770-9cc2-3314efcec8cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "cafb3284-68e5-422e-a064-beb6f3474249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "c0b7d30c-7934-4244-bd09-6f7a059e8155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "c93db4c1-e37d-481b-a800-12702aec0f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "a1ea13ff-4389-4a42-9203-20cf9b0b06de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "8e51e036-f955-4ee5-9ea0-ddaa5486e94d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "200870fd-fd5a-46b3-b0fc-e90c9e16d8a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "9c0ab7a6-f356-4adc-a1a5-d9ca21f7f165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "ef5919dd-8b57-4436-848c-0a9de5ea997f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "10c62fae-2f8d-4368-8a6d-765eb7e7ea6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "62882d5f-ec4e-4c47-a001-4b6037ed5b97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "7968fd65-3161-46e7-aee1-07bc37cbb561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "e657831e-21d8-4133-99ca-6e5b165b1479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "5ea08998-2ef0-48ee-af08-25fc23015976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "3aacd473-3b27-4892-8468-5f295fa1f0d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "542a1081-2897-45d7-87d0-4d3b8fb34dc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 44
        },
        {
            "id": "5862d34e-051f-4c1e-adf7-880459272419",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 45
        },
        {
            "id": "f04cc128-1090-4ce2-ac01-ad3d20565d05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 46
        },
        {
            "id": "6eddaf3f-cbfb-4625-94b3-235dddba7f83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 58
        },
        {
            "id": "23e0938f-c9c4-43b0-873a-a0e915a5f9b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 59
        },
        {
            "id": "ac3d32ac-2079-4a0e-acdd-7ddde0cf915d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "fc1ec616-1802-4f90-bdd4-d941d577d3bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "1c94b17b-d930-48b8-8595-0291c3d05848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "1911e8ae-aa1c-4357-a54b-0a21832f2488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "cee62bcd-a603-46cf-8d57-ea8fcc4065b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "f5be7ac5-e80b-495c-b61a-36b03a59755e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "475fc3e5-54f9-4440-8b7a-658675e59e7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "fc50bade-c5ff-45b8-81c6-2f1279f85aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "c1b163cd-966d-4f57-9164-93dc92961778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "1c49b665-a0ce-4291-a933-aa3f4effe7ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "df4a9b7c-2637-4954-8c4c-fb4073425951",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 173
        },
        {
            "id": "f9fa12f1-1d8f-4591-86f4-8bc6e73ddaff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 894
        },
        {
            "id": "4daf469d-66b1-40cb-a63a-4340f7761c86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "ea549a91-937b-45c3-bfe6-1284ba596e98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "23b1070d-3f96-44ef-b121-4665f8fd6f22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "00b09fa3-bd6c-47dd-82a2-8100de7a61be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "3afa539a-be82-45cb-bfe4-65f5a912ac02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "3755eb76-0b4b-4950-97fb-9c5fcee45011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "eaf97ff3-9054-41fa-8aff-6f7454bb1f99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "60783823-3508-4d39-8359-ecf5b6666ad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "8c1d8d49-3e88-4659-99f6-2ca56816f761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "422aa0cd-2bde-405c-8d39-909329c31f3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "96b33f4d-5080-4ee3-b862-0c91e4607c02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "Hit the notes",
    "size": 25,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}