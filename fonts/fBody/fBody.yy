{
    "id": "07fb5052-ab0b-497a-9710-59a321c2a9b4",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fBody",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b8ba3fd4-0054-47be-89fa-9af1e94748b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0b49147a-e4c7-4571-b118-9427f2a45047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 2,
                "y": 134
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "92aa1cde-fb26-414e-849e-d17f2caf103e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 31,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 240,
                "y": 101
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ed10b62a-3b78-4438-bf68-886755b1534a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 223,
                "y": 101
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b8160333-649b-4179-a942-a9042bd3df40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 207,
                "y": 101
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "57022f2c-6101-4bd1-a8c9-132d68c28be7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 31,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 183,
                "y": 101
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1ddf0338-ea01-4f77-81f6-124937e7b15a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 164,
                "y": 101
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d7a38be0-adb3-40ed-a66e-d964971f7f18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 31,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 159,
                "y": 101
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0f57adaa-7bb7-4344-ba2c-f2fada5801ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 149,
                "y": 101
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "53fa8afa-6c1f-4aba-ad83-b819a7f38366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 139,
                "y": 101
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "bc7e721d-9f65-4407-99df-bc348babc0e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 31,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 8,
                "y": 134
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1274bb47-462c-4986-a74a-f7bee15a31f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 123,
                "y": 101
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "897a9bed-ee6d-47f7-b25f-d496e18c2baa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 102,
                "y": 101
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1e859d79-d764-45e1-b72d-e1f61aac458b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 91,
                "y": 101
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d71b7ead-f259-4c3b-abd5-f9128b870ee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 85,
                "y": 101
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a6597fda-fab8-456f-bbf3-70ea1c531d2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 75,
                "y": 101
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "43035eb1-89d6-4b81-ade6-2128fdd7d8bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 60,
                "y": 101
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b90ba31a-73c1-4a31-8887-0b3d87f1825c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 31,
                "offset": 2,
                "shift": 15,
                "w": 9,
                "x": 49,
                "y": 101
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8b90dac6-a574-49fa-8672-2c35eaa4dd66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 33,
                "y": 101
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "6f3427ab-2db1-42fc-ad34-c4c7f507c20c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 18,
                "y": 101
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d8dc57f2-c6ce-4151-837e-0327512d380b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 101
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "2c8dd63d-155c-47d2-a60c-ab6cfd62b499",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 108,
                "y": 101
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ffd16b23-9414-4f36-a406-3fdd0922481c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 20,
                "y": 134
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "40c5aef0-1e88-4ac4-bc87-3e874b80d9bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 35,
                "y": 134
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "24f3e4e8-e346-4a52-8674-f2ab7d2ad81a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 50,
                "y": 134
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8f03c47f-619b-4479-b748-0047b3778b6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 145,
                "y": 167
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "330d09f4-50e1-4b07-a1db-97911e674634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 139,
                "y": 167
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2928514e-b3b2-458d-8b3c-bad9d7d249bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 133,
                "y": 167
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3004237a-fead-44f5-8fe0-dfcdf7a3b413",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 117,
                "y": 167
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "29314c76-2406-4d1e-9330-32295edb9023",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 101,
                "y": 167
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d1951bc6-cb0a-47c8-9f7b-33d3389338d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 85,
                "y": 167
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "fc978cfb-658d-4e96-a996-0bd59d4f8b49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 70,
                "y": 167
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "327f46c0-bc9d-4d62-90fc-ed0e2fb7c718",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 31,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 42,
                "y": 167
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d2bd488c-abbe-4316-b6d2-c46392add32d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 31,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 20,
                "y": 167
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e1f9dad9-6066-4e56-aec7-e621996e818c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 167
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "500e3b19-f568-4f51-b362-290a2a114099",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 232,
                "y": 134
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9a1757fd-df01-42b0-b21b-1bff52410554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 213,
                "y": 134
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b058e7a8-6fc9-443f-8c33-c42ce3eef230",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 31,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 196,
                "y": 134
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "05aa9771-0a35-4f20-8a5a-b9be00bf254d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 31,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 180,
                "y": 134
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "717555b3-2f6f-413b-899f-0b0f787f26d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 31,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 159,
                "y": 134
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "0e342505-8559-4f57-ad13-aad5964aaf49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 141,
                "y": 134
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6b7f9d4b-731e-4658-9eeb-21eb634c5b01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 135,
                "y": 134
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "dab41d4a-b87c-4183-8da5-92afc1fd7e3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 121,
                "y": 134
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ef7e1991-fd09-4fcf-87a3-02539c435635",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 102,
                "y": 134
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d78d347a-c5eb-4258-8eef-7dbe396feb42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 86,
                "y": 134
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0c315ab2-6934-4d97-8e6e-c55ee601e8ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 31,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 65,
                "y": 134
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3c6afcf8-a790-4ca5-b89a-0c892545ec9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 237,
                "y": 68
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "28ffbc1d-3dbc-4af5-90f6-41cd53e8fd83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 31,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 216,
                "y": 68
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ad9e6fd6-d639-47c0-b2c3-b28166725813",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 31,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 199,
                "y": 68
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "53b9544a-24d7-4fd9-bce1-53d327c35cd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 31,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 105,
                "y": 35
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fe37bd3a-dead-4c10-be17-91adb4ed4de6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 76,
                "y": 35
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "32bd49bb-a489-40ce-8990-65c886fcfa9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 58,
                "y": 35
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "8e274563-847e-41bf-a312-c931eb92efe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 40,
                "y": 35
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "27e5d2be-9668-4e81-a907-b9c2eb04003a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 22,
                "y": 35
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0566e5af-07ee-46ac-bd9f-2e220e40f4a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c855fee7-6ca9-487c-bbe7-4d36dbf57386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 31,
                "offset": 0,
                "shift": 25,
                "w": 26,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f2db2a2d-14a3-471a-99e0-b75a868a9feb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "848255e1-5b46-4c29-b045-95f21343aedd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "95e18717-4117-4868-93dc-b3a5e49b5d1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b4870f00-a4ce-4420-a2cc-2958914b622c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 31,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 96,
                "y": 35
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "57ea412a-2a8f-458d-a836-962040106ef6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "c9051c43-e641-489d-82c9-9ba748f05cf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d67ac6ae-aa9a-481a-8957-f3113582aa34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 31,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ed6107c1-62ca-44a0-b747-965f768a0a6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 31,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5af0b7b4-f7d5-4d84-8cee-bca65e616a0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c1c3990e-d4f3-4004-af50-86d0f1c7e502",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d4bde7c5-1c58-4a10-9cbe-2720b2699d31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0bcead2d-76b1-470c-aecb-44ea28331aa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 31,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "fb6fec35-bd41-4834-85e5-ee4e34a6bff2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f0deaa2c-03c8-4099-8b57-4db86372e9e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "da6e091c-5291-4e13-ad69-fb814acd443e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "bc3c2cf8-f030-44be-95a7-abba76ede099",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 127,
                "y": 35
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "540c634c-38aa-4ce9-a8a8-9140ec0e95a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 33,
                "y": 68
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "cac8d212-059a-42d6-aa61-1b8c4b4cb2b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 31,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 143,
                "y": 35
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e7f6adcf-5e9f-45ea-8389-094e4e679544",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 31,
                "offset": -2,
                "shift": 6,
                "w": 7,
                "x": 175,
                "y": 68
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b1959d28-6f58-4975-a75d-ffbfb206c973",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 31,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 160,
                "y": 68
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "95e6bf91-4f10-4eaf-81e9-4c49d768906f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 31,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 154,
                "y": 68
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "0c023163-8deb-4b9b-b60d-98c6b6500752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 31,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 132,
                "y": 68
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "9d08d716-8491-4e26-bdba-8d383aa5032d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 117,
                "y": 68
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "26599f54-b7b3-4872-a381-3edee33ecf99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 100,
                "y": 68
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c09dcaf8-098d-4390-92f3-bf3770aca626",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 85,
                "y": 68
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c613ebaa-449b-40cc-a9f3-10eec0b3670b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 69,
                "y": 68
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b1153dd1-14dc-49c6-9db0-05e9e6f9625f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 58,
                "y": 68
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "0fb38264-d647-44fc-ae3b-75c759bc4281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 184,
                "y": 68
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "eb1c6002-da5f-488d-9e3e-1d3537da4d15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 48,
                "y": 68
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "1e6126db-5e06-4c77-8394-c96bdcdfd65a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 18,
                "y": 68
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1a525ca6-1a28-41a0-be76-58f55787d2fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "50c30aff-d874-4e02-85a0-93f7edcdd3de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 31,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 223,
                "y": 35
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "5c73e6fa-a873-4352-85e1-29da8a72ec30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 207,
                "y": 35
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1437584b-d2e8-4fe8-98e8-fad85e506da5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 191,
                "y": 35
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "37006b45-6353-4496-9da5-42471d6426c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 176,
                "y": 35
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c9fcebcb-c28d-4791-b74f-945fb1651057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 165,
                "y": 35
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1f6824e1-156f-46c8-bf78-c1698291f4c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 31,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 160,
                "y": 35
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "4cc16323-4d93-4afd-8d9e-bcf9bbc18793",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 149,
                "y": 35
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "9d84292d-cee0-4990-8dcf-5c1798cdd86a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 160,
                "y": 167
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "20b2f52d-ba52-45e9-b44c-a08c8fc78757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 31,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 176,
                "y": 167
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "c0d72c01-fbc8-4cd0-9a7c-48c9068bc88e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "ec142134-7fbe-4246-b798-93175296d9ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "3f7a5d67-8177-4ae6-b03a-bcccc3e051b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "efd1bd4b-cc7e-472a-954b-b4e4465aec29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "cd8edc7f-0d61-4fbe-8d4d-ed329eccbb68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "d82cc135-3b40-418e-b26d-63ee61607539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 49
        },
        {
            "id": "5caf4fae-84e0-4db6-83e7-1d7f728465dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "9735a94f-d411-432b-8508-deba41bc9202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "210a41a8-919d-42b3-91b6-5795a992f8ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "e132b203-c20b-47a7-a010-bb0d2ffd1c40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "c936e245-ce9c-4fe3-ab7c-d529680f0bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "b98e0c1c-a401-400f-97fe-da8ba7f9d652",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "55643565-f787-4e26-915a-cb9e509b8a72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "c2d152c6-0b29-48ce-852f-bfc7fb549f00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "b90704c3-0592-4c7c-a319-6979ebe88b56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "b1d20a86-3161-46f9-9bb9-ca8786f6cc6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "e481f067-4b80-4023-bb70-799b879a14e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "70313751-2426-42e1-b56c-75d06f3c81a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "c09ca903-6c96-4b95-9246-1a435e93376e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "cefd3672-e983-4b3d-95d3-311af08db0b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "a34168a3-ae82-4d1c-9d2a-9d275523d6c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "b627f0d8-e183-4fa4-8692-0b2cad56a346",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "4aa782b3-4228-4083-93d7-c5eca46a0b89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "06acd751-8a2e-48a0-a158-8b2b79d4e764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "ff61a413-9f21-4f81-8657-522a92b54ace",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "0c7d3e96-4f1d-433a-881f-a19a1fa12232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "580e97b1-264f-4822-814f-0d4dcae5ba3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "652e7b4b-9104-479e-b533-66f19264f366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "081b8018-d448-42be-83e9-6bd0b353ed48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "69315e6c-5f80-4ea7-97d4-7b9420e51175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "2ed3b8f9-d3c1-4573-abd9-e7d420d65688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 58
        },
        {
            "id": "bb51bcf6-099d-40a1-aa89-0466a996aa05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 59
        },
        {
            "id": "f60c718a-effa-4394-a9cc-8c088bf51ca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "375ced10-6fc3-47a8-b9dd-d2ada882c1c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "5ea969fb-41e8-4907-ac7a-064390e4a5c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "b74232f4-5c11-4340-a6b7-689f776fcde1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "82a3fff8-3096-4d84-bed9-e00faace7cb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "e7ebb43a-336e-4380-ab07-79d14cbdd3d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "4770400d-ad6b-4873-981e-30bf9e095ac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "bb8f81a0-4ec4-43dd-9d06-5ebb7410af50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 115
        },
        {
            "id": "ff71b12b-4a56-412e-b6a9-91067ea3b60c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "ba085cbd-0553-43a9-86ac-11535fd0691c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "f83ea814-a025-4365-8813-7fcae0cef1bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "706b8340-64e9-4867-b504-97d8ddd7a13b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "23ad46a0-aebf-4e2c-93ea-69473cd4560b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 894
        },
        {
            "id": "8ff066ff-3c6a-4a50-9127-04b04f936ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "c474dbfa-19ea-439b-a9e5-5d3820385285",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "54cec1fa-94eb-4a92-b4ba-47531c00a68c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "d44e46aa-6143-47ff-9ffc-63049aa16560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "b0e5e3c1-d7b1-48e8-8247-8e913fd05973",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "42edfd7d-9a89-411a-8019-e3a96be9bfe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "efed74fe-4ab0-4e4d-85fa-cda86d4cddc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "12477bf9-54a1-4916-80f5-b7adace6b9a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "bd841f03-2581-4112-86d9-d077e4c50517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "f5e65ed4-d212-481a-9497-f84086c6a690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "4a40c3c2-b82c-4b2c-b778-02762171dde3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "7e57330a-84eb-4468-a1db-47a352429285",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "e999f42b-37f0-4164-a420-1779e1917931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "777f8d43-a4b2-49a1-88f4-e6b4bfe72d3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "96f84aae-eb9d-4198-84ae-ed447f6b3dea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "3c783ff0-5d47-4b67-929a-d7e4370a9762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "7775fe0c-e085-4fa0-86fa-9905632ce1f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "81b73e42-b99d-4b59-b43f-b11a50884c05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "36fb54ec-09bc-40f9-bb8e-ad356f53e0f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "de62253f-a2a6-4592-bc81-4ea44402f50b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "2fb72bd1-04ff-4a97-b378-3fd390214c36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "6fa6afbc-9d7c-48f4-a234-d3b023af3cad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "e03c72d0-85d7-4deb-94eb-3904475a5b63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 59
        },
        {
            "id": "d4715f62-96af-4f1f-8878-d55c4d454fe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "292e9ab7-a327-4126-8eaa-2d914c16e238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "b5165ab7-5d61-4889-9799-a2dd69c2520a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "c6c21c46-26af-4e3a-b431-01b7380d6b45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "58fc4b62-e79c-4a97-9e1b-70d5d3fefaf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "dffe1a97-83d1-401e-bdf5-03d9348a5de4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "3886b6db-d637-4c7f-b14b-50beaed0f684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "6d3882fc-c9ed-4d28-8e68-10be45b67ee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "8c711b4a-a1aa-4eaa-83e9-405dfcedf35f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "0a6eda4e-1598-4617-9505-878e9225df43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "10566dbf-2658-4ee4-851d-94617bc2df5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 894
        },
        {
            "id": "b3638983-2ace-44e3-8032-ce65828c0fe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "8ff3fee2-d846-49e2-b417-e2ec4019ac39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "70c96f54-7681-4260-87d5-6ef329a1fb4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "6d75a5c3-14f7-4674-8816-ae8da5652159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "592fc96b-3549-46ad-a5bd-c541933a19f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "78d9dd15-94bc-4ede-9e62-bc4d6028527f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "ea4b77a0-a2e2-4d33-93a1-794997fc00f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "39d711a9-22a1-4bdc-994f-c6b98fa6ac41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "f39a6a39-5508-410a-967a-f9bdb767dec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "Hit the notes",
    "size": 20,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}