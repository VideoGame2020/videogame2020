{
    "id": "07fb5052-ab0b-497a-9710-59a321c2a9b4",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fHeader1",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0032de9c-ae4f-420d-b22c-a4151ebad919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 69,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6dd48202-9080-4687-9680-bc9a1077775b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 69,
                "offset": 5,
                "shift": 17,
                "w": 7,
                "x": 484,
                "y": 215
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a0c04f38-8748-4c7c-b9c1-1897d987c7ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 69,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 465,
                "y": 215
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "9cf71f61-b821-4a29-9b2e-9449d5eb4df2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 69,
                "offset": 0,
                "shift": 33,
                "w": 33,
                "x": 430,
                "y": 215
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "44e3ca65-0452-4696-89a2-a03da09a8f0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 399,
                "y": 215
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d3273188-2044-4c26-80b6-4c5aa77add00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 69,
                "offset": 3,
                "shift": 53,
                "w": 47,
                "x": 350,
                "y": 215
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "71b6bc2e-2a16-4ad5-acd1-03dfe8d9d682",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 69,
                "offset": 2,
                "shift": 40,
                "w": 37,
                "x": 311,
                "y": 215
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "067b76b4-d19b-4c2b-b442-7e9211410a88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 69,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 302,
                "y": 215
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7d113634-cd2a-42c0-ad9f-e7b2c747446a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 69,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 285,
                "y": 215
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ed02aefe-f439-4516-8d46-b9cd7bab475f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 69,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 268,
                "y": 215
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0d2efa73-04f6-40d3-ab54-60da943d8c27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 69,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 2,
                "y": 286
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "34f7c59e-d696-4743-8c39-fff5069d33d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 69,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 237,
                "y": 215
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3a855f7a-1eea-452f-967c-e9ac7957e40a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 69,
                "offset": 4,
                "shift": 17,
                "w": 8,
                "x": 196,
                "y": 215
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8542870d-5c70-458b-ae3f-ebab57658ddf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 69,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 176,
                "y": 215
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "001976a7-5951-438a-a7aa-921b8cc7b230",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 69,
                "offset": 5,
                "shift": 17,
                "w": 7,
                "x": 167,
                "y": 215
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "7c6a2213-de7b-4d8a-b32d-da598820e042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 69,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 148,
                "y": 215
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "5f16afee-a889-453b-9496-269764481c5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 117,
                "y": 215
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "64ea328c-8eec-421c-9fa1-0d114507fd6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 69,
                "offset": 6,
                "shift": 33,
                "w": 17,
                "x": 98,
                "y": 215
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "02ba7776-9799-487b-afe6-89e3f838f49d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 69,
                "offset": 1,
                "shift": 33,
                "w": 30,
                "x": 66,
                "y": 215
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a80fb2f9-dd00-4f79-a0f7-d5eb75669283",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 35,
                "y": 215
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "1ef90266-5488-4feb-9981-a73016d0fdb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 69,
                "offset": 0,
                "shift": 33,
                "w": 31,
                "x": 2,
                "y": 215
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ab489b5b-8c48-464e-89fe-4909e5d5526a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 206,
                "y": 215
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9e5a0536-037c-4df7-9880-b9fe4f69b3c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 25,
                "y": 286
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "178b7c16-ec9e-4cd5-8654-0164bf3f17f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 56,
                "y": 286
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "0d688e23-2de4-4bc7-ba77-57d967f7f662",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 87,
                "y": 286
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "290a37ff-e3da-4ac2-bb1a-5bb96b630ffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 282,
                "y": 357
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e4fbae98-4503-49d2-8d10-e80ace9f5b58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 69,
                "offset": 5,
                "shift": 17,
                "w": 7,
                "x": 273,
                "y": 357
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "9ae9d37b-a5aa-4d2e-8fce-9242f7ff5aae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 69,
                "offset": 4,
                "shift": 17,
                "w": 8,
                "x": 263,
                "y": 357
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2775481e-d529-49a8-82c6-e210af6744bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 69,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 232,
                "y": 357
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ca720573-d9ee-47c3-9042-342072eb8f56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 69,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 201,
                "y": 357
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7a7393cd-2686-4113-8d03-c6ec264490bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 69,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 170,
                "y": 357
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "fd859096-de1d-400f-a4b9-eddf94fde295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 139,
                "y": 357
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "7fc1bcf4-6e44-46c5-bfac-a3f2bd45f747",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 69,
                "offset": 3,
                "shift": 61,
                "w": 56,
                "x": 81,
                "y": 357
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "21c9206d-9617-409e-b5a4-8a2f2a0840f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 69,
                "offset": -1,
                "shift": 40,
                "w": 42,
                "x": 37,
                "y": 357
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "dca2ab2f-f138-41c7-9cd5-cf5068c2a0a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 69,
                "offset": 4,
                "shift": 40,
                "w": 33,
                "x": 2,
                "y": 357
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "401acba5-5247-4094-bc8c-ad7e47ddf7b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 69,
                "offset": 2,
                "shift": 43,
                "w": 39,
                "x": 451,
                "y": 286
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "880d2245-3adb-4fa8-919a-2afa8a73feb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 69,
                "offset": 4,
                "shift": 43,
                "w": 37,
                "x": 412,
                "y": 286
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c3078955-f4df-4442-8a92-60b58c970b16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 69,
                "offset": 4,
                "shift": 40,
                "w": 33,
                "x": 377,
                "y": 286
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "41b26e96-d863-409e-8b7d-6ffdfb52b88b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 69,
                "offset": 4,
                "shift": 37,
                "w": 30,
                "x": 345,
                "y": 286
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e04ab37a-f293-4259-8b28-15dc6747a68e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 69,
                "offset": 3,
                "shift": 47,
                "w": 40,
                "x": 303,
                "y": 286
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f182ca68-0126-4a36-9ae2-20e718c05c15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 69,
                "offset": 4,
                "shift": 43,
                "w": 35,
                "x": 266,
                "y": 286
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "fd08840d-1511-4da0-b3e1-29f1f7beeba6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 69,
                "offset": 5,
                "shift": 17,
                "w": 7,
                "x": 257,
                "y": 286
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a5c9165a-1ea0-4ee9-ba6e-311378fc8361",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 69,
                "offset": 1,
                "shift": 30,
                "w": 25,
                "x": 230,
                "y": 286
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5c3d5a33-2432-41d7-8e61-5381ac6a3458",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 69,
                "offset": 4,
                "shift": 40,
                "w": 36,
                "x": 192,
                "y": 286
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "6ab821ac-c28d-43c7-9573-2d4ce3a4ba85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 69,
                "offset": 4,
                "shift": 33,
                "w": 28,
                "x": 162,
                "y": 286
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c083a13b-3011-4319-b204-eafa855e9cd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 69,
                "offset": 4,
                "shift": 50,
                "w": 42,
                "x": 118,
                "y": 286
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a957c611-3b4b-4535-aa28-3797545343b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 69,
                "offset": 4,
                "shift": 43,
                "w": 35,
                "x": 459,
                "y": 144
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f8a55619-1aea-46b7-8285-dee61a50bd00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 69,
                "offset": 2,
                "shift": 47,
                "w": 42,
                "x": 415,
                "y": 144
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "773dfb8c-3c76-4c74-8f5a-fcac2b43f92f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 69,
                "offset": 4,
                "shift": 40,
                "w": 34,
                "x": 379,
                "y": 144
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "09960438-e100-4a09-a6ef-8dd9c99dccd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 69,
                "offset": 2,
                "shift": 47,
                "w": 43,
                "x": 210,
                "y": 73
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9f193e55-d7f4-4d64-b0f7-01ee30c547b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 69,
                "offset": 4,
                "shift": 43,
                "w": 39,
                "x": 155,
                "y": 73
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c35cf7a5-a539-4f6c-9cf8-7eeba946b3b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 69,
                "offset": 2,
                "shift": 40,
                "w": 35,
                "x": 118,
                "y": 73
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "85ff5d47-a841-4c53-aaf5-fe29d82a159d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 69,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 81,
                "y": 73
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "36716414-855d-47f3-ad58-37c8a66a29a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 69,
                "offset": 4,
                "shift": 43,
                "w": 35,
                "x": 44,
                "y": 73
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e35acdb7-f18c-4a56-957e-f6756562faa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 69,
                "offset": 0,
                "shift": 40,
                "w": 40,
                "x": 2,
                "y": 73
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ea4ee76d-d9df-457c-8006-ce6f108ddcb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 69,
                "offset": 0,
                "shift": 57,
                "w": 56,
                "x": 428,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4b11b1fb-bb11-4468-8579-15abdf02e2fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 69,
                "offset": 0,
                "shift": 40,
                "w": 40,
                "x": 386,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ae7b55c5-5221-49f5-be31-2f7905bcea66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 69,
                "offset": 0,
                "shift": 40,
                "w": 40,
                "x": 344,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b984ef05-d9ba-47e0-989c-c118bdd0106b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 69,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 307,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9925c012-e527-4e00-8e84-f398cfcda9ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 69,
                "offset": 4,
                "shift": 17,
                "w": 12,
                "x": 196,
                "y": 73
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "0a695d7d-df99-40d0-a229-b6bb2ed343a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 69,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 288,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "bb94ad6e-f3db-4b5c-8911-6b6c629bc071",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 69,
                "offset": 1,
                "shift": 17,
                "w": 12,
                "x": 253,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "92b272e7-3b16-4a66-8be1-140973adfe03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 69,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "6999157c-a4f7-4ae0-a022-e715ca4268d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 69,
                "offset": -1,
                "shift": 33,
                "w": 36,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "07b37070-e6a1-47fa-9980-cbff1c42336a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 69,
                "offset": 2,
                "shift": 20,
                "w": 12,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1b6746e0-9786-44f5-b22c-14e1d581ac63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1f77636f-dc90-4792-8683-6407b9f3fa1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 69,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c92fdeea-d5cc-47e9-a29f-fa86bd4f8062",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 69,
                "offset": 2,
                "shift": 30,
                "w": 28,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e4c3611c-517c-41a8-a4a2-4c8e681da253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 28,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "adbaf18d-8103-46e8-9e97-fb74d2a5699a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d456016a-1287-4dcf-9f5d-563c0fc6c6d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 69,
                "offset": 0,
                "shift": 17,
                "w": 19,
                "x": 267,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "57b12136-9b2d-4d48-9040-e95b7c4fee50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 69,
                "offset": 1,
                "shift": 33,
                "w": 29,
                "x": 255,
                "y": 73
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "95e2020f-dda3-4a73-aebd-3d8d8566af93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 69,
                "offset": 3,
                "shift": 33,
                "w": 27,
                "x": 63,
                "y": 144
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e832a17d-1c0b-421c-add5-f7060971a6e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 69,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 286,
                "y": 73
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7a7cb50b-2849-420e-9189-17069f0bb576",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 69,
                "offset": -3,
                "shift": 13,
                "w": 13,
                "x": 335,
                "y": 144
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c9d11a45-a09b-4f2c-a10d-b91c7cb82b42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 69,
                "offset": 3,
                "shift": 30,
                "w": 27,
                "x": 306,
                "y": 144
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "8b455e61-68da-469b-a7ab-b1e797dc409f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 69,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 297,
                "y": 144
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f861bfaa-4393-4d85-948a-e7e121bd3b42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 69,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 251,
                "y": 144
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "247b25f2-76bd-4125-8c7c-4e748d3e771b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 69,
                "offset": 3,
                "shift": 33,
                "w": 27,
                "x": 222,
                "y": 144
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "daf0356d-a2a1-4482-b683-dff1e818291a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 190,
                "y": 144
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c7de1dbe-59f0-41ec-8ed1-0f010ab4fa19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 69,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 160,
                "y": 144
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "44ef8b7d-f242-4782-9b78-301980bdc3d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 69,
                "offset": 2,
                "shift": 33,
                "w": 28,
                "x": 130,
                "y": 144
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "008a418c-5069-4926-9075-54d681090961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 69,
                "offset": 3,
                "shift": 20,
                "w": 18,
                "x": 110,
                "y": 144
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d343efec-036f-492a-a565-0e6a4842b2bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 69,
                "offset": 1,
                "shift": 30,
                "w": 27,
                "x": 350,
                "y": 144
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "828b007a-6897-452a-a675-73b129173226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 69,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 92,
                "y": 144
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "862c2c6a-f9f0-4a6d-bfc4-c6019ca1479b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 69,
                "offset": 3,
                "shift": 33,
                "w": 27,
                "x": 34,
                "y": 144
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3b5bfec0-e9fc-49b8-a24b-ccb68fc4146d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 69,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 2,
                "y": 144
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1c270e83-cd6e-4649-8e59-79c1968bc6e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 69,
                "offset": 0,
                "shift": 43,
                "w": 43,
                "x": 437,
                "y": 73
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "57b5a3e1-bad2-4d01-abf7-1c183cb0ba0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 69,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 405,
                "y": 73
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4d7106ed-e9d3-4bf0-98f2-3b8a36236c5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 69,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 373,
                "y": 73
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8721bea5-4030-4e25-ae62-966293e1bc41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 69,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 343,
                "y": 73
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e5b6e4bd-a6cc-4915-be97-019e8923cf9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 69,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 323,
                "y": 73
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "203b7ee5-30f9-489c-8995-85e1a35c4fab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 69,
                "offset": 5,
                "shift": 16,
                "w": 6,
                "x": 315,
                "y": 73
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "89f7b1df-b568-4675-b329-ed71d64f7a58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 69,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 295,
                "y": 73
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "3a9fed42-a52e-465d-a199-6f8c0a7ddbfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 69,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 313,
                "y": 357
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "82dcf601-4858-4619-b35a-abb62bba9509",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 69,
                "offset": 9,
                "shift": 36,
                "w": 18,
                "x": 346,
                "y": 357
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "b62ba190-53b8-4e70-8add-d6f16fdca8d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 65
        },
        {
            "id": "6de02fd2-73c1-45f7-8222-c2435dd82ee8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "11e01d46-257c-4334-9236-67d512c7be71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "22b936e9-97d0-4a2e-99a7-30fd90eaba10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 902
        },
        {
            "id": "abab9375-5e9d-472c-8d7d-2e5b0df3dc2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 913
        },
        {
            "id": "784b0349-c74e-4e13-bc86-98f61c75ddd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 916
        },
        {
            "id": "a90e0abb-844d-4a65-bf10-a85212af7e0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 923
        },
        {
            "id": "cb6740fb-aabc-4e43-a3e3-e863dab5b926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "9b99a094-fbc5-4bed-863d-4fe1f9173e70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "b671c101-fa7c-451a-9e3c-e5cd96855475",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "7541e2f5-076a-485b-bed1-f3ed46bdc41e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 49
        },
        {
            "id": "0b16be3d-5974-4c2b-b709-09f0323dbd92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 32
        },
        {
            "id": "56c87aa3-fd3e-4746-970e-200e4c338e97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 84
        },
        {
            "id": "2dafe29b-60c9-4d2c-bf58-85ed6ab07b4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 86
        },
        {
            "id": "c10e2dac-6474-4502-b03d-cf575e3fae68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "a2322f2b-8d5b-400b-a00e-17091565c0d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 89
        },
        {
            "id": "79266a0d-02e7-4805-95cb-093455b009c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "1d291b7c-eb29-41a6-a719-91f9ce451163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "a10a4b32-6503-49cb-b705-06a21669bb9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "f4ec3cea-61d1-4cde-8696-856383fc6dca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 160
        },
        {
            "id": "d8aece7b-6ea8-42e6-9166-7286c3ca23e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 8217
        },
        {
            "id": "a49362f3-00dd-454f-a2c5-2f5dc1128d91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 44
        },
        {
            "id": "94f881c9-b36c-4b30-9b4c-d002b62b0016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 46
        },
        {
            "id": "2fbbe4a1-1c03-4551-b104-9a4c904c6cf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 65
        },
        {
            "id": "65e0ff98-0696-423a-9b57-aaf40e6f42ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 32
        },
        {
            "id": "f76e63a6-0488-4109-b2a7-2423991fbd85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 84
        },
        {
            "id": "6743e71f-96b2-4a03-b082-93583b724e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 86
        },
        {
            "id": "598e591b-2c3c-49c8-9199-fa3df18800b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 87
        },
        {
            "id": "b839fc70-f4e6-4c01-9d9a-bb1ea8eb3c22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 89
        },
        {
            "id": "3b58ef67-e0bc-4b39-8565-be8a30a8650c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "f7d0bf1e-3c9a-4d87-8289-93c7adec6661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 160
        },
        {
            "id": "062ababc-451c-4f8c-9a71-a7ff491b5153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "8efcc111-7654-4e93-9ab7-48a445202f03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "b1f450f0-8349-4604-bdc3-a0f3b4693af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 44
        },
        {
            "id": "8a05f3ec-b803-4e05-b189-db782890d29c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 46
        },
        {
            "id": "26caa7b0-7f3d-4e17-a586-1be6b249d018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 65
        },
        {
            "id": "a139815d-4066-417d-9bda-5e9caae07d1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "7d450437-805b-417c-8eae-b7bd81c71c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "fed4fd05-2522-4281-a8d4-83ab071952ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "b602aa79-901f-47d1-a400-842178c378fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "3f4b2c83-e1bc-4e68-9bd1-c52ad8a085f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "7b16df91-598b-4029-9b56-82db4aa9ad53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "f809cd88-e919-4761-959a-b68380ae3176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 44
        },
        {
            "id": "a75190f2-c7d8-4db0-96ce-56f4bc96c3cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "3da9f82f-a0ac-475a-b6f9-eecb12723d8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 46
        },
        {
            "id": "a39a96a5-be36-4d57-a7fc-411fdc983e0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 58
        },
        {
            "id": "68080e09-df67-4fca-a062-da9a2d4b8af3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 59
        },
        {
            "id": "2aefd557-d17a-4a36-ab8d-3f34f02fc446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 65
        },
        {
            "id": "a6d274f1-2101-42cb-9de6-a52428ded842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "92d93c88-f07d-4690-b859-7303c191c0fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 97
        },
        {
            "id": "845b4d1a-933c-4232-9a0a-b32e157c5865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 99
        },
        {
            "id": "24a693b2-0708-4978-91aa-55030e73442b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 101
        },
        {
            "id": "594af425-cece-4061-8f42-7922f712b033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 105
        },
        {
            "id": "ebbd56a8-10f5-4bf5-a553-1d15a7427270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 111
        },
        {
            "id": "a57eeda0-f991-4fac-bca6-018e2fcbf2fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "a31c0973-9fe9-4d24-b874-4ee8b85e8c20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 115
        },
        {
            "id": "00616d10-6dc5-4fcb-bc92-c4c54dc2df01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "03e6d935-c1a2-4d54-be26-07f64fdd91a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 119
        },
        {
            "id": "8afd45ef-f2f9-4d5a-87a9-8b91c33cfd16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 121
        },
        {
            "id": "8741e1f0-068d-4f7c-90f9-099145cccb54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "8112a51c-2c3b-4f05-a5e2-b74b6acec508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 173
        },
        {
            "id": "e7b34c3c-cad5-43c8-9c86-632129036b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 894
        },
        {
            "id": "5ee8ba61-6e81-4570-bc0e-b94ee2f9328e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 44
        },
        {
            "id": "098d3e44-9f76-4797-a421-6fb22b5ab40e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 45
        },
        {
            "id": "c69edeaa-796a-42e3-814f-3bc77952f7d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 46
        },
        {
            "id": "6edb7097-1860-4763-9522-cbf254886225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "4ab13687-1047-41f4-8d60-780d5385c612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "28d05f09-48eb-44d0-9fea-377cd73e60b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 65
        },
        {
            "id": "1bb7b906-abd1-47df-8801-394c81c9a310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 97
        },
        {
            "id": "9d41afb9-4fe7-4e18-8725-cec3afe2f237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 101
        },
        {
            "id": "55335487-f9d0-4dce-9954-d318dcba3638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "aaa6fa65-3c01-4b43-bb08-43b50c4cb100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 111
        },
        {
            "id": "8ddec350-23a0-46fb-8ddc-fe7eea148885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 114
        },
        {
            "id": "1d1e0506-dcd4-4c90-bd51-694566496be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "b39dec9f-f6b6-46b0-bbb1-c0a5863e977e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 121
        },
        {
            "id": "0998a971-1c1c-48a3-b5c7-b49a38923491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 173
        },
        {
            "id": "26e76e23-3ec0-4161-9516-d2aaf2e58b9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 894
        },
        {
            "id": "82608128-72de-45ff-9465-0037da86f3e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "6d0edadd-abc7-4141-ad83-89ce26d8c8d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "82b71f69-ee80-4f49-bda0-43d0b7e12aee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 46
        },
        {
            "id": "6f2e8706-7040-4ab0-8ccf-523a924fcc13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "93fd8217-0d9f-4c65-94b4-6bbeac7cc62a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "ee34c560-a13f-4361-9c05-6c497046fec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "71c0a743-51d4-4353-a8dc-5eaa2d82dc64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "6b22b4db-6259-4c86-aeef-a37bc83b5ff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "9ebaa3c6-89cc-4df2-bb9d-5062d21c716a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "a7245e31-9ffd-4bf5-a50b-41308b0e39e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "f1b400a7-c444-4890-92bd-60e67d5a707e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "beb12143-3e98-437f-9dee-1615f6885ed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "39ce3c83-8812-49a8-9bd1-9b521d4eb6fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "0451f41f-8233-4dd6-b103-21ca3f5d0714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "3d0d7626-2900-4f2a-9274-f5ecdc2ab415",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "757449da-df99-4cd7-a622-d315b20f611e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 44
        },
        {
            "id": "a8cae5c1-7411-44cb-9982-a5efa8c9935a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 45
        },
        {
            "id": "ba4bc809-6520-4b68-a676-482ac2b81263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 46
        },
        {
            "id": "56894add-391f-40de-99df-7ee170c2acc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 58
        },
        {
            "id": "daf22059-c531-495f-8148-47f6d6cadb70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 59
        },
        {
            "id": "909a18ba-75d6-4feb-94c3-899796dba868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 65
        },
        {
            "id": "53f0e0e5-e729-45a3-9b4a-43c2790e702a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 97
        },
        {
            "id": "9733e2a7-8af4-4f53-a7b6-f627a9c84e5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 101
        },
        {
            "id": "5a7c4d34-507d-4f7d-a4a8-77fc0bb71d26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 105
        },
        {
            "id": "3b13c859-97f0-4e44-894f-8faccc53bd7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 111
        },
        {
            "id": "2002b935-2c4a-4fff-8807-8dd6645d0ef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 112
        },
        {
            "id": "6b37d286-44f8-4d42-9394-65a2964fa6dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 113
        },
        {
            "id": "465f1ff0-e518-40e4-b137-0cd261720594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 117
        },
        {
            "id": "8f68bcd4-88c8-4765-bd7d-ae8f9ee3c0bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 118
        },
        {
            "id": "ec429f42-21fb-45a7-b9e3-f621e5d17a89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "3c627dbd-0a29-4dc7-b3ef-3288d11b93be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 173
        },
        {
            "id": "4d18fd7b-b53f-454d-9c51-1a7be7ad3b49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 894
        },
        {
            "id": "69e56679-894a-449f-8168-8562a8b5d6c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "538bac9b-4d5f-4ab1-9897-e02ef3d69874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "6491de6f-93b2-4c4d-832d-4fa42573975a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "27ba1c58-baea-4d78-9bfe-c28152876c35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "3138bf3e-f6da-4eb3-9cea-13f5bb9d184d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "fbb4f017-dd15-4209-af2a-0f089204597a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 44
        },
        {
            "id": "03765cae-e6fc-4007-887b-13aa04a1c37d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 46
        },
        {
            "id": "e5a4c241-c104-4151-9078-d60501150dbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 44
        },
        {
            "id": "dd216eea-9cb2-4135-a90a-bd1be7cacb46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 46
        },
        {
            "id": "35d31a31-728f-4580-b62c-d602e8384c2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 44
        },
        {
            "id": "5ce59b99-6634-4761-af93-944bf57848d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "Basics",
    "size": 45,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}