{
    "id": "bc0fa0d5-9c77-465f-b0ba-1e5a8f10a8c1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpeed1",
    "eventList": [
        {
            "id": "953bc22b-6187-4d5b-ad76-c1a486c5ebf7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bc0fa0d5-9c77-465f-b0ba-1e5a8f10a8c1"
        },
        {
            "id": "11750f76-21d8-45de-a76c-6cd3f69c721b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bc0fa0d5-9c77-465f-b0ba-1e5a8f10a8c1"
        },
        {
            "id": "22c98612-5ce5-44d4-ae4d-6f27b7342948",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "bc0fa0d5-9c77-465f-b0ba-1e5a8f10a8c1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cea494f2-2b94-4692-bc36-dcb494fce6b7",
    "visible": true
}