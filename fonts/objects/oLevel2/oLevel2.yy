{
    "id": "a0251122-8c35-4102-a53d-0574bc0e91e6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLevel2",
    "eventList": [
        {
            "id": "cec7f8ea-bfd6-4c3f-b3fe-b4eae7b70f70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a0251122-8c35-4102-a53d-0574bc0e91e6"
        },
        {
            "id": "ad6cb3f0-19ae-48a9-9d43-8ca4540e6882",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "a0251122-8c35-4102-a53d-0574bc0e91e6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f0144753-c4f0-49fb-9f15-f63375867f7c",
    "visible": true
}