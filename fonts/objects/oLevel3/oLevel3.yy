{
    "id": "4cfa118c-b363-435a-aa3a-7052b345502b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLevel3",
    "eventList": [
        {
            "id": "cf7f1578-5a54-4279-95e7-a92d37e54f9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4cfa118c-b363-435a-aa3a-7052b345502b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b27668cb-1af7-4338-aa32-e57ca6d9b98e",
    "visible": true
}