{
    "id": "e723bcbd-870d-40dc-8edc-dd386f66d0df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpeed3",
    "eventList": [
        {
            "id": "bdb42a3d-69ef-4dc6-b190-f0a4cdbe8f31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e723bcbd-870d-40dc-8edc-dd386f66d0df"
        },
        {
            "id": "07c13ef4-26e2-43c5-a55e-89b01202f029",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e723bcbd-870d-40dc-8edc-dd386f66d0df"
        },
        {
            "id": "68482c2f-f727-4b25-8858-dbfde21bc945",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "e723bcbd-870d-40dc-8edc-dd386f66d0df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ae22a2d7-7420-4e3d-b046-b78718bb840d",
    "visible": true
}