{
    "id": "92b4dfe8-e0af-46d2-8801-1d1ba3d176d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpeed2",
    "eventList": [
        {
            "id": "590704b1-c771-4ba1-bef4-243318bdd3ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "92b4dfe8-e0af-46d2-8801-1d1ba3d176d3"
        },
        {
            "id": "39097002-abf6-4f43-83a1-ef9660f75474",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "92b4dfe8-e0af-46d2-8801-1d1ba3d176d3"
        },
        {
            "id": "95283cd1-ec3a-4324-b5f4-dcac309f0222",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "92b4dfe8-e0af-46d2-8801-1d1ba3d176d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aae4d7e1-5663-48be-b353-9f8c837177cc",
    "visible": true
}