{
    "id": "cfc6987f-5053-4353-beb2-4f9a5abd99fe",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fScore",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "db2407c8-4b11-4740-b009-5dc7a8935535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 56,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ea9f1394-a0ea-4f9f-a830-e490ab0c8e7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 56,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 436,
                "y": 118
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "0d967eca-9b3b-4ea2-81c8-4ba976963047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 56,
                "offset": 2,
                "shift": 17,
                "w": 12,
                "x": 422,
                "y": 118
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "301117e2-1342-4127-96fe-51a2ec98b55e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 56,
                "offset": 0,
                "shift": 34,
                "w": 34,
                "x": 386,
                "y": 118
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "75085b89-1670-47d7-9fc7-9d0cf2fddbe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 56,
                "offset": 1,
                "shift": 28,
                "w": 23,
                "x": 361,
                "y": 118
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "6256d7fc-6e95-4e08-becc-1f1e9f4cc9a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 56,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 329,
                "y": 118
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ff980b4a-e000-4264-b0b0-8882a0dbb45e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 56,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 303,
                "y": 118
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a50156f4-2724-4496-826b-b2af816f671f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 56,
                "offset": 5,
                "shift": 16,
                "w": 5,
                "x": 296,
                "y": 118
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "bdd46d3d-8415-41bf-8cac-7eefa1d70082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 56,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 282,
                "y": 118
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ab8abca1-6818-4bcc-9cbb-63640028dac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 56,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 268,
                "y": 118
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d07eba6c-cde6-46e6-93b1-a7ce9e6b5f9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 56,
                "offset": 0,
                "shift": 21,
                "w": 19,
                "x": 443,
                "y": 118
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6f0a41bf-875a-4de5-9ce4-92bfd6295b60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 56,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 248,
                "y": 118
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2fdfded5-6176-4e81-b9be-b94d79c06491",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 56,
                "offset": 3,
                "shift": 11,
                "w": 7,
                "x": 216,
                "y": 118
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "efaa1f8c-cae1-4049-9288-0b19886aa8ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 56,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 201,
                "y": 118
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "97ccf9a7-d40b-4e0c-bc76-84acd1eab262",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 56,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 193,
                "y": 118
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "033dfad5-a383-4a40-9ed0-cca9c7c5b9f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 56,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 173,
                "y": 118
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ff17cb0f-b9bf-4f63-960c-2fb8750d17cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 148,
                "y": 118
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d0f93ac5-7cc1-494c-9f1a-ecc048271c59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 56,
                "offset": 3,
                "shift": 18,
                "w": 13,
                "x": 133,
                "y": 118
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "91de80cf-edc9-4db2-8bea-0288e7768d3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 56,
                "offset": 3,
                "shift": 24,
                "w": 19,
                "x": 112,
                "y": 118
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "28236a19-720c-4355-a51a-44a3bbd73c8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 90,
                "y": 118
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9ebeb422-2da2-4e90-809d-0debc7d73828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 56,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 64,
                "y": 118
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "cc6dc664-404b-4162-9afb-4ee5aa26e99e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 225,
                "y": 118
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e9fa6677-4f67-491c-96bb-37225ed8b415",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 464,
                "y": 118
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "15b88c02-fa7b-4dcd-8621-3289303dff46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 486,
                "y": 118
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d3c1ad08-cb8f-481c-990e-c81dfe8de6e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 2,
                "y": 176
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "31f1b425-38bc-445f-b8a9-8a3524c09e30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 481,
                "y": 176
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "482ec0a9-ccf1-4c92-b2d5-6829c59074cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 56,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 473,
                "y": 176
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7f49b687-9e56-4777-aa33-bf4d69421878",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 56,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 463,
                "y": 176
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f6195c8b-ce35-42e7-8582-b1745804b51f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 56,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 448,
                "y": 176
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7a5f2ccc-69a6-4a14-b83b-0dda4f3e0ccf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 56,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 429,
                "y": 176
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "32482cc4-6461-49b2-ac7d-091a6c69e988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 56,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 413,
                "y": 176
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b2e0eb0f-f654-4a2a-a850-bb522c8f74cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 393,
                "y": 176
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d7b7d5f9-df39-4473-8dd4-6bb41c85620a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 56,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 358,
                "y": 176
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "bffb8c6c-0030-4951-86ce-c0aa87f71c76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 56,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 331,
                "y": 176
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8d19dae8-4c66-4973-9261-6c4e28736761",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 56,
                "offset": 3,
                "shift": 25,
                "w": 21,
                "x": 308,
                "y": 176
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b3440e92-1041-4616-8d89-bffe6f2c0403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 283,
                "y": 176
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "2c79c997-b64f-49b4-9182-7c56359c2613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 56,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 257,
                "y": 176
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a9b847a6-913e-436e-9989-7053545d8dc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 56,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 233,
                "y": 176
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9fd88a5f-2d8b-4b82-b406-422d2bbec234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 56,
                "offset": 3,
                "shift": 24,
                "w": 21,
                "x": 210,
                "y": 176
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "8cab8ecb-1aaa-4a04-bf1b-0533d1f7f580",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 56,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 182,
                "y": 176
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e865e7fd-6800-441f-9a8a-9411e32f64a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 56,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 153,
                "y": 176
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1925fbd4-0edc-4fd6-9b28-84150d9deab4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 56,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 131,
                "y": 176
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "cc349a44-e9aa-44a4-9746-082d6a0d2d63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 56,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 104,
                "y": 176
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b1d36489-b805-468e-8ca8-280397d12fab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 56,
                "offset": 4,
                "shift": 24,
                "w": 21,
                "x": 81,
                "y": 176
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4d9cdb0a-a427-40ff-a9e7-2cb91033e7fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 56,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 58,
                "y": 176
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "bf5488bc-905d-4679-99de-2bdfa2dc80f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 56,
                "offset": 2,
                "shift": 35,
                "w": 32,
                "x": 24,
                "y": 176
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3c634d5a-1c1a-4742-9603-f841c371b4b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 56,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 33,
                "y": 118
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "545750e7-51ce-47c6-97fb-3c07f2a11fde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 56,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 2,
                "y": 118
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e3906834-a099-4f7d-87e7-8c1896ff6f9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 475,
                "y": 60
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "017a57d8-f1ce-4393-aa36-b532ec0439e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 56,
                "offset": 1,
                "shift": 35,
                "w": 34,
                "x": 2,
                "y": 60
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "31486ef3-e220-4678-b21a-77769315feb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 56,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 467,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e025f2a4-bab1-487e-9d0f-d54107b12659",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 56,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 441,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "2865dbd3-e91d-4509-a09d-608c4c98c26f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 56,
                "offset": 2,
                "shift": 27,
                "w": 27,
                "x": 412,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6320a274-1849-48f9-b40a-a0e07b5116c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 56,
                "offset": 3,
                "shift": 29,
                "w": 25,
                "x": 385,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ff7f0adb-a9dc-4746-8952-d427addfa2d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 56,
                "offset": 2,
                "shift": 26,
                "w": 24,
                "x": 359,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "780f2164-7b98-4490-9d8b-33212c14dc45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 56,
                "offset": 2,
                "shift": 42,
                "w": 39,
                "x": 318,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a9e7b4b9-a4b2-42e9-9986-83af6083ffa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 56,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 289,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e2d5feb9-c663-4427-a3db-d9619a60c801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 56,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 263,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "5d2ccef9-50a0-4e0f-a97e-c1802af67282",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 56,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "dbaa17e2-6d81-4cba-9812-7f2c21f0ad25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 56,
                "offset": 3,
                "shift": 15,
                "w": 11,
                "x": 491,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "87735561-ec8c-46a8-9a10-572bff22c93e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 56,
                "offset": 3,
                "shift": 22,
                "w": 17,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2c6fb0d2-c233-4961-87bf-03146c42cfb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 56,
                "offset": 3,
                "shift": 15,
                "w": 11,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7e60278f-4f8c-4020-940f-b9c2aafbacfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 56,
                "offset": 3,
                "shift": 23,
                "w": 17,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "283c04f8-a649-4127-8d9e-07c4187c614c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 56,
                "offset": -1,
                "shift": 25,
                "w": 27,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "bdf839cb-43c1-4037-a54e-3b9d249076ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 56,
                "offset": 2,
                "shift": 22,
                "w": 10,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f3e647ab-da11-4295-93b0-8c9ca28d4e22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 56,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a191b360-77e0-40bb-b32d-e731d25cb174",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d1d07081-3c85-419d-b0be-0145dcd68a22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "6252d6b9-e1a6-470e-91ff-140ac0d89037",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "762a4c50-9edf-4ca9-b6a9-321d116c88dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 56,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "84942fe0-83b4-43d1-9a70-1ea057dca7de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 56,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "8fa70d8f-19c8-4a54-a7a5-5a6d442b0f16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 38,
                "y": 60
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e0925804-7c74-4abf-8f80-ceb9d618a6ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 56,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 242,
                "y": 60
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "40097545-e1cc-47bb-9ad1-27dd5350e5de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 56,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 59,
                "y": 60
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "5b87619d-ca0d-4e86-a763-5b56617cc78e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 56,
                "offset": -1,
                "shift": 16,
                "w": 14,
                "x": 439,
                "y": 60
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "4d072f86-1f7f-4f60-a9e8-93ac521dc085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 56,
                "offset": 3,
                "shift": 22,
                "w": 19,
                "x": 418,
                "y": 60
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "4c80ae94-9871-4eb6-9bc4-4208ddb740e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 56,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 411,
                "y": 60
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f7e3e030-e742-419a-bf3d-160083c943a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 56,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 381,
                "y": 60
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c354f636-b1f7-4b5f-912d-24f294b0d77f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 361,
                "y": 60
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d6cb08a8-98e9-4735-9511-1fefff7f5a06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 341,
                "y": 60
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9dad022e-16d2-4790-931d-30a67283d059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 321,
                "y": 60
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c2ff3e85-fd75-4e14-b7ef-a66daaec2794",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 301,
                "y": 60
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8b3c98db-428a-44af-b82a-5f8a93fa6d27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 56,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 283,
                "y": 60
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "18b9cbd0-1b4c-44b1-9d7a-e6a11782523a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 56,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 455,
                "y": 60
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e17ba411-fedf-4043-b496-2def1cdaef35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 56,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 264,
                "y": 60
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "18a971d3-ba84-451f-8a12-db89994d7e8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 222,
                "y": 60
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0489a435-39d7-4f1d-a7cd-ae560210ba2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 56,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 202,
                "y": 60
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d573f6cc-f37e-42b0-a8a1-93560e1798d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 56,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 174,
                "y": 60
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2bed18b1-3c29-405b-b483-7cb364e1db1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 150,
                "y": 60
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d01418b8-e07f-4a3c-a8f1-49b8bcdf92fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 56,
                "offset": -1,
                "shift": 21,
                "w": 21,
                "x": 127,
                "y": 60
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ffaeb984-9820-4547-a138-a32a84a9dae5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 56,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 106,
                "y": 60
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "7b301611-5e85-44b9-bce9-d0d3eb474a0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 56,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 90,
                "y": 60
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "38806235-82fa-4212-a598-0271da6a4d0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 56,
                "offset": 6,
                "shift": 17,
                "w": 5,
                "x": 83,
                "y": 60
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b5c34c62-40d1-4385-a7e2-695c362f63cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 56,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 67,
                "y": 60
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "80ccf167-e23e-46b8-8ed2-f0874e0af923",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 2,
                "y": 234
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "dcdc4838-5513-403e-a361-a373c3011bdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 56,
                "offset": 6,
                "shift": 24,
                "w": 12,
                "x": 26,
                "y": 234
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "20",
    "size": 30,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}