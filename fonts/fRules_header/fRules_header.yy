{
    "id": "0acaf05b-80e0-4070-be53-d9493c5df71e",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fRules_header",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e7bdcbdf-5c6f-499d-b69a-8bd82a7cb3b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 77,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "c0c2101d-b6a1-44a1-8f94-e06b84b07f24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 77,
                "offset": 5,
                "shift": 19,
                "w": 9,
                "x": 151,
                "y": 160
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f6d8e759-c7d9-40fe-a5f3-94850f0c863b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 77,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 131,
                "y": 160
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e92be1b0-5cbb-4bb1-94ec-5a01f36af5f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 77,
                "offset": 0,
                "shift": 37,
                "w": 37,
                "x": 92,
                "y": 160
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "021b8670-0c79-403e-9df0-8f12e8541fd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 57,
                "y": 160
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d8b0ecf5-eef8-4438-b6c6-a0497a035f2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 77,
                "offset": 3,
                "shift": 60,
                "w": 53,
                "x": 2,
                "y": 160
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6c227012-9f86-434d-bc4b-f75c74655703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 77,
                "offset": 2,
                "shift": 45,
                "w": 42,
                "x": 929,
                "y": 81
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b334e415-e3ce-47d4-b94f-e7dae46c698c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 77,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 919,
                "y": 81
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "99dfb4c8-0f53-49d7-935f-e8cc521ce311",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 77,
                "offset": 4,
                "shift": 22,
                "w": 16,
                "x": 901,
                "y": 81
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "19dab18e-6d89-4f5f-9162-17d227f23268",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 77,
                "offset": 4,
                "shift": 22,
                "w": 16,
                "x": 883,
                "y": 81
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "240e8411-92bc-4ef3-8174-9c8a2869165a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 77,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 162,
                "y": 160
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "948ddc04-f53a-4722-9059-ae7e79da2557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 77,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 848,
                "y": 81
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e6df66be-eeb4-4ed9-a140-ff04eefc6a03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 77,
                "offset": 5,
                "shift": 19,
                "w": 8,
                "x": 803,
                "y": 81
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0f0167c3-f48c-433c-8297-64dc0c2d73aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 77,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 782,
                "y": 81
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5eea87fc-7757-4475-8cdf-c3b34ded48b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 77,
                "offset": 6,
                "shift": 19,
                "w": 7,
                "x": 773,
                "y": 81
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "7af7aae5-f11a-464f-b22a-6270490f5b5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 77,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 752,
                "y": 81
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "567fc97f-7bf7-47b2-a3dd-cf3a2a9ce15f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 717,
                "y": 81
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a4f1fb12-b73b-4c39-b33d-3b6a8fe6fa56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 77,
                "offset": 7,
                "shift": 37,
                "w": 18,
                "x": 697,
                "y": 81
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "00a4e2c3-a2de-41f7-bc62-a9b07a8b3492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 77,
                "offset": 1,
                "shift": 37,
                "w": 33,
                "x": 662,
                "y": 81
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3859f14e-9fa7-4273-a0b1-edf0feaac3cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 627,
                "y": 81
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ff095ea7-3349-4788-939a-f1fa758124f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 77,
                "offset": 0,
                "shift": 37,
                "w": 35,
                "x": 590,
                "y": 81
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "d03903ca-71ca-4b3c-bb94-28963f8fb837",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 813,
                "y": 81
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "2f620686-6aca-4134-8ce5-04b1416f6aaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 186,
                "y": 160
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8835f8e3-736e-4def-afdd-84f9660ade54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 77,
                "offset": 3,
                "shift": 37,
                "w": 32,
                "x": 221,
                "y": 160
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "14aab02f-c91e-4f07-9374-de8062e76123",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 255,
                "y": 160
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5e4001dd-0821-457d-bc59-8321e4144423",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 2,
                "y": 239
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7c8957d6-cd4e-4980-b4b5-f6813d575497",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 77,
                "offset": 6,
                "shift": 19,
                "w": 7,
                "x": 1004,
                "y": 160
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ac92ef05-488a-4c13-b012-0eaf9b663370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 77,
                "offset": 5,
                "shift": 19,
                "w": 8,
                "x": 994,
                "y": 160
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b9ed4f19-0008-4a88-a905-5f716eea7d03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 77,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 959,
                "y": 160
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "8159b1ea-7978-442d-a6e3-124868185499",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 77,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 924,
                "y": 160
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d489551a-7998-4ed2-a371-4452490697e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 77,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 889,
                "y": 160
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "726ff6e0-3188-4f63-be68-c8f56fac791d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 855,
                "y": 160
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "56465534-ec06-4a9a-b3e5-decc007f5a1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 77,
                "offset": 3,
                "shift": 68,
                "w": 63,
                "x": 790,
                "y": 160
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "52906d5e-6685-40bf-bba7-746c2da34037",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 77,
                "offset": -1,
                "shift": 45,
                "w": 46,
                "x": 742,
                "y": 160
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f52bfa88-7d27-4176-8291-1f8cef0e6365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 77,
                "offset": 4,
                "shift": 45,
                "w": 38,
                "x": 702,
                "y": 160
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "55dc08b0-e0be-47ac-93ad-2abf1656b89b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 77,
                "offset": 3,
                "shift": 48,
                "w": 43,
                "x": 657,
                "y": 160
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e0d03064-92cc-4a4e-a650-266812d52727",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 77,
                "offset": 5,
                "shift": 48,
                "w": 40,
                "x": 615,
                "y": 160
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "079924af-7d39-4d88-bffc-520da62c789c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 77,
                "offset": 5,
                "shift": 45,
                "w": 37,
                "x": 576,
                "y": 160
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "eb4e8eef-b1a1-43d0-bef8-7e47445d8368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 77,
                "offset": 5,
                "shift": 41,
                "w": 33,
                "x": 541,
                "y": 160
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0fd29bb0-f78c-47c4-97d2-0297b93fdd85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 77,
                "offset": 3,
                "shift": 52,
                "w": 45,
                "x": 494,
                "y": 160
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "98d1f2f4-0332-43b7-a91c-13e50e542f5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 77,
                "offset": 5,
                "shift": 48,
                "w": 38,
                "x": 454,
                "y": 160
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1adb6ac4-9299-4b19-b961-2c41b5d57b3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 77,
                "offset": 6,
                "shift": 19,
                "w": 7,
                "x": 445,
                "y": 160
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5586feac-8a78-4170-9758-a2d29a1ea4a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 77,
                "offset": 1,
                "shift": 34,
                "w": 28,
                "x": 415,
                "y": 160
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "598886ec-bbe4-4b7d-be1e-a0be7ff7311a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 77,
                "offset": 4,
                "shift": 45,
                "w": 41,
                "x": 372,
                "y": 160
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9dde5f11-a543-411f-91bc-598b1a1f4703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 77,
                "offset": 4,
                "shift": 37,
                "w": 31,
                "x": 339,
                "y": 160
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "804e07e2-86ec-4ce6-a466-72c7cf4a7905",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 77,
                "offset": 4,
                "shift": 56,
                "w": 47,
                "x": 290,
                "y": 160
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "bc633acd-f0be-4747-9933-89c4d96a3603",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 77,
                "offset": 5,
                "shift": 48,
                "w": 38,
                "x": 550,
                "y": 81
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0b9c9319-f6b1-45ab-8e2d-02759c68a612",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 77,
                "offset": 3,
                "shift": 52,
                "w": 47,
                "x": 501,
                "y": 81
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "903dce64-bedd-4083-b366-6b9e88dc9ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 77,
                "offset": 5,
                "shift": 45,
                "w": 37,
                "x": 462,
                "y": 81
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2c350a16-cb35-4da8-93df-792e93bf4bb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 77,
                "offset": 2,
                "shift": 52,
                "w": 48,
                "x": 772,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "92176745-4cff-41d5-b7a7-5b2435eee64a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 77,
                "offset": 5,
                "shift": 48,
                "w": 43,
                "x": 711,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "a214e1cd-40e8-4b55-b441-2995e812e67b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 77,
                "offset": 3,
                "shift": 45,
                "w": 39,
                "x": 670,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d638fa2d-3416-4d73-adb6-f87597b2aea9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 77,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 629,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a5e42b6f-302a-418e-b310-6ccaa23c8e12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 77,
                "offset": 5,
                "shift": 48,
                "w": 38,
                "x": 589,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "9afa1336-b596-4b13-8ddb-4a6e4c6a5efe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 77,
                "offset": 0,
                "shift": 45,
                "w": 45,
                "x": 542,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "719d7292-9379-4c15-ad33-9ed68fe6ea14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 77,
                "offset": 0,
                "shift": 63,
                "w": 63,
                "x": 477,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "6df36a19-3e19-4c88-9382-21c2ee1515a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 77,
                "offset": 0,
                "shift": 45,
                "w": 45,
                "x": 430,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "2af6ea4d-c9fd-4c59-84ba-ede24f2dd1e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 77,
                "offset": 0,
                "shift": 45,
                "w": 45,
                "x": 383,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6bb13fda-dcf7-4660-b33d-11ac1d37f0da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 77,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "bac016ca-9218-4e08-aa56-c70f741c28a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 77,
                "offset": 4,
                "shift": 19,
                "w": 14,
                "x": 756,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "725eb709-9419-4043-8991-9722f4104fb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 77,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 321,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f9e0cca5-c964-490c-b006-dd8787c11e66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 77,
                "offset": 1,
                "shift": 19,
                "w": 14,
                "x": 282,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "3cfc79fa-50d6-4048-abe9-2d7316fd70cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 77,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 251,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "067fba43-37f3-4e86-a80e-7c3858693364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 77,
                "offset": -2,
                "shift": 37,
                "w": 41,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2d5b7a3b-148f-47d3-9956-8f25d733ace9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 77,
                "offset": 2,
                "shift": 22,
                "w": 14,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "0b60ad36-6d9a-4c48-9476-22b1f8c953bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a1150184-d4ce-4bf8-a156-ca9f6ff50e6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 77,
                "offset": 4,
                "shift": 37,
                "w": 31,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d9ff21cd-b6ee-4842-94c7-4460ac717c0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 77,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "508b97e8-3183-4924-ac8e-7a8cc548ae21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 31,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "b165cfbe-510c-4a6f-b4e9-5f0173fb8034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "59c7a3af-7e4e-46f7-9545-21c3e783f48f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 77,
                "offset": 0,
                "shift": 19,
                "w": 21,
                "x": 298,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6682244e-c260-473b-92a6-7620e961674b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 31,
                "x": 822,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3b928fd0-2558-496b-b772-a9f58df766bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 77,
                "offset": 4,
                "shift": 37,
                "w": 29,
                "x": 118,
                "y": 81
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b8322082-e155-4430-8421-252b9b7440f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 77,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 855,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "2e57dafb-c5f2-40eb-8634-67539074462b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 77,
                "offset": -4,
                "shift": 15,
                "w": 15,
                "x": 414,
                "y": 81
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "87fdc883-e676-423b-a396-7bfa85b0fec5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 77,
                "offset": 4,
                "shift": 34,
                "w": 30,
                "x": 382,
                "y": 81
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7712f1a9-7344-4cd4-bac2-1ed52f6a6f61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 77,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 373,
                "y": 81
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "2d83754c-c99c-4991-ac7d-85a2f017a3c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 77,
                "offset": 4,
                "shift": 56,
                "w": 48,
                "x": 323,
                "y": 81
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a5c92805-8912-40ce-ae4f-4421cdf0fe75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 77,
                "offset": 4,
                "shift": 37,
                "w": 29,
                "x": 292,
                "y": 81
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e0398ad9-4aef-40d0-8a00-8c0d68ae8128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 257,
                "y": 81
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c29383c3-f607-4bfe-9942-6758f6ecbada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 77,
                "offset": 4,
                "shift": 37,
                "w": 31,
                "x": 224,
                "y": 81
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e3604d5a-bf90-45f3-8548-64256771083f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 31,
                "x": 191,
                "y": 81
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "fe6f586a-8f28-4fdd-b8cb-8ae6ee694bb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 77,
                "offset": 4,
                "shift": 22,
                "w": 20,
                "x": 169,
                "y": 81
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9b49456b-4b17-4949-8348-a06d6b777d81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 77,
                "offset": 2,
                "shift": 34,
                "w": 29,
                "x": 431,
                "y": 81
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "55e0e8f1-9d2d-41e3-a27e-86c1e701770f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 77,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 149,
                "y": 81
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "76bdce1e-1d70-4b07-88df-af491e94c917",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 77,
                "offset": 4,
                "shift": 37,
                "w": 29,
                "x": 87,
                "y": 81
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "6b590a07-fc25-4607-bf7d-607b33c37606",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 77,
                "offset": 0,
                "shift": 34,
                "w": 33,
                "x": 52,
                "y": 81
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c82b28fd-1306-42bd-8037-832536d69bfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 77,
                "offset": 0,
                "shift": 48,
                "w": 48,
                "x": 2,
                "y": 81
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4b95fff1-2b58-4422-8bc0-3e01ed1ff312",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 77,
                "offset": 0,
                "shift": 34,
                "w": 34,
                "x": 984,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5c0f10f1-01b3-4261-b477-01c667e72a19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 77,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 950,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8ded39c3-1180-4a5a-9567-acf2e9aa4364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 77,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 916,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a8b73562-3c26-4448-8f8f-af7143cbd534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 77,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 894,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6e099d79-ecc5-44f5-91d9-a3a2ec86aa3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 77,
                "offset": 6,
                "shift": 17,
                "w": 6,
                "x": 886,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5ce3d309-1017-4dac-8db0-8967379e0a25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 77,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 864,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "266cd695-9e54-4fb9-acd0-fc20115f510d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 77,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 37,
                "y": 239
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "edba99ae-d9f9-4ac9-9de1-033aefba91db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 77,
                "offset": 10,
                "shift": 40,
                "w": 20,
                "x": 74,
                "y": 239
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "e6205ea3-0099-4ae0-aa59-c8244c31aec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 65
        },
        {
            "id": "83615c20-0636-43d9-9ace-f27092cb2f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "afe3497b-6f18-48f6-a736-e05ab8930628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "8b7720f3-61f1-48f6-a35e-ddc952331e10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 902
        },
        {
            "id": "0f38eb0a-9522-47e8-947b-88c76678c8a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 913
        },
        {
            "id": "00e6ab58-ee25-4c29-b66c-c0efc4daf8bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 916
        },
        {
            "id": "88a2d7eb-d43e-42b2-8dab-832c2c613614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 32,
            "second": 923
        },
        {
            "id": "b767f829-fd10-4139-87ea-2b209c863bb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "1ca77695-377b-41e9-8741-12a543b50dc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "9a85e326-5ede-4c6b-a988-94e83d7b513e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "91a7381e-c097-4ec4-a480-a1252621c024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 49
        },
        {
            "id": "e099602e-5dcd-48d7-98af-bfcc8856287a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 32
        },
        {
            "id": "5299219d-350a-4403-85d8-46a2ac8a6fd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 84
        },
        {
            "id": "d744ca73-fcc7-4715-af67-a6434ebb0bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 86
        },
        {
            "id": "70c2f34e-6784-4447-a48a-516f87f8b3bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "ae944ece-d017-4ef8-9eca-43f7bf5778c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 89
        },
        {
            "id": "93fb7b7e-9db5-46f3-afd0-eef4470b4da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "cd4ffdff-7f70-4e8d-a6f5-18e1de633873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "ab6018ab-cd13-4c68-ace4-17df2ffdcc2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "ee589e90-3411-4112-b097-0c5348633f75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 160
        },
        {
            "id": "c7deb1f0-ccbe-4799-ba04-01466ed26438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 8217
        },
        {
            "id": "74e7389b-50ba-4673-8bf3-77f1667331f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 44
        },
        {
            "id": "a1f152db-629c-4b40-a7b6-30637bac6102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 46
        },
        {
            "id": "acc0da7c-b65d-4aec-8797-8a6d059518c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 65
        },
        {
            "id": "7fbe0717-afc8-485c-9c4b-8d47ceb21790",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 32
        },
        {
            "id": "38a1e4ee-17e8-44a5-abb1-61ea92817465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 84
        },
        {
            "id": "6510134c-b2dd-4ff7-838a-67712aae9d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 86
        },
        {
            "id": "ec202af2-1d0b-4cf4-9f6f-17a6a272a33a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 87
        },
        {
            "id": "a7e78997-59b2-49b3-8436-4f5da5efc86e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 89
        },
        {
            "id": "c944a965-e875-4d17-9e11-938aa04ce9e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "54f0604e-e98f-4829-bb42-b883a4346b24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 160
        },
        {
            "id": "a5b21ce3-60ef-4e3f-acaf-b2fc7a021ea5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8217
        },
        {
            "id": "66caa579-fb3d-42d5-b16f-4acb6166f813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "4e7d5e55-1146-444e-8307-bc105ba70b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 80,
            "second": 44
        },
        {
            "id": "5d306db3-0958-4e58-8773-4073df2fdf32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 80,
            "second": 46
        },
        {
            "id": "b791ab61-e2d7-41e8-b1e4-4740727dfc59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 65
        },
        {
            "id": "9d3c613d-2367-478a-94fb-963480f140ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "e1c81621-5cc8-46b4-91bf-cb0c3fee11e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "9834c0d2-9e61-4c08-b6cd-647bb88ed260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "454c3433-e43b-45ae-879b-49ef6deb77ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "bb48fe5e-3507-4743-b0bc-e94ab3109d81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "91be4a23-2489-41f8-93b1-a48435c7c810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "ff134367-bd8a-4172-a85e-271f6b872390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 44
        },
        {
            "id": "d7bc2f6d-0fb1-43e6-848f-e08f94ebcc7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 45
        },
        {
            "id": "3cb9fa8a-52d1-4a37-8bdb-31dafdf3e5ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 46
        },
        {
            "id": "4815dcb8-880f-4b6b-9939-1e3fdca00e06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 58
        },
        {
            "id": "1153dfc3-0db5-4525-a1eb-5fc1c002c223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 59
        },
        {
            "id": "b3b5d1f0-b473-4053-96e2-2b67631661a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 65
        },
        {
            "id": "8e245d1c-a125-4721-ad83-bba027012042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "ae56ca0f-ec5a-4d4c-b9ed-464a01365e27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 97
        },
        {
            "id": "2668709a-40b1-4d3d-9e04-9b06b0027a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 99
        },
        {
            "id": "1d7bfd6c-9aac-41d4-b9de-3897961df0e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 101
        },
        {
            "id": "0cc9748b-055f-4084-a415-b681e102495d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 105
        },
        {
            "id": "e46c7bcd-bc7e-4626-bbc1-ee865f086e74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 111
        },
        {
            "id": "77330318-8918-4fe5-a56d-a139eb7ffc2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "06a8cfa2-ab4d-4a6d-b4eb-b0530abaa869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 115
        },
        {
            "id": "93361a94-ce15-4eea-b832-64ee561ed8e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "c6ec942b-bb70-4eb6-b8d7-5af9d489860a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 119
        },
        {
            "id": "0e157901-84cf-4d5d-97c3-90c9247841c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 121
        },
        {
            "id": "69eee823-08ed-4613-9289-190b27ab38ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "a545075c-1161-4af7-8da1-a82d14e0c62b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 173
        },
        {
            "id": "267ce0de-524f-48c0-9d9a-820c9c6eb84a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 894
        },
        {
            "id": "995f5849-6eb6-47ab-b546-fdde442fe261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 44
        },
        {
            "id": "42d0153c-d5d5-4c4f-a069-0363974fee1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 45
        },
        {
            "id": "93b80e88-640d-42f3-a7d2-f723059d83b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 46
        },
        {
            "id": "c10e3f29-3bf3-4a84-bb51-3b14665c4a79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "5f1f6f24-62d1-495a-ab93-9405c9b48abf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "d5ccd1fd-5d26-4a6f-bc1c-342eb720dac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 65
        },
        {
            "id": "7f1e93ac-c8e1-4bfa-a04e-fb2ab6ad0205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 97
        },
        {
            "id": "011bfad6-bf8f-48cd-9aac-83f22091b81d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 101
        },
        {
            "id": "36782a27-18dd-49b6-a81e-f49917638d66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "14d38446-66fe-460c-9583-77e29047c17f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 111
        },
        {
            "id": "dc5d98be-9b72-4fb8-a410-64f083aa7749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 114
        },
        {
            "id": "c72371b1-01c1-45d6-b9cc-c24990dfcbb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "73834f0d-ef6f-403b-908a-8631fe007106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 121
        },
        {
            "id": "527cc2d8-e728-431d-9179-f6170e800d27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 173
        },
        {
            "id": "cdd54fa6-ff64-4a84-a658-3bfb8758b188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 894
        },
        {
            "id": "53fb49ac-973d-4231-a89b-954a3c8799b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 44
        },
        {
            "id": "96e19ce8-e032-452e-b327-3f2212bd6678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "be96a8e7-9ac6-466c-9a12-ad49757fd2c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 46
        },
        {
            "id": "c3961c1f-9746-47b4-95ee-220d79ca1e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "35a9e102-34d9-404d-8b4e-86b105ceca88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "7c7d2403-5ac1-41dc-a3d2-f8f242b50cb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "d0341b61-df4d-4b61-ba01-72394c92c59d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "54139c1f-495e-42de-afe8-4444a4573991",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "3d25574a-4688-49f3-b982-1dffc343f095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "c7a8723e-6522-4676-b70e-c63bffa805c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "b96c264a-6a8d-4991-83e0-072d7469d24f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "ee3cd521-a15a-43ba-a705-87fa1be375b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "aa4b80f2-aee3-474d-a95f-6ec798ce271b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "c1c6fc61-91ed-4d5c-a764-e22b49e387bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "613c3aa5-41be-4bf0-8d0e-0d36ae027fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "232c74f8-d0b7-4b05-b161-aeb3475e15ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 44
        },
        {
            "id": "9e1aa71c-be0c-4db0-b2da-d9b859433094",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 45
        },
        {
            "id": "9ff6759c-5300-4fd2-a04b-0b24b4f3cfe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 46
        },
        {
            "id": "396c988c-a60c-4f68-8c23-d98fbbbdb5fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 58
        },
        {
            "id": "53ef2892-f23e-4d25-b979-f81558777d94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 59
        },
        {
            "id": "995e52b1-b310-4d25-a417-4151f4158e2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 65
        },
        {
            "id": "a9ec15e8-9311-47b3-8842-78e27ca289d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 97
        },
        {
            "id": "86c15e52-c595-4afe-9c4a-28c9833d9a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 101
        },
        {
            "id": "29b859ba-debb-4a9c-b940-e2a6419f8e8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 105
        },
        {
            "id": "75e7c25f-1c47-4842-ad76-9d30696144bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 111
        },
        {
            "id": "06bd673f-10b2-43ca-9070-e6f4e38ec3a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 112
        },
        {
            "id": "d987a977-8aaf-4224-a437-8a239b27a919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 113
        },
        {
            "id": "ac7739c3-8b12-4acf-a101-4d15e77d06e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 117
        },
        {
            "id": "5ab478ce-1b6c-4c2e-84d5-e36e1c1fd0ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 118
        },
        {
            "id": "47abffc9-dca9-4d14-8dd6-60061bac7c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "38e179fc-71c6-4851-88b1-b9ab9e6e56b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 173
        },
        {
            "id": "3b5f521c-7960-4850-b833-ba03c71433d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 894
        },
        {
            "id": "ce6878bc-351c-4387-9d60-cec6c4d31668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "e715586e-0088-44b6-b51c-d28664ba161a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "5fb46e5a-f94a-4352-89f4-56fbba6a012a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 44
        },
        {
            "id": "7eaff0f6-9afc-48dc-8807-3e50102c542f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 46
        },
        {
            "id": "17950ef7-33e4-400e-8916-70b15a5ea01b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "0bf9b44e-c683-40a9-9cad-927fd3ef6df7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 44
        },
        {
            "id": "0378e0b8-9d2a-4b5d-a202-c136c82090cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 46
        },
        {
            "id": "2ec8605e-f554-4971-a1be-e7e33a857af6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 44
        },
        {
            "id": "8ea15540-d95c-4fda-b3f6-600c2e759b62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 46
        },
        {
            "id": "da15c8fe-51ec-4fc6-bfaa-97ed0b752b17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 44
        },
        {
            "id": "21b54645-5a3a-43ce-9e56-9cefc1aedf72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "Basics",
    "size": 50,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}