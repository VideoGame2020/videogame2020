{
    "id": "726034ca-8f34-4c17-bee8-766ad7e4840e",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fScore",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "9fce0be0-da1b-492d-8be9-8d7ca89a96f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 56,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f2718dbd-0fbb-447f-98df-c750131a8b3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 56,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 56,
                "y": 176
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "de6cfd79-afb1-4887-8053-6926b43e8fad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 56,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 39,
                "y": 176
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f9b97d7f-a98f-46be-8446-33863d8c296a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 56,
                "offset": -1,
                "shift": 34,
                "w": 35,
                "x": 2,
                "y": 176
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d457aaba-2b41-4075-84e5-421e32307c71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 56,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 473,
                "y": 118
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "1531d73e-1fc5-46f3-815d-33348df5c623",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 56,
                "offset": 2,
                "shift": 33,
                "w": 31,
                "x": 440,
                "y": 118
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5f5c6af7-5874-4f1f-a6fd-79a7c921fd08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 56,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 412,
                "y": 118
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9333f23c-0014-4d41-81d4-2b3707f192b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 56,
                "offset": 5,
                "shift": 17,
                "w": 7,
                "x": 403,
                "y": 118
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6df1cfec-4246-46d6-a529-6c4550030bf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 56,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 387,
                "y": 118
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "15a446de-0c7c-4128-94ce-56e5d237d4bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 56,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 371,
                "y": 118
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "459d7ba0-d52a-430c-8dd4-c6a165c2ae00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 56,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 65,
                "y": 176
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "052456cd-0ec9-4414-ab2b-805c609cd76a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 56,
                "offset": 3,
                "shift": 24,
                "w": 19,
                "x": 350,
                "y": 118
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a6621bc5-4e81-46fd-98cf-002557966bf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 56,
                "offset": 4,
                "shift": 17,
                "w": 8,
                "x": 315,
                "y": 118
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "600d6f80-e162-4538-aadf-86b855c1fd0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 56,
                "offset": 4,
                "shift": 24,
                "w": 17,
                "x": 296,
                "y": 118
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "04214f16-44fd-4ab2-9feb-3ab1ad3ced7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 56,
                "offset": 5,
                "shift": 17,
                "w": 7,
                "x": 287,
                "y": 118
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6f72198b-03ea-46bc-ac45-24c0be635c49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 56,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 265,
                "y": 118
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9389f94c-abc5-4dfa-a01f-eab3b8a0a213",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 56,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 239,
                "y": 118
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "71f02da4-304c-4fbc-9ecc-2e62661d12d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 56,
                "offset": 4,
                "shift": 24,
                "w": 15,
                "x": 222,
                "y": 118
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3c8bf39e-4f68-4dc0-90cf-15242f4b283a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 199,
                "y": 118
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "fc5dda23-66f6-475b-836b-2f88df070f4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 177,
                "y": 118
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f7c273eb-84b5-430b-b797-08c28ac06c99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 56,
                "offset": 0,
                "shift": 24,
                "w": 25,
                "x": 150,
                "y": 118
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c0bc111d-308a-4f99-9c05-ca2b0f89afd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 325,
                "y": 118
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1812deec-68dc-47d7-9c76-41b2a6d8b840",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 87,
                "y": 176
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5b173a2e-8a16-496f-981f-d4d4fb700c9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 56,
                "offset": 0,
                "shift": 24,
                "w": 25,
                "x": 111,
                "y": 176
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "214879c7-50d0-4268-af9f-369369cd4744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 138,
                "y": 176
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "cf13e15e-ca2e-48e0-99bd-51369fe4af39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 165,
                "y": 234
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "686b36fe-f740-42fc-bbc9-d8bfa890e038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 56,
                "offset": 5,
                "shift": 17,
                "w": 7,
                "x": 156,
                "y": 234
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7d5af456-9232-4f5a-b07a-2bfa94439f61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 56,
                "offset": 4,
                "shift": 17,
                "w": 9,
                "x": 145,
                "y": 234
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f3650ab4-ba0c-40f4-b838-d00200f05dc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 56,
                "offset": 4,
                "shift": 24,
                "w": 15,
                "x": 128,
                "y": 234
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9ce6879e-6673-4b0d-b7fb-ae82313eaf5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 56,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 108,
                "y": 234
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "31dc9808-5695-4408-9f2f-3dcba87978ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 56,
                "offset": 5,
                "shift": 24,
                "w": 16,
                "x": 90,
                "y": 234
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "ba716369-41c2-41df-8f6c-4781c21327ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 56,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 68,
                "y": 234
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "75e67490-21da-44ea-abac-c3f56baf5306",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 56,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 31,
                "y": 234
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "cb8e8ab1-d186-45c9-8b8d-53c98d665445",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 56,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 2,
                "y": 234
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1954da92-8424-4d8e-93b6-3074f5343ba9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 56,
                "offset": 2,
                "shift": 25,
                "w": 23,
                "x": 464,
                "y": 176
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "76f112ff-ae39-4a3c-990f-916690f66a57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 56,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 437,
                "y": 176
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "8543aaa5-48d4-4cc9-b469-e4f9d8303070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 56,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 409,
                "y": 176
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "5893f39b-9bbb-4388-8bc6-554af75ce8ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 56,
                "offset": 1,
                "shift": 25,
                "w": 24,
                "x": 383,
                "y": 176
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b6bfd905-07d9-4127-b97a-fd43ac58146f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 358,
                "y": 176
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "389f34c0-ccc2-463b-81f3-8098135fd4b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 56,
                "offset": 0,
                "shift": 27,
                "w": 28,
                "x": 328,
                "y": 176
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "16ee5070-1c8e-4fbf-bf2c-f5a51b2663aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 56,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 298,
                "y": 176
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d658d45c-b4c0-4ffd-bec6-873234830c28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 56,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 274,
                "y": 176
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6b4ec521-445a-4baf-b9c9-d0daee7dc661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 56,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 246,
                "y": 176
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0e8352b7-ed33-4495-aeea-c6fd05ed829b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 222,
                "y": 176
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e43c416c-3c3b-40b2-afe9-98e788a632a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 56,
                "offset": 1,
                "shift": 22,
                "w": 22,
                "x": 198,
                "y": 176
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1ee0f687-1cdb-452f-9f98-32ed23ea8f14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 56,
                "offset": 1,
                "shift": 35,
                "w": 34,
                "x": 162,
                "y": 176
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ce37dbe3-9282-4b82-beaa-d575a8e0b4a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 56,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 117,
                "y": 118
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "624cf88a-4739-42ba-8b83-91b49f7b1b93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 56,
                "offset": 1,
                "shift": 32,
                "w": 30,
                "x": 85,
                "y": 118
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b5fa1780-d550-44f3-94ed-ab82f30bb0d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 63,
                "y": 118
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "4c81720f-a919-44dd-9a26-4424f19972b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 56,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 43,
                "y": 60
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fd576092-a646-4701-831f-138d81ac8606",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 56,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 2,
                "y": 60
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2924d186-6710-4456-a4d9-3b53cfc8d27b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 56,
                "offset": 0,
                "shift": 28,
                "w": 27,
                "x": 478,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "da06e73c-021c-4331-b577-127ac0464b1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 56,
                "offset": 0,
                "shift": 28,
                "w": 29,
                "x": 447,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "0378c030-3d64-4ee2-81a8-42ab16c09dab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 56,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 418,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a4949d70-d65a-4918-85ab-64d5c3950fb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 56,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 390,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e43b683c-dd72-4af2-9e48-26bf6b572039",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 56,
                "offset": 0,
                "shift": 42,
                "w": 41,
                "x": 347,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c11b16ab-0f24-408e-9966-29f8ae1ca842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 56,
                "offset": 0,
                "shift": 29,
                "w": 28,
                "x": 317,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "eedaecf8-545a-43e9-9551-074b0a7d7967",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 56,
                "offset": -1,
                "shift": 25,
                "w": 26,
                "x": 289,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "26ad59f9-45ea-48f1-a8c8-188d333da546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 56,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 259,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5a0811e2-030e-4ba3-8d6a-a1e65afa73b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 56,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 28,
                "y": 60
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1c5bc9bf-2634-4d04-9599-c1ded5707aca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 56,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "413eee89-90df-44a0-9ad2-0ab125d07410",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 56,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "40bc62a0-3df0-4da8-9779-6cc6c6fa5735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 56,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "2cc5e033-3463-47c2-8592-2c8af94b5d43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 56,
                "offset": -2,
                "shift": 25,
                "w": 29,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "0021ae8c-9d8a-4363-9fb0-3d6451f88415",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 56,
                "offset": 2,
                "shift": 22,
                "w": 11,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6789d6cd-8b75-4c39-b448-eefb381a8e9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 56,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c3b715e0-022b-48bd-80bd-689c0cbe3ec5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a17e6e17-2b2c-4bf2-8cc1-fc2fcf3a291d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 56,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b3ac3bf5-9fc4-4451-84af-02f7228750d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 56,
                "offset": 0,
                "shift": 24,
                "w": 22,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1b8643c8-3aad-4a8b-b988-0ee66feab05f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 56,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "902a1d3c-1bc8-48db-b36c-eeb64391d75a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 56,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4f50f945-0949-4b14-8805-0919e8764159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 56,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 80,
                "y": 60
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "bea02c0d-7229-4eb7-89fe-e65cdf43abec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 56,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 303,
                "y": 60
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7b078f80-b972-42de-ab69-1dabae2f0730",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 56,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 103,
                "y": 60
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ab591ef9-ed4f-47e8-9352-4b419cc586ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 56,
                "offset": -2,
                "shift": 16,
                "w": 16,
                "x": 25,
                "y": 118
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3e9e0c3c-0e16-4ebf-bcbd-a1ee1c0b078a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 56,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 2,
                "y": 118
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d8bb7c12-ebd7-4dfb-b4ed-14e9c85be900",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 56,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 486,
                "y": 60
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "36da5d47-d06f-4a96-9272-465c917571da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 56,
                "offset": 1,
                "shift": 31,
                "w": 30,
                "x": 454,
                "y": 60
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "9e604add-b6a2-4a50-94b7-53a76acaba6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 432,
                "y": 60
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "af0a9f8d-9ee0-4834-b9b7-7766d3bf0776",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 56,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 410,
                "y": 60
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "3e956b47-493e-4a5d-a8c0-9e21492a9ff3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 388,
                "y": 60
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "909ce448-0b93-43f4-b846-3129c900b0aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 56,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 366,
                "y": 60
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "29add750-a5b8-4e81-ae12-3e2d085bf135",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 56,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 346,
                "y": 60
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c94ab955-2468-4688-9b88-c913e0d4be78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 56,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 43,
                "y": 118
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "2c277d2f-3541-4681-8259-f93250798a3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 56,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 325,
                "y": 60
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6159311f-beb8-4807-b67f-d9c103c0d164",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 282,
                "y": 60
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0a319e8e-2b76-4e5c-9195-d4accc4a49c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 56,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 260,
                "y": 60
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "fd0e730a-d50b-4350-8d8e-c39db04e8045",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 56,
                "offset": 0,
                "shift": 27,
                "w": 28,
                "x": 230,
                "y": 60
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2a8e4cbd-b43b-4af5-9fb5-b6e63b5deb6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 56,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 204,
                "y": 60
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "51c7e540-5aaa-44d5-aeb4-9a6fc233d7d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 56,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 180,
                "y": 60
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "6cdaace1-4f2d-482d-94a4-436b0609e221",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 56,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 157,
                "y": 60
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "468b666d-9ff2-4466-8242-53984e48e77b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 56,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 139,
                "y": 60
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "134ec1fa-ddd1-4462-b686-f9faf6124912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 56,
                "offset": 6,
                "shift": 17,
                "w": 6,
                "x": 131,
                "y": 60
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "3fb8bdd8-19a0-4540-83a7-a99c8f8b25d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 56,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 113,
                "y": 60
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a41ffbcd-1f32-4a41-aeea-5ca332515fe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 56,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 189,
                "y": 234
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "38ffa8d5-9ad8-4d42-81f5-49ecd5b6d2c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 56,
                "offset": 6,
                "shift": 24,
                "w": 12,
                "x": 214,
                "y": 234
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "25",
    "size": 30,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}