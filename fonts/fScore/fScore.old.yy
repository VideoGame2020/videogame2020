{
    "id": "726034ca-8f34-4c17-bee8-766ad7e4840e",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fScore",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "fb9e2319-5652-477c-af1e-04c5b959c5ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "161256f7-474e-486c-a0e3-e9ab430979ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 165,
                "y": 42
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "1e88a59b-e3a1-4dfa-804f-4abb216a4a10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 158,
                "y": 42
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f14bbfc8-3e63-4329-a4e4-426a90eaea47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 147,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d13c589d-e26d-4e81-99f4-77c1f1b2ab50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 136,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ffe39416-5d0e-4005-9447-fbca053dd22a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 120,
                "y": 42
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9113f336-1f07-4028-b4af-3f6f49000b27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 107,
                "y": 42
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f561ce24-f477-42ef-8d32-12c82982d852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 102,
                "y": 42
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5c868697-b910-48fd-8240-547d57b489f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 95,
                "y": 42
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b9d17d48-028b-4de4-8acc-829ebdaae5c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 88,
                "y": 42
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e058122e-1b73-4024-8428-4cbcb744fd2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 170,
                "y": 42
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "739d12a1-22c5-48c8-aae0-3142326bdf90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 77,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f2d95db3-ad8b-4ba6-b84b-c37f88460942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 61,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "11abda1b-e914-4f52-8aff-24eb586b5141",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 54,
                "y": 42
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "dca35c44-ac04-435e-b8d5-112eac852b80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 49,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "544d7bd6-b4d9-445e-a6ca-ed7851bd8b98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 42,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "bdfac1ff-c57a-4e17-9fc2-120d61f53357",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 31,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "3e165e2c-473b-4a61-9739-8066669014ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "65442667-18fe-4757-8544-ab5bd795c4d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9fac8673-aaba-401a-beba-bde1f2ac628c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "13a19019-2988-4200-9c83-67a0a79c4d64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 240,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "94699c77-cf64-4d30-8859-3ac76a153425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 66,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "cc7eadeb-f762-4dd0-a4f8-08908cf3e379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7b8be173-9aea-4b37-a7ca-0fee0dfe6820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 189,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ee549891-90d9-468a-8660-8b071c691887",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 200,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f52d2f35-6ec4-4076-bbed-7c076d0375cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 186,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8404f780-b033-4ba3-b822-e4b87456f955",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 181,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "58aa8e29-7b68-4bd2-a22a-bc6fad531bef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 176,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "199f91cd-dc2b-4d6f-a213-c6a30b90497b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 165,
                "y": 62
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d5948677-36fe-4ab0-a5cf-ce412b89973e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 154,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fad225c0-8910-48fa-907d-9733f913c0f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 143,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "4d7426b2-e7a1-41bb-a3de-bfb8e3c1603b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 132,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "5a802fb2-7684-4f4a-9b14-0d07857f98d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 114,
                "y": 62
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9266c23e-f771-4268-97b2-d789af2fe99e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 100,
                "y": 62
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "58cb2b28-3266-478e-a946-806f91c69343",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 89,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "17499d7f-ef90-4ffc-8cd5-82813c2812a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 76,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "0de0fbea-8ca4-4504-9e74-f3586e5ac572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 64,
                "y": 62
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c60b998b-d65e-44f1-8cf6-1999f0989791",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 53,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "3a62089f-11e5-4620-8c48-3943a3929c1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 42,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "886dfc9b-7b17-4f0b-97c1-d10aeeb9d948",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 28,
                "y": 62
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4331b209-4c90-4924-bc3b-03fd0f439376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 16,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "59d9264e-5d88-4e47-aa2d-f675736b0350",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "93bc54db-fba2-4e7b-96d8-fad043d9add8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1489708c-4ea5-4494-ae0f-45840cd22a60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 235,
                "y": 42
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "24a98674-30a2-4fc4-b1c8-59dff4882aae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 225,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "cdc30558-cdd9-454d-8f25-56904b103a68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 211,
                "y": 42
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "199d5b1e-6e6d-4f15-86b6-dbf12e43eba4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 228,
                "y": 22
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c97e7efd-92f8-4693-a303-065eb9245ae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 214,
                "y": 22
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "cee08240-8ccf-436a-b5a7-5b095abf0671",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 203,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3c3e3692-5d99-41d9-bc19-f7757fd5953d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "934af74d-2bfc-4a67-976b-39812d2bada1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b59f37fd-7a65-4b2c-a343-58d443af7cee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "dda9a808-8e24-4c08-909c-e4be2f551c13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "0fc1879b-0795-4a89-9f4f-276580e9db72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "eb708eee-6c50-4ee7-abc3-e6732dc78e98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "38ceb054-870b-4a5e-9e6b-62d8087d8f65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "92002705-0950-4807-ada0-a54d0ddb1e58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a0438c80-fbdf-490d-b4e3-c57c30717948",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "87c5a51b-a6e5-4dc2-8f40-6fdf9f574d0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "a22df90f-e07c-4bf7-9522-a206e4c9f183",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "aed28f5a-064d-4703-bf32-b13666748ec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "0ac4ef14-8f60-4fa2-9c87-8aae588257e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b831b4b3-0533-407c-946a-22e2d5de8bbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e5a6fb64-ab0e-4608-a0f5-a72a23adb18f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b33853a3-bfa0-4b89-9070-6ca612713e1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "83cf557b-162e-46b0-aa0b-c3e7b62b3f4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "05b3dcb7-d413-4af5-8b1e-a19bab7c728a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "038f4c4c-68d7-4f38-be1e-fce76090503c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e9a3fe5b-9edc-46b9-8881-5a86a9275972",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e0fc99d8-b1c2-4bfd-b528-66555b3e6ea8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d89ff492-24e7-42be-a45e-fd5ee97d2e35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f3b94a62-ee2d-48c5-93b6-c7da4d887bcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b96e4352-0a6d-4cea-8405-34ed9f8cba27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 97,
                "y": 22
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "df31411a-136d-4bb2-b762-fbc306813318",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 12,
                "y": 22
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "39e38c6d-f00e-4a4f-9324-39d3b47e34a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 187,
                "y": 22
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b6bfd85b-16f5-4ea4-bb56-80f3e7be4da3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 178,
                "y": 22
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f14757aa-d393-4b37-a5cc-160ce46247d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 174,
                "y": 22
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "77421642-5b00-4f03-a1a3-1e0de4f36376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 160,
                "y": 22
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5e609fea-c849-42bd-8ad8-a153942e366b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 151,
                "y": 22
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "63c49ddd-1abf-4b4b-a382-de7906e3e7eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 140,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "84a6a6ce-de38-4131-bcef-7174f7ed2fc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 130,
                "y": 22
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c6ab1942-ec38-4158-bf5e-7bd8a81d0d38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 120,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c435e1bb-cd5e-448a-af00-c24b1ec2009f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 113,
                "y": 22
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5a7e6590-8ca2-49c0-af70-30e0359471db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 193,
                "y": 22
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8f2e65b3-055b-49b9-904a-482d6c0f531e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 106,
                "y": 22
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "86a6d8ad-f57a-4468-a655-18e455a4baa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 88,
                "y": 22
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "dd2137b9-646c-4521-b651-40cf198d7249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 78,
                "y": 22
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b6f81104-e876-4ec1-a582-f94aa0e512ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 64,
                "y": 22
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "3c8d9917-60be-4b84-afcd-a84750afc19b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 54,
                "y": 22
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "bebc8761-66dd-4cea-b9bb-f3ea4736033a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 44,
                "y": 22
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2181df41-bbaf-4b1e-8843-2ad06f8ed6c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 34,
                "y": 22
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "709608a9-9b46-4178-bf1f-62a226a53a9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 27,
                "y": 22
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "099b35a9-d1bd-491d-af7c-d1317d61b712",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 23,
                "y": 22
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "033ad52a-2bd1-4f74-adf1-04fb1817d795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 22
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a0a1f9ec-3a8e-4cfd-8df5-bdbdcb620f5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 197,
                "y": 62
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4433628d-35a0-4299-8f90-8c408b485311",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 208,
                "y": 62
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "8361122d-9784-4b3b-bb04-e5259a6a3474",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "3a2691d6-0cfd-4853-b2e7-5a2c32eebd69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "02925e8b-806d-4a57-82b4-21d5958ec1a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "4a0f3257-5270-481e-8e69-5608900c95c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "599d114d-28ea-4765-9004-7088a3e5fd6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "165f9364-1e06-46e5-a8b4-ea98a3d230b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "975c429d-22e7-44a1-9667-a349c7fc6700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "500a1849-c168-4e02-b203-2b855ceb8323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "c377a9d9-faa1-4602-8c94-6b49052eee0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "384f8ae5-93c6-4e37-8cf8-3e9f8f2d17ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "63e223ef-07f8-4e08-b222-acabde0b6b9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "83105aec-de76-4725-acee-28f21bc09416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "68dc99f6-d309-48df-83cf-6c0d88081aef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "2aadb9c7-64f2-405b-b8d2-19161a7b515e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "4597dd1d-f31d-48d3-935c-43de141c1c8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "f6ce4a0b-3a59-48e0-892c-e33ffcbeae8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "b199e6b8-eabb-4ce6-a100-61d2d80a22ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "ab7d8586-0534-4c8a-b1b9-5700c8d1b2e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "5b81465c-c3d0-45a7-8ebf-55db30af329b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "b821259e-5f49-4902-b499-f167f9841fe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "8e9b8812-fb83-44be-a954-76a4f150f233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "960348e5-63a2-4c76-8325-ff4a478e0092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "c1728b95-d9ff-4ceb-aa96-533a7d712a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "a6fbb184-5a0c-4902-9067-f16af65c3a91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "8381faa6-711c-4ff3-a70c-97c10035cce4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "c479c304-4886-4eff-9e8d-0641bc6da217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "75179578-8b55-447f-ad73-88cb998980df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "d0d14e40-30bc-403b-a170-39c2c4356579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "763d1cce-b94f-4615-99ef-372d54e46c2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "adf53e38-4931-4c37-bbc7-1e55d2bfa3c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "5e9e0914-a86f-4b9c-8ffd-2968d3c03e46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "a5098af4-b7fa-49b9-8619-0524762bc674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "ffa4ea42-bdd3-4f76-9d24-a28c3f086549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "22223f84-7b1e-4e49-b50e-23d7b886a979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "8da280bc-33f2-47ee-9813-a64df3a99e72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "8926817c-851f-4df2-8d79-188c2ffa744e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "9189d3c0-c668-42a9-b877-217315466642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "931fcdcd-3c25-43e0-bba9-84e19428c79b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "8c0e6df5-4384-47f1-a185-2f126c6d2d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "cf70bd91-d8ec-4dd5-bcf5-25593128cf79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "2e2d093a-c1a9-4d61-aa71-40718a0f235b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "2cf400b2-60ad-41cd-875a-b6f33da27730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "ea81c3b8-16eb-44c4-a1ac-984f1afb581c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "bb1bdb95-f957-44fa-955e-4cd9f7b8498c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "069f2ddf-8399-41b7-9c28-e98f32906f1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "8d745cb6-f6ba-43c1-841f-a979cd885273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "89a63c8a-012d-4611-8f43-39f70e615075",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "c351d92d-5946-4196-bbf8-24d51a6e3a35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "a5144252-cdb5-4f0f-898b-5ace115dca2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "ed032cb0-b294-4dad-a4fc-a6015fa04b23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "f76db51d-f9e6-4908-8890-30c2ed8fafca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "cda5b973-84e9-4167-8bb7-34f9be32abf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "5e3009ac-c2ed-4590-9816-11e4afe41974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "71339707-c703-4558-a7e1-befef5c1e861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "899d190c-878c-4f6f-a838-e5c1f78cb713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "5d66e93a-15a1-4c6a-aec2-8c68ae12d748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "b61279f0-de99-4f9b-8ac6-ca48742e7c73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "fb9c9acd-b0c1-4db7-a533-53cc08e84c03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "ed14ea84-00d8-4c30-a87b-f1b2923bc140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "4a4ec6ce-2f15-474e-8181-e677ece7eca9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "dc4791d6-7d90-4059-969d-02273efcbd44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "c19d18fb-115d-42cc-8301-9225a62cc20f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "50407713-16d5-4f3b-9ef8-491a92061a59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "dbbbf66f-0d9d-4b72-a6d0-b885472ab05f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "25bcd8f5-6b0c-4806-9ed0-b1e89a6d7998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "05a11eb6-07d5-4c82-b453-3108d10a5cb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "ef87f4fb-c23f-4f18-a395-e22a306959c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "de1e75df-7785-42d3-8f88-619d70d53c2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "65640723-368e-4f55-8090-e4242440de6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "4f48711c-3348-4934-944c-288fe67e925e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "f912de6b-80ca-4eb8-b5c9-4e1f4493dbd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\nDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}